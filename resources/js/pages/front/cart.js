var sumnumber = 0;
var sumprice = 0;
var sumtotal = 0;

function updateTotal(id) {

    var qty = document.getElementById('id' + id).value;
    var sum_qty = parseFloat(qty) + 1;
    document.getElementById('id' + id).value = sum_qty;
    document.getElementById('ids' + id).value = sum_qty;
    updatePrice(id, sum_qty, "add", qty);
}


function downTotal(id) {
    var qty = document.getElementById('id' + id).value;

    if (qty == 1) {
        return false;
    }
    var sum_qty = parseInt(qty) - 1;
    document.getElementById('id' + id).value = sum_qty;
    document.getElementById('ids' + id).value = sum_qty;
    updatePrice(id, sum_qty, "del");
}

function calShipping(sum) {
    if(sum > 500){
        return sum;
    }else {
        return sum + 40;
    }
}
function updatePrice(id, sum_qty, type, qty) {

    $.ajax({
        type: "POST",
        dataType: 'JSON',
        url: '/shop/me',
        data: {
            data: 1
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function success(data) {

            var html = '';
            var html_mobile = '';
            var fixid = "sumtotal" + id;
            var fixidmobile = "sumtotalmobile" + id;
            $("#" + fixid).html("");
            $("#" + fixidmobile).html("");


            var html_mobile_cal = '';
            $("#product-incard_mobile_cal").html("");
            $.each(data.products, function(index, itemdata) {
                if (index == id) {
                    html = '<font id="' + fixid + '"class="{{$a}}" style="vertical-align: inherit;">฿' + ((itemdata.price_discount) * (sum_qty)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '' + '</font>\n'
                    html_mobile = '<font id="' + fixidmobile + '"class="{{$a}}" style="vertical-align: inherit;">฿' + ((itemdata.price_discount) * (sum_qty)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '' + '</font>\n'
                    //   ---------------------------------- Update Sum Price ----------------------------------
                    if (type == "add") {

                        var productitem = {
                            //  สินค้า Sku
                            sku_qty: sum_qty,
                            sku: id,
                            sku_total: (itemdata.price_discount) * (sum_qty),
                            type: "+",
                        };

                        $.ajax({
                            type: "POST",
                            dataType: 'JSON',
                            url: '/update/cart',
                            data: productitem,
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function success(data) {
                                console.log('itemdata up',data.carts);
                                var prouduct_alone_total = [];
                                var sku_qty_total = [];
                                $.each(data.carts, function(index, itemdata) {
                                    prouduct_alone_total.push(parseFloat(itemdata.sum_total));
                                    sku_qty_total.push(parseFloat(itemdata.sku_qty));
                                });

                                var sum = prouduct_alone_total.reduce(function(a, b){
                                    return a + b;
                                });
                                var sum_qty_total = sku_qty_total.reduce(function(a, b){
                                    return a + b;
                                });




                                var cal = calShipping(sum);


                                var htmlsumnumber = '';

                                $("#number_sum").html("");
                                htmlsumnumber = '<span id="number_sum">' + sum_qty_total + '</span>'
                                $("#number_sum").append(htmlsumnumber);
                                var htmlsumprice = '';
                                $("#sumprice").html("");
                                htmlsumprice = '<span id="sumprice">' + sum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '' + '</span>'
                                $("#sumprice").append(htmlsumprice);

                                $("#sumprice_mobile").html("");
                                htmlsumprice_mobile = '<span id="sumprice">' + sum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.00' + '</span>'
                                $("#sumprice_mobile").append(htmlsumprice_mobile);

                                $("#total_sum").html("");
                                htmltotal_sum = '<span id="total_sum">' + cal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '' + '</span>'
                                $("#total_sum").append(htmltotal_sum);

                                $("#total_sum_mobile").html("");
                                htmltotal_sum_mobile = '<span id="total_sum">' + cal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.00' + '</span>'
                                $("#total_sum_mobile").append(htmltotal_sum_mobile);

                                if (cal < 500) {
                                    $("#ems_sum").html("");
                                    htmlems_sum = '<span id="ems_sum">' + 40 + '</span>'
                                    $("#ems_sum").append(htmlems_sum);

                                    $("#ems_sum_mobile").html("");
                                    htmlems_sum_mobile = '<span id="ems_sum">' + 40 + '</span>'
                                    $("#ems_sum_mobile").append(htmlems_sum_mobile);


                                } else if (cal > 500) {
                                    $("#ems_sum").html("");
                                    htmlems_sum = '<span id="ems_sum">' + 0 + '</span>'
                                    $("#ems_sum").append(htmlems_sum);

                                    $("#ems_sum_mobile").html("");
                                    htmlems_sum_mobile = '<span id="ems_sum">' + 0 + '</span>'
                                    $("#ems_sum_mobile").append(htmlems_sum_mobile);

                                }

                                console.log('sum',sum);
                                var logis = 0;
                                if(sum < 500){
                                    var logis = 0;
                                }

                                // html_mobile_cal += '<div class="col-xs-12">';
                                // html_mobile_cal += '<div class="price-total-info-top"><p class="clearFix"><span class="fl">ยอดรวมสินต้า</span>';
                                // html_mobile_cal += '<span class="fr"><b>' + sum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.00' + ' บาท</b></span></p>';
                                // html_mobile_cal += '<p class="clearFix"><span class="fl">ค่าจัดส่ง</span><span class="fr">'+ logis +' บาท</span></p>';
                                // html_mobile_cal += '<p class="clearFix"><span class="fl">ยอดรวมทั้งหมด</span><span class="fr"><strong class="text-danger"><b>' + cal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.00' + ' บาท</b></strong></span></span></p>';
                                // html_mobile_cal += '</div>';
                                // $("#product-incard_mobile_cal").append(html_mobile_cal);

                                var htmlsumnumber = '';
                                $("#sumprice").html("");
                                htmlsumprice = '<span id="sumprice">' + sum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '' + '</span>'
                                $("#sumprice").append(htmlsumprice);






                                if (data.status == false) {
                                    swal("Out of stock", '', "error");
                                } else {}
                            },
                            error: function error(qXHR, textStatus, errorThrown) {}
                        });

                    } else if (type == "del") {
                        var productitem = {
                            //  สินค้า Sku
                            sku_qty: sum_qty,
                            sku: id,
                            sku_total: (itemdata.price_discount) * (sum_qty),
                            type: "+",
                        };
                        $.ajax({
                            type: "POST",
                            dataType: 'JSON',
                            url: '/update/cart',
                            data: productitem,
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function success(data) {
                                console.log('itemdata down',data.carts);
                                var prouduct_alone_total = [];
                                var sku_qty_total = [];

                                $.each(data.carts, function(index, itemdata) {
                                    prouduct_alone_total.push(parseFloat(itemdata.sum_total));
                                    sku_qty_total.push(parseFloat(itemdata.sku_qty));
                                });

                                var sum = prouduct_alone_total.reduce(function(a, b){
                                    return a + b;
                                });
                                var sum_qty_total = sku_qty_total.reduce(function(a, b){
                                    return a + b;
                                });

                                var cal = calShipping(sum);

                                $("#sumprice_mobile").html("");
                                htmlsumprice_mobile = '<span id="sumprice">' + sum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.00' + '</span>'
                                $("#sumprice_mobile").append(htmlsumprice_mobile);

                                var htmlsumprice = '';
                                $("#number_sum").html("");
                                htmlsumnumber = '<span id="number_sum">' + sum_qty_total + '</span>'
                                $("#number_sum").append(htmlsumnumber);
                                $("#sumprice").html("");
                                htmlsumprice = '<span id="sumprice">' + sum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '' + '</span>'
                                $("#sumprice").append(htmlsumprice);




                                $("#total_sum").html("");
                                htmltotal_sum = '<span id="total_sum">' + cal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '' + '</span>'
                                $("#total_sum").append(htmltotal_sum);

                                $("#total_sum_mobile").html("");
                                htmltotal_sum_mobile = '<span id="total_sum">' + cal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.00' + '</span>'
                                $("#total_sum_mobile").append(htmltotal_sum_mobile);





                                if (cal < 500) {
                                    $("#ems_sum").html("");
                                    htmlems_sum = '<span id="ems_sum">' + 40 + '</span>'
                                    $("#ems_sum").append(htmlems_sum);

                                    $("#ems_sum_mobile").html("");
                                    htmlems_sum_mobile = '<span id="ems_sum">' + 40 + '</span>'
                                    $("#ems_sum_mobile").append(htmlems_sum_mobile);


                                } else if (cal > 500) {
                                    $("#ems_sum").html("");
                                    htmlems_sum = '<span id="ems_sum">' + 0 + '</span>'
                                    $("#ems_sum").append(htmlems_sum);

                                    $("#ems_sum_mobile").html("");
                                    htmlems_sum_mobile = '<span id="ems_sum">' + 0 + '</span>'
                                    $("#ems_sum_mobile").append(htmlems_sum_mobile);

                                }


                                if (data.status == false) {
                                    swal("Out of stock", '', "error");
                                } else {}
                            },
                            error: function error(qXHR, textStatus, errorThrown) {}
                        });
                    }
                    // ---------------------------------- End Update Sum Price -------------------------------
                }

            });


            $("#" + fixid).append(html);
            $("#" + fixidmobile).append(html_mobile);

        },
        error: function error(qXHR, textStatus, errorThrown) {}
    });

}




$(document).ready(function () {

    getincart();
    function getincart() {  // ดึง session มาแสดง

        $.ajax({
            type: "POST",
            dataType: 'JSON',
            url: '/shop/me',
            data: {
                data: 1
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {

                var html = '';
                var html_mobile = '';
                var html_mobile_button = '';
                var html_mobile_cal = '';
                $("#product-incard").html("");
                $("#product-incard_mobile").html("");
                $("#product-incard_mobile_button").html("");
                $("#product-incard_mobile_cal").html("");

                $.each(data.products, function(index, itemdata) {

                    sumnumber += itemdata.sku_qty; //นับจำนวนรวมทั้งตะกร้า
                    sumprice += parseFloat(itemdata.sum_total);


                    html += '<tr>';
                    html += '<td class="displaynone">\n' + '<div class="tb-center">\n' + '<input type="checkbox" name="basketchks" id="basketchks" checked="checked" class="MS_input_checkbox pr_NORMAL_2191468" onclick="cal_basket_chk(this);">\n' + '<input type="hidden" name="basket_item" value="{&quot;uid&quot;:&quot;2191468&quot;,&quot;cart_id&quot;:&quot;1&quot;,&quot;cart_type&quot;:&quot;NORMAL&quot;,&quot;pack_uid&quot;:&quot;&quot;}">\n' + '<input type="hidden" name="extra_item" value="{&quot;extra_require_uid&quot;:null,&quot;extra_require&quot;:null,&quot;extra_main_brandname&quot;:&quot;&quot;}">\n' + '</div>\n' + '</td>';
                    html += '<td>\n' + '<div class="tb-center">\n' + '<div class="thumb">\n' + '<a href="#">\n' + '<img src="/public/product/' + itemdata.img + '" alt="Product thumbnail" title="Product thumbnail">\n' + '</a>\n' + '</div>\n' + '</div>\n' + '</td>';
                    html += '<td>\n' + '<div class="tb-left">\n' + '<h5 class="media-heading">\n' + '<a href="#">\n' + '<font class="name-product" style="vertical-align: inherit;">\n' + '<font style="vertical-align: inherit;">' + itemdata.product_name + '</font>\n' + '</font>\n' + '</a>\n' + '</h5>\n' + '<span class="MK-product-icons"></span>\n' + '<div id="2191468_1" class="tb-left tb-opt">\n' + '<strong>\n' + '<font style="vertical-align: inherit;">\n' + '<font style="vertical-align: inherit;"></font>\n' + '</font>\n' + '</strong>\n' + '<font style="vertical-align: inherit;">\n' + '<font style="vertical-align: inherit;"> ' + itemdata.sku_name + '</font>\n' + '</font>\n' + '</div>\n' + '<div class="uni-opt">\n' + '<a href="javascript:modify_option(\'2191468\', \'1\',\'\');" class="modify">\n' + '<font style="vertical-align: inherit;">\n' + '<font class="btn-options" hidden style="vertical-align: inherit;">pdate</font>\n' + '</font>\n' + '</a>\n' + '</div>\n' + '</div>\n' + '</td>';
                    html += '<td>\n' + '<div class="tb-center">\n' + '<font style="vertical-align: inherit;">\n' + '<font style="vertical-align: inherit;">฿' + itemdata.price_discount + '</font>\n' + '</font>\n' + '</div>\n' + '</td>';
                    html += '<td>\n' + '<div class="tb-center">\n' + '<div class="num_c">\n' + '<input type="text" name="sku_qty[]" id="id' + index + '" value="' + itemdata.sku_qty + '" class="form-control input-text qty text" readonly="readonly">\n' + '<a class="num_up" onclick="updateTotal(' + index + ')">\n' + '<i class="fa fa-caret-up"></i>\n' + '</a>\n' + '<a class="num_down" onclick="downTotal(' + index + ')">\n' + '<i class="fa fa-caret-down"></i>\n' + '</a>\n' + '</div>\n' + '<a href="#" class="update_qty btn btn-gray btn-block btn-sm"  id="' + index + '" style="margin-top: 3px;">\n' + '<i class="fa fa-repeat"></i>\n' + '<span class="letters">\n' + '<font style="vertical-align: inherit;">\n' + '<font class="{{$a}}" style="vertical-align: inherit;" onclick="updateProduct(' + index + ')">Modify</font>\n' + '</font>\n' + '</span>\n' + '</a>\n' + '</div>\n' + '</td>';
                    html += '<td>\n' + '<div class="tb-center tb-bold">\n' + '<span>\n' + '<font style="vertical-align: inherit;">\n' + '<font id="sumtotal' + index + '" class="{{$a}}" value="' + itemdata.sum_total + '" style="vertical-align: inherit;">฿ ' + itemdata.sum_total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '' + '</font>\n' + '</font>\n' + '</span>\n' + '</div>\n' + '</td>';
                    html +=
                        '<td>\n' +
                        '<div class="tb-center tb-delivery">\n' +
                        '<div class="MS_tb_delivery">\n' +
                        '<span class="MS_deli_txt" onmouseover="overcase(this, \'1\')" onmouseout="outcase(this, \'1\')">\n' +
                        '<span class="MS_deli_title MS_deli_block">\n' +
                        '<font style="vertical-align: inherit;">\n' +
                        '<font style="vertical-align: inherit;">[Free shipping with purchase over 500 BHT]</font>\n' +
                        '</font>\n' +
                        '</span>\n' +
                        '</span>\n' +
                        '</div>\n' +
                        '</div>\n' +
                        '</td>';
                    html += ' <td>\n' + '<div class="tb-center">\n' + '<span class="d-block link_del">\n' + '<a href="#" data-product_id="16828" data-product_sku="" data-toggle="tooltip" data-placement="top" data-original-title="ลบ"  id="' + index + '" class="remove btn btn-primary btn-block">\n' + '<i class="fa fa-trash-o"></i>\n' + '</a>\n' + '</span>\n' + '</div>\n' + '</td>';
                    html += '</tr>';



                    html_mobile += '<div class="col-xs-5">';
                    html_mobile += '<div class="basketLeft">';
                    html_mobile += '<a href="#">';
                    html_mobile += '<img src="/public/product/' + itemdata.img + '"" alt="상품명" class="response100">';
                    html_mobile += '</a>';
                    html_mobile += '<div class="center">';
                    html_mobile += '<div class="tb-center">\n' + '<span class="d-block link_del">\n' + '<a href="#" data-product_id="16828" data-product_sku="" data-toggle="tooltip" data-placement="top" data-original-title="ลบ"  id="' + index + '" class="remove_mobile btn btn-primary btn-block">\n' + '<i class="fa fa-trash-o"></i>\n' + '</a>\n' + '</span>\n' + '</div>\n';
                    html_mobile += '</div>';
                    html_mobile += '</div>';
                    html_mobile += '</div>';
                    html_mobile += '<div class="col-xs-7">';
                    html_mobile += '<figcaption class="basketRight">';
                    html_mobile += '<p class="pname clearFix">';
                    html_mobile += '<font class="name-product" style="vertical-align: inherit;">';
                    html_mobile += '<font style="vertical-align: inherit;">' + itemdata.product_name + '</font>';
                    html_mobile += '</font>';
                    html_mobile += '</p>';
                    html_mobile += '<div class="tb-center tb-bold">\n' + '<span>\n' + '<font style="vertical-align: inherit;">\n' + '<font id="sumtotalmobile' + index + '" class="{{$a}}" value="' + itemdata.sum_total + '" style="vertical-align: inherit;">฿ ' + itemdata.sum_total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '' + '</font>\n' + '</font>\n' + '</span>\n' + '</div>';

                    html_mobile += '</p>';
                    html_mobile += '<p>';
                    html_mobile += '<span class="num_c">';
                    html_mobile += '<div>';
                    html_mobile += '<td>\n' + '<div class="tb-center">\n' + '<div class="num_c">\n' + '<input type="text" name="sku_qty[]" id="ids' + index + '" value="' + itemdata.sku_qty + '" class="form-control input-text qty text" readonly="readonly">\n' + '<a class="num_up" onclick="updateTotal(' + index + ')">\n' + '<i class="fa fa-caret-up"></i>\n' + '</a>\n' + '<a class="num_down" onclick="downTotal(' + index + ')">\n' + '<i class="fa fa-caret-down"></i>\n' + '</a>\n' + '</div>\n' + '<a href="#" class="update_qty btn btn-gray btn-block btn-sm"  id="' + index + '" style="margin-top: 3px;">\n' + '<i class="fa fa-repeat"></i>\n' + '<span class="letters">\n' + '<font style="vertical-align: inherit;">\n' + '<font class="{{$a}}" style="vertical-align: inherit;" onclick="updateProduct(' + index + ')">Modify</font>\n' + '</font>\n' + '</span>\n' + '</a>\n' + '</div>\n' + '</td>';

                    html_mobile += '</div>';
                    html_mobile += '</span>';
                    html_mobile += '</p>';
                    html_mobile += '<p style="font-size: 12px; margin-bottom: 5px;">\n' +
                        '                        <strong><b>' + itemdata.sku_name + '</b></strong>\n' +
                        '                    </p>';
                    html_mobile += '</figcaption>';
                    html_mobile += '</div>';
                    html_mobile += '<div style="border-bottom: 1px solid #eaeaea;"></div>';

                });


                var cal = calShipping(sumprice);
                var htmlsumnumber = '';
                $("#number_sum").html("");
                htmlsumnumber = '<span id="number_sum">' + sumnumber.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.00' + '</span>'
                htmlsumnumber = '<span id="number_sum1">' + sumnumber.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.00' + '</span>'

                $("#number_sum").append(htmlsumnumber);
                $("#number_sum_mobile").append(htmlsumnumber);
                var htmlsumprice = '';
                $("#sumprice").html("");
                htmlsumprice = '<span id="sumprice">' + sumprice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.00' + '</span>'
                $("#sumprice").append(htmlsumprice);

                $("#sumprice_mobile").html("");
                htmlsumprice_mobile = '<span id="sumprice">' + cal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.00' + '</span>'
                $("#sumprice_mobile").append(htmlsumprice_mobile);


                if (cal < 500) {
                    var htmlems_sum = '';
                    $("#ems_sum").html("");
                    htmlems_sum = '<span id="ems_sum">' + 40 + '</span>'
                    $("#ems_sum").append(htmlems_sum);

                    $("#ems_sum_mobile").html("");
                    htmlems_sum_mobile = '<span id="ems_sum">' + 40 + '</span>'
                    $("#ems_sum_mobile").append(htmlems_sum_mobile);


                    var htmltotal_sum = '';
                    $("#total_sum").html("");
                    htmltotal_sum = '<span id="total_sum">' + cal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.00' + '</span>'
                    $("#total_sum").append(htmltotal_sum);

                    $("#total_sum_mobile").html("");
                    htmltotal_sum_mobile = '<span id="total_sum">' + cal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.00' + '</span>'
                    $("#total_sum_mobile").append(htmltotal_sum_mobile);

                } else if (cal > 500) {

                    var htmlems_sum = '';
                    $("#ems_sum").html("");
                    htmlems_sum = '<span id="ems_sum">' + 0 + '</span>'
                    $("#ems_sum").append(htmlems_sum);

                    $("#ems_sum_mobile").html("");
                    htmlems_sum_mobile = '<span id="ems_sum">' + 0 + '</span>'
                    $("#ems_sum_mobile").append(htmlems_sum_mobile);

                    sumtotal = sumprice;

                    var htmltotal_sum = '';
                    $("#total_sum").html("");
                    htmltotal_sum = '<span id="total_sum">' + cal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.00' + '</span>'
                    $("#total_sum").append(htmltotal_sum);

                    $("#total_sum_mobile").html("");
                    htmltotal_sum_mobile = '<span id="total_sum">' + cal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.00' + '</span>'
                    $("#total_sum_mobile").append(htmltotal_sum_mobile);
                }

                // html_mobile_button += '<div class="col-xs-12" style="margin: 10px 0 50px 0;" >';
                // html_mobile_button += '<div class="row">';
                // html_mobile_button += '<div class="col-xs-6">';
                // html_mobile_button += '<a href="javascript:basket_multidel()" class="btn btn-default btn-mobile" style="height: 4.4%!important;">\n';
                // html_mobile_button += '<span>ช้อปปิ้งต่อ</span>';
                // html_mobile_button += '</a>';
                // html_mobile_button += '</div>';
                // html_mobile_button += '<div class="col-xs-6">';
                // html_mobile_button += '<a href="javascript:basket_clear();" class="btn btn-primary btn-mobile" style="height: 4.4%!important;">\n';
                // html_mobile_button += '<span>';
                // html_mobile_button += '<i class="fa fa-trash"></i> ลบสินค้า';
                // html_mobile_button += '</span>';
                // html_mobile_button += '</a>';
                // html_mobile_button += '</div></div></div>';
                //
                // var logis = 0;
                // if(sumprice < 500){
                //     var logis = 0;
                // }
                // html_mobile_cal += '<div class="col-xs-12">';
                // html_mobile_cal += '<div class="price-total-info-top"><p class="clearFix"><span class="fl">ยอดรวมสินต้า</span>';
                // html_mobile_cal += '<span class="fr"><b>' + sumprice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.00' + ' บาท</b></span></p>';
                // html_mobile_cal += '<p class="clearFix"><span class="fl">ค่าจัดส่ง</span><span class="fr">'+ logis +' บาท</span></p>';
                // html_mobile_cal += '<p class="clearFix"><span class="fl">ยอดรวมทั้งหมด</span><span class="fr"><strong class="text-danger"><b>' + cal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.00' + ' บาท</b></strong></span></span></p>';
                // html_mobile_cal += '</div>';



                $('#product-incard').append(html);
                //   $('#testxxx').append(html);
                $("#product-incard_mobile").append(html_mobile);
                // $("#product-incard_mobile_button").append(html_mobile_button);
                // $("#product-incard_mobile_cal").append(html_mobile_cal);
            },
            error: function(qXHR, textStatus, errorThrown) {

            }
        });
    }

    $(document).on('click', '.empty', function() {


        var base_url = window.location.origin;
        var urlVerify = base_url + '/delcartall/me';
        var productitem = {
            data: 1,
        };
        axios.post(urlVerify, productitem).then(function (response) {
            $('.badge-cart').text('0');

            //    deleteincart();
            var sumnumber = 0;
            var sumprice = 0;
            var sumtotal = 0;
            var htmlsumnumber = '';
            $("#number_sum").html("");
            htmlsumnumber = '<span id="number_sum">'+sumnumber+'</span>'
            $("#number_sum").append(htmlsumnumber);
            var htmlsumprice = '';
            $("#sumprice").html("");
            htmlsumprice = '<span id="sumprice">'  + sumprice + '</span>'
            $("#sumprice").append(htmlsumprice);
            $("#total_sum").html("");
            htmlsumprice = '<span id="sumprice">'  + sumtotal + '</span>'
            $("#total_sum").append(htmlsumprice);


            if(response.status == 200){
                deleteall();
            }

            $("#sumprice_mobile").html("");
            htmlsumprice_mobile = '<span id="sumprice">' + sumprice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.00' + '</span>'
            $("#sumprice_mobile").append(htmlsumprice_mobile);

            $("#ems_sum_mobile").html("");
            htmlems_sum_mobile = '<span id="ems_sum">' + 40 + '</span>'
            $("#ems_sum_mobile").append(htmlems_sum_mobile);

            $("#total_sum_mobile").html("");
            htmltotal_sum_mobile = '<span id="sumtotal">' + cal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.00' + '</span>'
            $("#total_sum_mobile").append(htmltotal_sum_mobile);


        })["catch"](function (error) {

        });


    });

    function deleteall(){
        $.ajax({
            type: "POST",
            dataType: 'JSON',
            url: '/shop/me',
            data: {
                data: 1
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {


                var html = '';
                var html_mobile = '';
                $("#product-incard").html("");
                $("#product-incard_mobile").html("");
                $.each(data.products, function(index, itemdata) {

                    sumnumber += itemdata.sku_qty; //นับจำนวนรวมทั้งตะกร้า
                    sumprice += parseFloat(itemdata.sum_total);

                    html += '<tr>';
                    html += '<td class="displaynone">\n' + '<div class="tb-center">\n' + '<input type="checkbox" name="basketchks" id="basketchks" checked="checked" class="MS_input_checkbox pr_NORMAL_2191468" onclick="cal_basket_chk(this);">\n' + '<input type="hidden" name="basket_item" value="{&quot;uid&quot;:&quot;2191468&quot;,&quot;cart_id&quot;:&quot;1&quot;,&quot;cart_type&quot;:&quot;NORMAL&quot;,&quot;pack_uid&quot;:&quot;&quot;}">\n' + '<input type="hidden" name="extra_item" value="{&quot;extra_require_uid&quot;:null,&quot;extra_require&quot;:null,&quot;extra_main_brandname&quot;:&quot;&quot;}">\n' + '</div>\n' + '</td>';
                    html += '<td>\n' + '<div class="tb-center">\n' + '<div class="thumb">\n' + '<a href="#">\n' + '<img src="/public/product/' + itemdata.img + '" alt="Product thumbnail" title="Product thumbnail">\n' + '</a>\n' + '</div>\n' + '</div>\n' + '</td>';
                    html += '<td>\n' + '<div class="tb-left">\n' + '<h5 class="media-heading">\n' + '<a href="#">\n' + '<font class="name-product" style="vertical-align: inherit;">\n' + '<font style="vertical-align: inherit;">' + itemdata.product_name + '</font>\n' + '</font>\n' + '</a>\n' + '</h5>\n' + '<span class="MK-product-icons"></span>\n' + '<div id="2191468_1" class="tb-left tb-opt">\n' + '<strong>\n' + '<font style="vertical-align: inherit;">\n' + '<font style="vertical-align: inherit;"></font>\n' + '</font>\n' + '</strong>\n' + '<font style="vertical-align: inherit;">\n' + '<font style="vertical-align: inherit;"> ' + itemdata.sku_name + '</font>\n' + '</font>\n' + '</div>\n' + '<div class="uni-opt">\n' + '<a href="javascript:modify_option(\'2191468\', \'1\',\'\');" class="modify">\n' + '<font style="vertical-align: inherit;">\n' + '<font class="btn-options" hidden style="vertical-align: inherit;">pdate</font>\n' + '</font>\n' + '</a>\n' + '</div>\n' + '</div>\n' + '</td>';
                    html += '<td>\n' + '<div class="tb-center">\n' + '<font style="vertical-align: inherit;">\n' + '<font style="vertical-align: inherit;">฿' + itemdata.price_discount + '</font>\n' + '</font>\n' + '</div>\n' + '</td>';
                    html += '<td>\n' + '<div class="tb-center">\n' + '<div class="num_c">\n' + '<input type="text" name="sku_qty[]" id="id' + index + '" value="' + itemdata.sku_qty + '" class="form-control input-text qty text" readonly="readonly">\n' + '<a class="num_up" onclick="updateTotal(' + index + ')">\n' + '<i class="fa fa-caret-up"></i>\n' + '</a>\n' + '<a class="num_down" onclick="downTotal(' + index + ')">\n' + '<i class="fa fa-caret-down"></i>\n' + '</a>\n' + '</div>\n' + '<a href="#" class="update_qty btn btn-gray btn-block btn-sm"  id="' + index + '" style="margin-top: 3px;">\n' + '<i class="fa fa-repeat"></i>\n' + '<span class="letters">\n' + '<font style="vertical-align: inherit;">\n' + '<font class="{{$a}}" style="vertical-align: inherit;" onclick="updateProduct(' + index + ')">Modify</font>\n' + '</font>\n' + '</span>\n' + '</a>\n' + '</div>\n' + '</td>';
                    html += '<td>\n' + '<div class="tb-center tb-bold">\n' + '<span>\n' + '<font style="vertical-align: inherit;">\n' + '<font id="sumtotal' + index + '" class="{{$a}}" value="' + itemdata.sum_total + '" style="vertical-align: inherit;">฿ ' + itemdata.sum_total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '' + '</font>\n' + '</font>\n' + '</span>\n' + '</div>\n' + '</td>';
                    html +=
                        '<td>\n' +
                        '<div class="tb-center tb-delivery">\n' +
                        '<div class="MS_tb_delivery">\n' +
                        '<span class="MS_deli_txt" onmouseover="overcase(this, \'1\')" onmouseout="outcase(this, \'1\')">\n' +
                        '<span class="MS_deli_title MS_deli_block">\n' +
                        '<font style="vertical-align: inherit;">\n' +
                        '<font style="vertical-align: inherit;">[Free shipping with purchase over 500 BHT]</font>\n' +
                        '</font>\n' +
                        '</span>\n' +
                        '</span>\n' +
                        '</div>\n' +
                        '</div>\n' +
                        '</td>';
                    html += ' <td>\n' + '<div class="tb-center">\n' + '<span class="d-block link_del">\n' + '<a href="#" data-product_id="16828" data-product_sku="" data-toggle="tooltip" data-placement="top" data-original-title="ลบ"  id="' + index + '" class="remove btn btn-primary btn-block">\n' + '<i class="fa fa-trash-o"></i>\n' + '</a>\n' + '</span>\n' + '</div>\n' + '</td>';
                    html += '</tr>';


                    html_mobile += '<div class="col-xs-5">';
                    html_mobile += '<div class="basketLeft">';
                    html_mobile += '<a href="#">';
                    html_mobile += '<img src="/public/product/' + itemdata.img + '"" alt="상품명" class="response100">';
                    html_mobile += '</a>';
                    html_mobile += '<div class="center">';
                    html_mobile += '<div class="tb-center">\n' + '<span class="d-block link_del">\n' + '<a href="#" data-product_id="16828" data-product_sku="" data-toggle="tooltip" data-placement="top" data-original-title="ลบ"  id="' + index + '" class="remove_mobile btn btn-primary btn-block">\n' + '<i class="fa fa-trash-o"></i>\n' + '</a>\n' + '</span>\n' + '</div>\n';
                    html_mobile += '</div>';
                    html_mobile += '</div>';
                    html_mobile += '</div>';
                    html_mobile += '<div class="col-xs-7">';
                    html_mobile += '<figcaption class="basketRight">';
                    html_mobile += '<p class="pname clearFix">';
                    html_mobile += '<font class="name-product" style="vertical-align: inherit;">';
                    html_mobile += '<font style="vertical-align: inherit;">' + itemdata.product_name + '</font>';
                    html_mobile += '</font>';
                    html_mobile += '</p>';
                    html_mobile += '<div class="tb-center tb-bold">\n' + '<span>\n' + '<font style="vertical-align: inherit;">\n' + '<font id="sumtotalmobile' + index + '" class="{{$a}}" value="' + itemdata.sum_total + '" style="vertical-align: inherit;">฿ ' + itemdata.sum_total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '' + '</font>\n' + '</font>\n' + '</span>\n' + '</div>';

                    html_mobile += '</p>';
                    html_mobile += '<p>';
                    html_mobile += '<span class="num_c">';
                    html_mobile += '<div>';
                    html_mobile += '<td>\n' + '<div class="tb-center">\n' + '<div class="num_c">\n' + '<input type="text" name="sku_qty[]" id="ids' + index + '" value="' + itemdata.sku_qty + '" class="form-control input-text qty text" readonly="readonly">\n' + '<a class="num_up" onclick="updateTotal(' + index + ')">\n' + '<i class="fa fa-caret-up"></i>\n' + '</a>\n' + '<a class="num_down" onclick="downTotal(' + index + ')">\n' + '<i class="fa fa-caret-down"></i>\n' + '</a>\n' + '</div>\n' + '<a href="#" class="update_qty btn btn-gray btn-block btn-sm"  id="' + index + '" style="margin-top: 3px;">\n' + '<i class="fa fa-repeat"></i>\n' + '<span class="letters">\n' + '<font style="vertical-align: inherit;">\n' + '<font class="{{$a}}" style="vertical-align: inherit;" onclick="updateProduct(' + index + ')">Modify</font>\n' + '</font>\n' + '</span>\n' + '</a>\n' + '</div>\n' + '</td>';

                    html_mobile += '</div>';
                    html_mobile += '</span>';
                    html_mobile += '</p>';
                    html_mobile += '<p style="font-size: 12px; margin-bottom: 5px;">\n' +
                        '                        <strong><b>' + itemdata.sku_name + '</b></strong>\n' +
                        '                    </p>';
                    html_mobile += '</figcaption>';
                    html_mobile += '</div>';
                    html_mobile += '<div style="border-bottom: 1px solid #eaeaea;"></div>';
                });

                $('#product-incard').append(html);
                $('#product-incard_mobile').append(html_mobile);
            },
            error: function(qXHR, textStatus, errorThrown) {

            }
        });


    }





    $(document).on('click', '.remove', function() {
        var base_url = window.location.origin;
        var urlVerify = base_url + '/delcart/me';
        var button_id = $(this).attr('id');
        var button_ids = $(this).attr('ids');

        var productitem = {
            data: button_id,
        };


        var qty = document.getElementById('id' + button_id).value;
        var sum_qty = parseFloat(qty) + 1;
        document.getElementById('id' + button_id).value = sum_qty;

        // updatePrice(button_id, sum_qty, "add", qty);


        axios.post(urlVerify, productitem).then(function (response) {
            $('.badge-cart').text(response.data.sum_quantity);
            //    deleteincart();
            var sumnumber = 0;
            var sumprice = 0;
            var sumtotal = 0;
            $("#number_sum").html("");
            $("#ems_sum").html("");
            $("#sumprice").html("");
            $("#total_sum").html("");


            $.ajax({
                type: "POST",
                dataType: 'JSON',
                url: '/shop/me',
                data: {
                    data: 1
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {


                    var html = '';
                    var html_mobile = '';
                    $("#product-incard").html("");
                    $("#product-incard_mobile").html("");
                    var prouduct_alone_total = [];
                    var sku_qty_total = [];
                    $.each(data.products, function(index, itemdata) {
                        prouduct_alone_total.push(parseFloat(itemdata.sum_total));
                        sku_qty_total.push(parseFloat(itemdata.sku_qty));
                        sumnumber += itemdata.sku_qty; //นับจำนวนรวมทั้งตะกร้า
                        sumprice += parseFloat(itemdata.sum_total);

                        html += '<tr>';
                        html += '<td class="displaynone">\n' + '<div class="tb-center">\n' + '<input type="checkbox" name="basketchks" id="basketchks" checked="checked" class="MS_input_checkbox pr_NORMAL_2191468" onclick="cal_basket_chk(this);">\n' + '<input type="hidden" name="basket_item" value="{&quot;uid&quot;:&quot;2191468&quot;,&quot;cart_id&quot;:&quot;1&quot;,&quot;cart_type&quot;:&quot;NORMAL&quot;,&quot;pack_uid&quot;:&quot;&quot;}">\n' + '<input type="hidden" name="extra_item" value="{&quot;extra_require_uid&quot;:null,&quot;extra_require&quot;:null,&quot;extra_main_brandname&quot;:&quot;&quot;}">\n' + '</div>\n' + '</td>';
                        html += '<td>\n' + '<div class="tb-center">\n' + '<div class="thumb">\n' + '<a href="#">\n' + '<img src="/public/product/' + itemdata.img + '" alt="Product thumbnail" title="Product thumbnail">\n' + '</a>\n' + '</div>\n' + '</div>\n' + '</td>';
                        html += '<td>\n' + '<div class="tb-left">\n' + '<h5 class="media-heading">\n' + '<a href="#">\n' + '<font class="name-product" style="vertical-align: inherit;">\n' + '<font style="vertical-align: inherit;">' + itemdata.product_name + '</font>\n' + '</font>\n' + '</a>\n' + '</h5>\n' + '<span class="MK-product-icons"></span>\n' + '<div id="2191468_1" class="tb-left tb-opt">\n' + '<strong>\n' + '<font style="vertical-align: inherit;">\n' + '<font style="vertical-align: inherit;"></font>\n' + '</font>\n' + '</strong>\n' + '<font style="vertical-align: inherit;">\n' + '<font style="vertical-align: inherit;"> ' + itemdata.sku_name + '</font>\n' + '</font>\n' + '</div>\n' + '<div class="uni-opt">\n' + '<a href="javascript:modify_option(\'2191468\', \'1\',\'\');" class="modify">\n' + '<font style="vertical-align: inherit;">\n' + '<font class="btn-options" hidden style="vertical-align: inherit;">pdate</font>\n' + '</font>\n' + '</a>\n' + '</div>\n' + '</div>\n' + '</td>';
                        html += '<td>\n' + '<div class="tb-center">\n' + '<font style="vertical-align: inherit;">\n' + '<font style="vertical-align: inherit;">฿' + itemdata.price_discount + '</font>\n' + '</font>\n' + '</div>\n' + '</td>';
                        html += '<td>\n' + '<div class="tb-center">\n' + '<div class="num_c">\n' + '<input type="text" name="sku_qty[]" id="id' + index + '" value="' + itemdata.sku_qty + '" class="form-control input-text qty text" readonly="readonly">\n' + '<a class="num_up" onclick="updateTotal(' + index + ')">\n' + '<i class="fa fa-caret-up"></i>\n' + '</a>\n' + '<a class="num_down" onclick="downTotal(' + index + ')">\n' + '<i class="fa fa-caret-down"></i>\n' + '</a>\n' + '</div>\n' + '<a href="#" class="update_qty btn btn-gray btn-block btn-sm"  id="' + index + '" style="margin-top: 3px;">\n' + '<i class="fa fa-repeat"></i>\n' + '<span class="letters">\n' + '<font style="vertical-align: inherit;">\n' + '<font class="{{$a}}" style="vertical-align: inherit;" onclick="updateProduct(' + index + ')">Modify</font>\n' + '</font>\n' + '</span>\n' + '</a>\n' + '</div>\n' + '</td>';
                        html += '<td>\n' + '<div class="tb-center tb-bold">\n' + '<span>\n' + '<font style="vertical-align: inherit;">\n' + '<font id="sumtotal' + index + '" class="{{$a}}" value="' + itemdata.sum_total + '" style="vertical-align: inherit;">฿ ' + itemdata.sum_total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '' + '</font>\n' + '</font>\n' + '</span>\n' + '</div>\n' + '</td>';
                        html +=
                            '<td>\n' +
                            '<div class="tb-center tb-delivery">\n' +
                            '<div class="MS_tb_delivery">\n' +
                            '<span class="MS_deli_txt" onmouseover="overcase(this, \'1\')" onmouseout="outcase(this, \'1\')">\n' +
                            '<span class="MS_deli_title MS_deli_block">\n' +
                            '<font style="vertical-align: inherit;">\n' +
                            '<font style="vertical-align: inherit;">[Free shipping with purchase over 500 BHT]</font>\n' +
                            '</font>\n' +
                            '</span>\n' +
                            '</span>\n' +
                            '</div>\n' +
                            '</div>\n' +
                            '</td>';
                        html += ' <td>\n' + '<div class="tb-center">\n' + '<span class="d-block link_del">\n' + '<a href="#" data-product_id="16828" data-product_sku="" data-toggle="tooltip" data-placement="top" data-original-title="ลบ"  id="' + index + '" class="remove btn btn-primary btn-block">\n' + '<i class="fa fa-trash-o"></i>\n' + '</a>\n' + '</span>\n' + '</div>\n' + '</td>';
                        html += '</tr>';


                        html_mobile += '<div class="col-xs-5">';
                        html_mobile += '<div class="basketLeft">';
                        html_mobile += '<a href="#">';
                        html_mobile += '<img src="/public/product/' + itemdata.img + '"" alt="상품명" class="response100">';
                        html_mobile += '</a>';
                        html_mobile += '<div class="center">';
                        html_mobile += '<div class="tb-center">\n' + '<span class="d-block link_del">\n' + '<a href="#" data-product_id="16828" data-product_sku="" data-toggle="tooltip" data-placement="top" data-original-title="ลบ"  id="' + index + '" class="remove_mobile btn btn-primary btn-block">\n' + '<i class="fa fa-trash-o"></i>\n' + '</a>\n' + '</span>\n' + '</div>\n';
                        html_mobile += '</div>';
                        html_mobile += '</div>';
                        html_mobile += '</div>';
                        html_mobile += '<div class="col-xs-7">';
                        html_mobile += '<figcaption class="basketRight">';
                        html_mobile += '<p class="pname clearFix">';
                        html_mobile += '<font class="name-product" style="vertical-align: inherit;">';
                        html_mobile += '<font style="vertical-align: inherit;">' + itemdata.product_name + '</font>';
                        html_mobile += '</font>';
                        html_mobile += '</p>';
                        html_mobile += '<div class="tb-center tb-bold">\n' + '<span>\n' + '<font style="vertical-align: inherit;">\n' + '<font id="sumtotalmobile' + index + '" class="{{$a}}" value="' + itemdata.sum_total + '" style="vertical-align: inherit;">฿ ' + itemdata.sum_total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '' + '</font>\n' + '</font>\n' + '</span>\n' + '</div>';

                        html_mobile += '</p>';
                        html_mobile += '<p>';
                        html_mobile += '<span class="num_c">';
                        html_mobile += '<div>';
                        html_mobile += '<td>\n' + '<div class="tb-center">\n' + '<div class="num_c">\n' + '<input type="text" name="sku_qty[]" id="ids' + index + '" value="' + itemdata.sku_qty + '" class="form-control input-text qty text" readonly="readonly">\n' + '<a class="num_up" onclick="updateTotal(' + index + ')">\n' + '<i class="fa fa-caret-up"></i>\n' + '</a>\n' + '<a class="num_down" onclick="downTotal(' + index + ')">\n' + '<i class="fa fa-caret-down"></i>\n' + '</a>\n' + '</div>\n' + '<a href="#" class="update_qty btn btn-gray btn-block btn-sm"  id="' + index + '" style="margin-top: 3px;">\n' + '<i class="fa fa-repeat"></i>\n' + '<span class="letters">\n' + '<font style="vertical-align: inherit;">\n' + '<font class="{{$a}}" style="vertical-align: inherit;" onclick="updateProduct(' + index + ')">Modify</font>\n' + '</font>\n' + '</span>\n' + '</a>\n' + '</div>\n' + '</td>';

                        html_mobile += '</div>';
                        html_mobile += '</span>';
                        html_mobile += '</p>';
                        html_mobile += '<p style="font-size: 12px; margin-bottom: 5px;">\n' +
                            '                        <strong><b>' + itemdata.sku_name + '</b></strong>\n' +
                            '                    </p>';
                        html_mobile += '</figcaption>';
                        html_mobile += '</div>';
                        html_mobile += '<div style="border-bottom: 1px solid #eaeaea;"></div>';
                    });

                    var sum = prouduct_alone_total.reduce(function(a, b){
                        return a + b;
                    });
                    var sum_qty_total = sku_qty_total.reduce(function(a, b){
                        return a + b;
                    });
                    var cal = calShipping(sum);



                    var htmlsumnumber = '';
                    $("#number_sum").html("");
                    htmlsumnumber = '<span id="number_sum">' + sum_qty_total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.00' + '</span>'
                    $("#number_sum").append(htmlsumnumber);
                    var htmlsumprice = '';
                    $("#sumprice").html("");
                    htmlsumprice = '<span id="sumprice">' + sumprice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.00' + '</span>'
                    $("#sumprice").append(htmlsumprice);

                    $("#sumprice_mobile").html("");
                    htmlsumprice_mobile = '<span id="sumprice">' + sumprice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.00' + '</span>'
                    $("#sumprice_mobile").append(htmlsumprice_mobile);

                    if (cal < 500) {
                        var htmlems_sum = '';
                        $("#ems_sum").html("");
                        htmlems_sum = '<span id="ems_sum">' + 40 + '</span>'
                        $("#ems_sum").append(htmlems_sum);

                        $("#ems_sum_mobile").html("");
                        htmlems_sum_mobile = '<span id="ems_sum">' + 40 + '</span>'
                        $("#ems_sum_mobile").append(htmlems_sum_mobile);

                        sumtotal = sumprice + 40;

                        var htmltotal_sum = '';
                        $("#total_sum").html("");
                        htmltotal_sum = '<span id="total_sum">' + cal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.00' + '</span>'
                        $("#total_sum").append(htmltotal_sum);

                        $("#total_sum_mobile").html("");
                        htmltotal_sum_mobile = '<span id="total_sum">' + cal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.00' + '</span>'
                        $("#total_sum_mobile").append(htmltotal_sum_mobile);



                    } else if (cal > 500) {

                        var htmlems_sum = '';
                        $("#ems_sum").html("");
                        htmlems_sum = '<span id="ems_sum">' + 0 + '</span>'
                        $("#ems_sum").append(htmlems_sum);

                        $("#ems_sum_mobile").html("");
                        htmlems_sum_mobile = '<span id="ems_sum">' + 0 + '</span>'
                        $("#ems_sum_mobile").append(htmlems_sum_mobile);

                        var htmltotal_sum = '';
                        $("#total_sum").html("");
                        htmltotal_sum = '<span id="total_sum">' + cal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.00' + '</span>'
                        $("#total_sum").append(htmltotal_sum);

                        $("#total_sum_mobile").html("");
                        htmltotal_sum_mobile = '<span id="total_sum">' + cal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.00' + '</span>'
                        $("#total_sum_mobile").append(htmltotal_sum_mobile);
                    }
                    $('#product-incard').append(html);
                    $("#product-incard_mobile").append(html_mobile);
                },
                error: function(qXHR, textStatus, errorThrown) {

                }
            });
        })["catch"](function (error) {

        });
    });


    $(document).on('click', '.remove_mobile', function() {
        var base_url = window.location.origin;
        var urlVerify = base_url + '/delcart/me';
        var button_ids = $(this).attr('id');

        var productitem = {
            data: button_ids,
        };




        axios.post(urlVerify, productitem).then(function (response) {

            $('.badge-cart').text(response.data.sum_quantity);
            //    deleteincart();
            var sumnumber = 0;
            var sumprice = 0;
            var sumtotal = 0;
            $("#number_sum").html("");
            $("#ems_sum").html("");
            $("#sumprice").html("");
            $("#total_sum").html("");


            $.ajax({
                type: "POST",
                dataType: 'JSON',
                url: '/shop/me',
                data: {
                    data: 1
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {

                    var html = '';
                    var html_mobile = '';
                    $("#product-incard").html("");
                    $("#product-incard_mobile").html("");
                    var prouduct_alone_total = [];
                    var sku_qty_total = [];
                    $.each(data.products, function(index, itemdata) {
                        prouduct_alone_total.push(parseFloat(itemdata.sum_total));
                        sku_qty_total.push(parseFloat(itemdata.sku_qty));
                        sumnumber += itemdata.sku_qty; //นับจำนวนรวมทั้งตะกร้า
                        sumprice += parseFloat(itemdata.sum_total);




                        html_mobile += '<div class="col-xs-5">';
                        html_mobile += '<div class="basketLeft">';
                        html_mobile += '<a href="#">';
                        html_mobile += '<img src="/public/product/' + itemdata.img + '"" alt="상품명" class="response100">';
                        html_mobile += '</a>';
                        html_mobile += '<div class="center">';
                        html_mobile += '<div class="tb-center">\n' + '<span class="d-block link_del">\n' + '<a href="#" data-product_id="16828" data-product_sku="" data-toggle="tooltip" data-placement="top" data-original-title="ลบ"  id="' + index + '" class="remove_mobile btn btn-primary btn-block">\n' + '<i class="fa fa-trash-o"></i>\n' + '</a>\n' + '</span>\n' + '</div>\n';
                        html_mobile += '</div>';
                        html_mobile += '</div>';
                        html_mobile += '</div>';
                        html_mobile += '<div class="col-xs-7">';
                        html_mobile += '<figcaption class="basketRight">';
                        html_mobile += '<p class="pname clearFix">';
                        html_mobile += '<font class="name-product" style="vertical-align: inherit;">';
                        html_mobile += '<font style="vertical-align: inherit;">' + itemdata.product_name + '</font>';
                        html_mobile += '</font>';
                        html_mobile += '</p>';
                        html_mobile += '<div class="tb-center tb-bold">\n' + '<span>\n' + '<font style="vertical-align: inherit;">\n' + '<font id="sumtotalmobile' + index + '" class="{{$a}}" value="' + itemdata.sum_total + '" style="vertical-align: inherit;">฿ ' + itemdata.sum_total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '' + '</font>\n' + '</font>\n' + '</span>\n' + '</div>';

                        html_mobile += '</p>';
                        html_mobile += '<p>';
                        html_mobile += '<span class="num_c">';
                        html_mobile += '<div>';
                        html_mobile += '<td>\n' + '<div class="tb-center">\n' + '<div class="num_c">\n' + '<input type="text" name="sku_qty[]" id="ids' + index + '" value="' + itemdata.sku_qty + '" class="form-control input-text qty text" readonly="readonly">\n' + '<a class="num_up" onclick="updateTotal(' + index + ')">\n' + '<i class="fa fa-caret-up"></i>\n' + '</a>\n' + '<a class="num_down" onclick="downTotal(' + index + ')">\n' + '<i class="fa fa-caret-down"></i>\n' + '</a>\n' + '</div>\n' + '<a href="#" class="update_qty btn btn-gray btn-block btn-sm"  id="' + index + '" style="margin-top: 3px;">\n' + '<i class="fa fa-repeat"></i>\n' + '<span class="letters">\n' + '<font style="vertical-align: inherit;">\n' + '<font class="{{$a}}" style="vertical-align: inherit;" onclick="updateProduct(' + index + ')">Modify</font>\n' + '</font>\n' + '</span>\n' + '</a>\n' + '</div>\n' + '</td>';

                        html_mobile += '</div>';
                        html_mobile += '</span>';
                        html_mobile += '</p>';
                        html_mobile += '<p style="font-size: 12px; margin-bottom: 5px;">\n' +
                            '                        <strong><b>' + itemdata.sku_name + '</b></strong>\n' +
                            '                    </p>';
                        html_mobile += '</figcaption>';
                        html_mobile += '</div>';
                        html_mobile += '<div style="border-bottom: 1px solid #eaeaea;"></div>';
                    });

                    var sum = 0;
                    var sum_qty_total = 0;
                    if(prouduct_alone_total.length > 0){
                        var sum = prouduct_alone_total.reduce(function(a, b){
                            return a + b;
                        });
                    }
                    if(sku_qty_total.length > 0){
                        var sum_qty_total = sku_qty_total.reduce(function(a, b){
                            return a + b;
                        });
                    }
                    var cal = calShipping(sum);

                    $("#sumprice_mobile").html("");
                    htmlsumprice_mobile = '<span id="sumprice">' + sumprice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.00' + '</span>'
                    $("#sumprice_mobile").append(htmlsumprice_mobile);

                    if (cal < 500) {
                        var htmlems_sum = '';


                        $("#ems_sum_mobile").html("");
                        htmlems_sum_mobile = '<span id="ems_sum">' + 40 + '</span>'
                        $("#ems_sum_mobile").append(htmlems_sum_mobile);

                        sumtotal = sumprice + 40;

                        var htmltotal_sum = '';


                        $("#total_sum_mobile").html("");
                        htmltotal_sum_mobile = '<span id="total_sum">' + cal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.00' + '</span>'
                        $("#total_sum_mobile").append(htmltotal_sum_mobile);



                    } else if (cal > 500) {

                        $("#ems_sum_mobile").html("");
                        htmlems_sum_mobile = '<span id="ems_sum">' + 0 + '</span>'
                        $("#ems_sum_mobile").append(htmlems_sum_mobile);



                        $("#total_sum_mobile").html("");
                        htmltotal_sum_mobile = '<span id="total_sum">' + cal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.00' + '</span>'
                        $("#total_sum_mobile").append(htmltotal_sum_mobile);
                    }
                    $('#product-incard').append(html);
                    $("#product-incard_mobile").append(html_mobile);
                },
                error: function(qXHR, textStatus, errorThrown) {

                }
            });
        })["catch"](function (error) {

        });
    });



});
