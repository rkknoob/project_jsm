<?php

return [

	'product_menu_All' => 'ALL PRODUCTS',
	'product_menu_Best' => 'BEST SELLER',
	'product_menu_Base' => 'BASE MAKE UP',
	'product_menu_lip' => 'LIP',
	'product_menu_eye' => 'EYE',
	'product_menu_skincare' => 'SKINCARE',
	'product_menu_tool' => 'Tool',


    /*-----------frontindex---------------*/
    'langth' => 'Thai',
    'langen' => 'English',
    'order' => 'ORDER',
    'login' => 'LOGIN',
    'logout' => 'Logout',
    'language' => 'LANGUAGE',
    'mypage' => 'MY PAGE',
    'join' => 'JOIN',
    'event' => 'EVENT',
    'product' => 'PRODUCT',


    /*-----------Brand jsm---------------*/
    'concept' => 'Concept',
    'film' => 'Film',
    'magazine' => 'Magazine',
    'introduction' => 'Introduction',
    'media' => 'Media',
    'baht' => 'Baht',

    /*--- Page Login ---*/
    'page_login' => 'Login',
    'page_sublogin' => 'Enter the ID and password in a case-sensitive case.',
    'page_login_user' => 'ID / E-MAIL',
    'page_login_pass' => 'PASSWORD',
    'page_login_find' => 'Find ID/Password',
    'page_login_regis' => 'New Member Registration',
    'page_login_order' => 'Order/Delivery inquiry',
    'page_login_save'=> 'Save ID',

    /*--- Page Cart ---*/
    'page_cart' => 'CART',
    'page_cart_basket' => 'Shopping Basket',
    'page_cart_payment' => 'Order / Payment',
    'page_cart_order' => 'Order Completed',
    'page_cart_title' => 'Shopping Cart Products',
    'page_cart_photo' => 'Photo',
    'page_cart_product' => 'Product Name',
    'page_cart_quantity' => 'Quantity',
    'page_cart_unit' => 'Unit Price',
    'page_cart_price' => 'Price',
    'page_cart_shipping' => 'Shipping Fee',
    'page_cart_cancel' => 'Cancel',
    'page_cart_options' => 'Change/add options',
    'page_cart_modified' => 'Modified',
    'page_cart_remove' => 'Remove this item',
    'page_cart_amount' => 'Merchandise Subtotal',
    'page_cart_amount_sum' => 'Total',
    'page_cart_amount_ems' => 'Shipping Fee',
    'page_cart_items' => 'items',
    'page_cart_empty' => 'Empty All',
    'page_cart_continue' => 'Continue Shopping',
    'page_cart_place' => 'Place An Order',
    'page_cart_baht' => 'Baht',

    /*--- Page Checkout ---*/
    'page_checkout' => 'CHECKOUT',
    'page_checkout_title' => 'Products to order',
    'page_checkout_pieces' => 'Pieces',
    'page_checkout_purchase' => 'Purchase Product Name',
    'page_checkout_unit' => 'Unit Price',
    'page_checkout_quantity' => 'Quantity',
    'page_checkout_price' => 'Price',
    'page_checkout_order' => 'Order Information', 
    'page_checkout_mail' => 'E-mail', 
    'page_checkout_phone' => 'Cell Phone', 
    'page_checkout_firstname' => 'First name', 
    'page_checkout_lastname' => 'Last name', 
    'page_checkout_address' => 'Address', 
    'page_checkout_province' => 'Province', 
    'page_checkout_district' => 'District', 
    'page_checkout_subdistrict' => 'Sub-District', 
    'page_checkout_code' => 'Postal code', 


    /*--- Page Product ---*/
    'page_product_price' => 'Price',
    'page_product_sales' => 'Sales Price',   
    'page_product_amount' => 'Total Purchase Amount',
    'page_product_buy' => 'BUY NOW',
    'page_product_cart' => 'CART',  








    
  
    

 



    
   

    





    
    




    









];
