<?php

return [
    /*---------- --------------*/
	'product_menu_All' => 'รวมสินค้า',
	'product_menu_Best' => 'สินค้าขายดี',
	'product_menu_Base' => 'เบซ',
	'product_menu_lip' => 'ลิป',
	'product_menu_eye' => 'อาย',
	'product_menu_skincare' => 'สกินแคร',

    /*---------- Header --------------*/
	'menu_login' => 'ล็อกอิน',
    'menu_join' => 'เข้าร่วม',
    'menu_order' => 'รายการสั่ง',
    'menu_th' => 'ไทย',
    'menu_en' => 'อังกฤษ',

    /*---------- Footer --------------*/
    /*-----------JUNGSAEMMOOL---------*/
    'menu_jungsaemmool' => 'JUNGSAEMMOOL',
    'menu_brand' => 'แบน',
    'menu_artist' => 'อาร์ติสท์',
    'menu_artist_tip' => 'อาร์ติสท์ทิป',
    'menu_find' => 'ฟิด',

    /*-------------Lounge------------------*/
    'menu_lounge' => 'ทูล',
    'menu_event' => 'ทูล',
    'menu_linestory' => 'ทูล',


    /*-------------Contact------------------*/
    'menu_contact' => 'ทูล',
    'menu_faq' => 'ทูล',
    'menu_not' => 'ทูล',
    'menu_qat' => 'ทูล',
    'menu_review' => 'ทูล',

    /*--------------หน้าหลัด-----------------------*/

    'langth' => 'ไทย',
    'langen' => 'อังกฤษ',
    'login' => 'ล็อคอิน',
    'logout' => 'ล็อคเอาท์',
    'mypage' => 'หน้าผู้ใช้งาน',
    'language' => 'ภาษา',

    'order' => 'รายการสั่งซื้อ',
    'join' => 'เข้าร่วม',

    'event' => 'EVENT',
    'product' => 'PRODUCT',

    /*------------หน้า brand jsm*/
    'concept' => 'Concept',
    'film' => 'Film',
    'magazine' => 'Magazine',
    'introduction' => 'Introduction',
    'media' => 'Media',
    'baht' => 'บาท',

    /*--- Page Login ---*/
    'page_login' => 'เข้าสู่ระบบ',
    'page_sublogin' => 'กรุณากรอกชื่อผู้ใช้และรหัสผ่านให้ถูกต้อง',
    'page_login_user' => 'ชื่อผู้ใช้งาน / อีเมล',
    'page_login_pass' => 'รหัสผ่าน',
    'page_login_find' => 'ลืมรหัสผ่าน',
    'page_login_regis' => 'สมัครสมาชิกใหม่',
    'page_login_order' => 'สั่งซื้อ/รายละเอียดการจัดส่งสินค้า',
    'page_login_save' => 'บันทึกผู้ใช้งาน',

    /*--- Page Cart ---*/
   'page_cart' => 'ตะกร้าสินค้า',
   'page_cart_basket' => 'ตะกร้าสินค้า',
   'page_cart_payment' => 'สั่งซื้อ / ชําระเงิน',
   'page_cart_order' => 'การสั่งซื้อสำเร็จ',
   'page_cart_title' => 'ตะกร้าสินค้า',
   'page_cart_photo' => 'รูปภาพ',
   'page_cart_product' => 'ชื่อสินค้า',
   'page_cart_quantity' => 'จำนวน',
   'page_cart_unit' => 'ราคาต่อหน่วย',
   'page_cart_price' => 'ราคา',
   'page_cart_shipping' => 'ค่าจัดส่ง',
   'page_cart_cancel' => 'ยกเลิก',
   'page_cart_options' => 'เปลี่ยน/ เพิ่มเฉดสี',
   'page_cart_modified' => 'แก้ไข',
   'page_cart_remove' => 'ลบรายการนี้',
   'page_cart_amount' => 'ยอดรวมสินค้า',
   'page_cart_amount_sum' => 'ยอดรวมทั้งหมด',
   'page_cart_amount_ems' => 'ค่าจัดส่ง',
   'page_cart_items' => 'ชิ้น',
   'page_cart_empty' => 'ลบรายการสั่งซื้อ',
   'page_cart_continue' => 'ช้อปปิ้งต่อ',
   'page_cart_place' => 'สั่งซื้อสินค้า',
   'page_cart_baht' => 'บาท',

   /*--- Page Checkout ---*/
   'page_checkout' => 'รายการสินค้า',
   'page_checkout_title' => 'รายการสินค้าทั้งหมด',
   'page_checkout_pieces' => 'ชิ้น',
   'page_checkout_purchase' => 'สินค้าที่ซื้อ',
   'page_checkout_unit' => 'ราคาต่อหน่วย',
   'page_checkout_quantity' => 'จำนวน',
   'page_checkout_price' => 'ราคา',
   'page_checkout_order' => ' ข้อมูลการสั่งซื้อ',     
   'page_checkout_mail' => 'อีเมล', 
   'page_checkout_phone' => 'เบอร์โทรศัพท์', 
   'page_checkout_firstname' => 'ชื่อ', 
   'page_checkout_lastname' => 'นามสกุล', 
   'page_checkout_address' => 'ที่อยู่', 
   'page_checkout_province' => 'จังหวัด', 
   'page_checkout_district' => 'อำเภอ', 
   'page_checkout_subdistrict' => 'ตำบล', 
   'page_checkout_code' => 'รหัสไปรษณีย์', 

    /*--- Page Product ---*/
    'page_product_price' => 'ราคา',
    'page_product_sales' => 'ราคาขาย',
    'page_product_amount' => 'ยอดรวมสินค้า',
    'page_product_buy' => 'ซื้อเลย',
    'page_product_cart' => 'ตะกร้าสินค้า',


  



   

  








];
