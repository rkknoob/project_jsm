@extends('layouts.template.frontend')

<style type="text/css">

    @font-face {
        font-family: SukhumvitSet;
        src: url('/fonts/SukhumvitSet-Thin.ttf');
    }
    .text-thai {
        font-family: 'SukhumvitSet';
        font-style: normal;
        font-weight: bold;
    }
    .title-login{
        font-family: "Exo2Bold";
        margin-top: 4px;
        margin-bottom: 17px;
        font-size: 26px;
    }
    #ju-container {
        padding: 50px 0 !important;
    }
    #ju-container .ju-page-title {
        text-align: center;
        margin-bottom: 40px;
    }
    .h_msg{
        font-family: "Exo2Regular";
        letter-spacing: 0px;
    }
    .letters{
        font-family: "Exo2Regular";
        letter-spacing: 0px !important;
    }
    .btn-social{
        font-family: "Exo2Regular";
        width: 100%;
        text-align: center;
        height: 7%;
        margin-top: 3%;
        padding: 0px 8px;
        color: #fff;
        background: #292828;
        border: 1px solid #292828;
    }
    .btnlogin {
        width: 100%;
        text-align: center;
        padding: 12px 15px;
        font-weight: bold;
        color: #fff;
        background: #292828;
        border: 1px solid #292828;
        font-family: "Exo2Regular";
    }
    .u_menu>li {
        font-family: "Exo2Regular";
        position: relative;
        display: inline-block;
        padding: 0 3px;
    }
    .u_menu>li>a {
        color: #333;
    }
    .well.well-white {
        border-top: 2px solid #333;
        border-bottom: 2px solid #333;
        border-radius: 0px !important;
    }
    .form-control {
        display: block;
        width: 100%;
        height: 37px !important;
        padding: 6px 6px 6px 115px ! important;
        font-size: 14px;
        line-height: 1.65;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        border-radius: 0;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        -webkit-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    }
    input:invalid,
    textarea:invalid {
        background-color: #fff !important;
        border-radius: 0 !important;
        box-shadow: inset 0 0px 0px rgba(0, 0, 0, .075), 0 0 0px rgba(102, 175, 233, .6) !important;
    }
    input:valid,
    textarea:valid {
        background-color: #fff !important;
        border-radius: 0 !important;
        box-shadow: inset 0 0px 0px rgba(0, 0, 0, .075), 0 0 0px rgba(102, 175, 233, .6) !important;
    }
    .form-control:focus {
        border-color: #e51a94 !important;
        -webkit-box-shadow: none;
    }
    .text--email {
        font-family: "Exo2Regular";
        margin-top: -28px;
        margin-left: 10px;
        position: absolute;
        color: #ccc !important;
    }
    a:hover {
        color: #e51a94 !important;
    }

    /* Horizontal 6/7/8 Plus*/
    @media screen and (max-width: 736px) {
        #content {
            width: 100%;
        }
        #ju-container .ju-page-title {
            margin-top: 10px !important;
            text-align: center;
            margin-bottom: 40px;
        }

    }
    @media screen and (min-width: 501px) {
        .type_mobile{
            display:none!important;
        }
    }

    @media screen and (max-width: 500px) {
        .type_mobile {
            display: block!important;
        }
        .type_desktop{
            display: none!important;
        }
        #ju-container {
            padding-top: 13px!important;
        }
        h1.entry-title {
            margin-bottom: 21px !important;
        }
        .text-muted{
            font-size: 12.6px;
        }
        .form-control {
            padding: 6px 12px!important;;
        }
        .form-control:focus {
            border-color: orange !important;
            -webkit-box-shadow: none;
        }
    }
</style>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title> JUNG SAEM MOOL | Cosmetics - Official Site </title>
    <meta name="description" content="">
    <meta name="keywords" content="">
</head>


@php

    $datas = \App\CoreFunction\Helper::menufront();
    $locale = session()->get('locale');
    if($locale == 'th'){
        $a = 'text-thai';
    }else{
        $a = '';
    }
@endphp

@section('content')
<div id="ju-container">
    <div id="ju-content" class="container">
        <div class="ju-page-title">
            <h1 class="entry-title">My Page</h1>
            <p class="h_msg text-center">My Page</p>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <h3 class="entry-title title-login {{$a}}">@lang('lang.page_login')</h3>
                    <div class="well well-white">
                        <p class="text-muted letters {{$a}}">@lang('lang.page_sublogin')</p>
                        <form class="user" id="FormLogin">
                            {{ csrf_field() }}
                            <div id="content">
                                <div class="form-group">
                                    <div class="row text-left">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input id="email" type="email"
                                                class="form-control @error('email') is-invalid @enderror" name="email"
                                                value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="@lang('lang.page_login_user')">
                                            <span class="text--email type_desktop {{$a}}">@lang('lang.page_login_user')</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group text-left">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input id="pass" type="password"
                                                class="form-control @error('password') is-invalid @enderror" name="password"
                                                required autocomplete="current-password" placeholder="@lang('lang.page_login_pass')">
                                            <span class="text--email type_desktop {{$a}}">@lang('lang.page_login_pass')</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button type="button" name="submit" id="submit"
                                                class="button btnlogin btn-save {{$a}}">@lang('lang.page_login')</button>
                                        </div>
                                    </div>
                                </div>
                                <hr class="">
                                <div id="content" class="text-center col-12" style="color:#fff">
                                    <ul class="u_menu {{$a}}">
                                        <li>
                                            <a href="/resetpassword">
                                                <i class="fa fa-lock"></i>
                                                <span class="{{$a}}">@lang('lang.page_login_find')</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/register">
                                                <i class="fa fa-user-plus"></i>
                                                <span class="{{$a}}">@lang('lang.page_login_regis')</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-truck"></i>
                                                <span class="{{$a}}">@lang('lang.page_login_order')</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="form-group" style="margin-top: 20px;">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-6 center">
                                        <a href="{{ url('auth/login/facebook') }}">
                                            <button type="button" class="button btn-social">
                                                <img src="{!!asset('jsmbeauty/src/Icon/icon_facebook.png')!!}" alt="" style="width: 25px; margin-right: 5%;">
                                                Login with Facebook
                                            </button>
                                        </a>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-6 center">
                                            <button type="button" class="button btn-social">
                                                <img src="{!!asset('jsmbeauty/src/Icon/icon_google.png')!!}" alt="" style="width: 25px; margin-right: 5%;">
                                                Login with Google
                                            </button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-6 type_desktop" style="margin-top: 5%;">
                    <a href="#">
                        <img src="{!!asset('jsmbeauty/src/login_banner2.jpg')!!}" alt="" style="margin: auto; display: block">
                    </a>
                </div>
            </div>
        </div>

        <br><br><br>
    </div>
</div>



<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.js"></script>
<script>
    if (!window.moment) {
        document.write('<script src="assets/plugins/moment/moment.min.js"><\/script>');
    }
</script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.5/daterangepicker.css" rel="stylesheet">
<script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.js"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('body').on('click', '.btn-save', function(e) {
        var email = $('#email').val();
        var password = $('#pass').val();

        if ((email == '') || (password == '')) {
            swal('เข้าสู่ระบบ', 'ชื่อผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง', 'error');
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: {
                email: email,
                password: password
            },
            url: '/index/checkLogin',
            success: function(res) {

                if (res == 0) {
                    swal('Login', 'Username and password incorrect', 'error');
                } else if (res == 501) {
                    swal('Please', 'You need to confirm your account', 'warning');
                } else {
                    window.location = "{{url('/')}}";
                }
            }
        })
    });
</script>
@endsection
