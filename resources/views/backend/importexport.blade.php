@extends('backend.layouts.content')

@section('content')
<!DOCTYPE html>
<html>
<head>
    <title>Import Stock</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
</head>
<body>
	<div class="container">

    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message')}}
        </div>
    @endif

    <div class="card bg-light mt-3">
       
        <div class="card-header">
            Import Stock Excel
        </div>
        <div class="card-body">
            <form action="{{ route('import') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="file" name="file" class="btn btn-secondary">
                <br>
                <button style="margin-top:20px;" class="btn btn-success">Import Stock</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>

@endsection