@extends('backend.layouts.content')

@section('content')
    <style>
        .btn-icon-split .icon {
            padding-top: 5%;
        }
        .box-footer{
            clear:both;
            margin-top: 15px;
            text-align : left;
            padding-top: 20px;
            padding-bottom: 15px;
        }
        .note-popover{
            display:none;
        }
        .padding-zero{
            padding-left: 0px!important;
        }
        .profile-image{
            width: 250px;
            padding-top: 25px;
            margin-bottom: 25px;
        }
        .table{
            width: 0pt !important;
        }
        .full-ta{
            width: 100%!important;
        }

        #page_navigation {
            clear:both;
            margin: 20px 0;
        }
        #page_navigation a{
            padding:3px 6px;
            border:1px solid #2e6da4;
            margin:2px;
            color:black;
            text-decoration:none
        }

        .box {
            float: left;
            padding: 50px 0px;
        }

        .clearfix::after {
            clear: both;
            display: table;
        }

        .options {
            margin: 5px 0px 0px 0px;
            float: left;
        }

        .pagination {
            float: right;
        }

        .pagination a {
            color: black;
            float: left;
            padding: 8px 16px;
            text-decoration: none;
            transition: background-color .3s;
            border: 1px solid #ddd;
            margin: 0 4px;
        }

        .pagination a.active {
            background-color: #4CAF50;
            color: white;
            border: 1px solid #4CAF50;
        }
        .pagination a:hover:not(.active) {
            background-color: #ddd;
        }

    </style>


    <div>

        <!-- Page Heading -->
        @if($status == 'edit')
            <h1 class="h3 mb-4 text-gray-800">Edit Product Form</h1>
        @else
            <h1 class="h3 mb-4 text-gray-800">Add Product Form</h1>
        @endif

        <div class="card shadow col-md-12" style="margin-bottom: 7%;">
            <div class="collapse show" id="collapseCardExample" >
                <div class="card-body">

                    <form name="myForm" action="#" id="productForm" onsubmit="return validateForm()" method="post">
                        <input type="hidden" name="_method" value="PUT">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        @if($status == 'edit')
                            <input type="hidden" name="id" id="id" value="{{$product->id}}">
                            <input type="hidden" name="image" id="image" value="{{$product->cover_img}}">
                            <input type="hidden" name="imagezoom" id="imagezoom" value="{{$product->cover_zoom}}">
                            <input type="hidden" name="multifile" id="multifile" value="{{$multifile}}">
                            <input type="hidden" name="multifile2" id="multifile2" value="{{$multifile2}}">
                            <input type="hidden" name="status" id="status" value="{{$status}}">
                            <input type="hidden" name="deletefile_name" id="deletefile_name" value="">
                            <input type="hidden" name="deletefile_id" id="deletefile_id" value="">

                            <input type="hidden" name="deletefile_name_th" id="deletefile_name_th" value="">
                            <input type="hidden" name="deletefile_id_th" id="deletefile_id_th" value="">



                        @else
                            <input type="hidden" name="id" value="">
                            <input type="hidden" name="image" id="image" value="">
                            <input type="hidden" name="imagezoom" id="imagezoom" value="">
                            <input type="hidden" name="multifile" id="multifile" value="">
                            <input type="hidden" name="multifile2" id="multifile2" value="">
                            <input type="hidden" name="status" id="status" value="{{$status}}">
                            <input type="hidden" name="deletefile_name" value="">
                            <input type="hidden" name="deletefile_id" value="">
                        @endif

                        <div class="box">
                            {{ csrf_field() }}
                            <div class="box-body col-md-12">

                                <div class="form-group">
                                    <label for="fname"><b>Name</b><font color="red">*</font></label>
                                    @if($status == 'edit')
                                        <input type="text" class="form-control" name="name_en" id="name_en" placeholder="Enter Name" value="{{$product->name_en}}">
                                    @else
                                        <input type="text" class="form-control" name="name_en" id="name_en" placeholder="Enter Name" value="">
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="price"><b>Price</b><font color="red">*</font></label>
                                    @if($status == 'edit')
                                        <input type="number" class="form-control" name="price" id="price"placeholder="Enter Price" value="{{$product->price}}">
                                    @else
                                        <input type="number" class="form-control" name="price" id="price"placeholder="Enter Price">
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="price"><b>Size</b></label>
                                    @if($status == 'edit')
                                        <input type="text" class="form-control" name="size" id="size"placeholder="Enter Size" value="{{$product->size}}">
                                    @else
                                        <input type="text" class="form-control" name="size" id="size"placeholder="Enter Size">
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="category"><b>Category</b><font color="red">*</font></label>
                                    <select class="form-control" name="idCategory" id="idCategory">
                                        <option value="">--- Choose A Category ---</option>
                                        @foreach($itemCategory as $category)
                                            <option value="{{$category->id}}" @if($status =="edit"){{ $category->id==$product->category_id ? 'selected' : '' }} @endif> {{$category->name}} </option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="is_active"><b>Status</b><font color="red">*</font></label>
                                    <select class="form-control" name="is_active" id="is_active">
                                        <option value="">--- Choose A Status ---</option>
                                        @if($status == 'edit')
                                            <option value="Y"@if($product->is_active=="Y"){{'selected'}}@endif>Yes</option>
                                            <option value="N"@if($product->is_active=="N"){{'selected'}}@endif>No</option>
                                        @else
                                            <option value="Y">Yes</option>
                                            <option value="N">No</option>
                                        @endif
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="is_active"><b>Display</b><font color="red">*</font></label>
                                    <select class="form-control" name="display_type" id="display_type">
                                        <option value="">--- Choose A Status ---</option>
                                        @if($status == 'edit')
                                            <option value="Y"@if($product->display_type=="Y"){{'selected'}}@endif>Yes</option>
                                            <option value="N"@if($product->display_type=="N"){{'selected'}}@endif>No</option>
                                        @else
                                            <option value="Y">Yes</option>
                                            <option value="N">No</option>
                                        @endif
                                    </select>
                                </div>


                                <div class="form-group">
                                    <label for="is_active"><b>Best Seller</b><font color="red">*</font></label>
                                    <select class="form-control" name="best" id="best">
                                        <option value="">--- Choose A Status ---</option>
                                        @if($status == 'edit')
                                            <option value="Y"@if($product->is_bestseller=="Y"){{'selected'}}@endif>Yes</option>
                                            <option value="N"@if($product->is_bestseller=="N"){{'selected'}}@endif>No</option>
                                        @else
                                            <option value="Y">Yes</option>
                                            <option value="N">No</option>
                                        @endif
                                    </select>
                                </div>


                                <div class="form-group">
                                    <label for="is_active"><b>Promotion</b><font color="red">*</font></label>
                                    <select class="form-control" name="promotion" id="promotion">
                                        @if($status == 'edit')
                                            <option value="" @if($query==""){{'selected'}}@endif >ไม่เข้าร่วมโปรโมชั่น</option>
                                            @foreach($promotions as $promotion)
                                                @if($query=="")
                                                    <option value="{{$promotion->id}}">{{$promotion->title_en}}</option>
                                                @endif 
                                                @if($query!="")
                                                    <option value="{{$promotion->id}}" @if($query->id_promotion==$promotion->id){{'selected'}}@endif >{{$promotion->title_en}}</option>
                                                @endif 
                                            @endforeach
                                        @else
                                            <option value="" >ไม่เข้าร่วมโปรโมชั่น</option>
                                            @foreach($promotions as $promotion)
                                                <option value="{{$promotion->id}}">{{$promotion->title_en}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                  
                                </div>


                                <div class="col-sm-12 col-12">
                                    <div class="row">
                                        <div class="col-sm-5 col-12 padding-zero">
                                            <label><b>Picture Product</b><font color="red">*(ขนาดรูป 600*600)</font></label>
                                            <input type="file" name="file_upload" id="file_upload">
                                            @if($status == 'edit')
                                                <img src="/public/product/{{ $product->cover_img }}" alt="Picture Product" class="img-fluid rounded mx-auto d-block profile-image" id="showImage" >
                                            @else
                                                <img src="{{ $image_display }}" alt="Picture Product" class="img-fluid rounded mx-auto d-block profile-image" id="showImage" >
                                            @endif
                                        </div>
                                        <div class="col-sm-1 padding-zero">
                                        </div>

                                        <div class="col-sm-5 col-12 padding-zero">
                                            <label><b>Picture Product Zoom</b><font color="red">*(ขนาดรูป 1000*1000)</font></label>
                                            <input type="file" name="img_zoom" id="img_zoom">
                                            @if($status == 'edit')
                                                <img src="/public/product/{{ $product->cover_zoom }}" alt="Picture Product Zoom" class="img-fluid rounded mx-auto d-block profile-image" id="showImageZoom" >
                                            @else
                                                <img src="{{ $image_display }}" alt="Picture Product Zoom" class="img-fluid rounded mx-auto d-block profile-image" id="showImageZoom" >

                                            @endif
                                        </div>
                                        <div class="col-sm-1 padding-zero">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label><B>
                                            Date Range:
                                        </B></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="livicon" data-name="phone" data-size="14" data-loop="true"></i>
                                        </div>

                                        @if($status == 'edit')
                                            <input type="text" class="form-control daterangepicker-field" id="daterange"  value="{{$date}}"/>
                                        @else
                                            <input type="text" class="form-control daterangepicker-field" id="daterange"  value=""/>
                                        @endif

                                    </div>
                                    <!-- /.input group -->
                                </div>

                                <div class="form-group" hidden>
                                    <label for="startdate"><b>Start Date</b></label><br>
                                    <input type="date" name="startdate" id="startdate" />
                                </div>

                                <div class="form-group" hidden>
                                    <label for="enddate"><b>End Date</b></label><br>
                                    <input type="date" name="enddate" id="enddate">
                                </div>

                                <div class="form-group">
                                    <label for="editor"><b>Detail Text EN</b><font color="red">*</font></label><br>
                                    <!-- Create the editor container -->
                                    @if($status == 'edit')


                                        <textarea name="summernotenproduct"  id="summernotenproduct">{{ $product->detail_en }}</textarea>
                                    @else
                                        <textarea name="summernotenproduct" id="summernotenproduct"></textarea>
                                    @endif
                                </div>

                                <div class="form-group" >
                                    <label for="editor"><b>Detail Text TH</b><font color="red">*</font></label><br>
                                    <!-- Create the editor container -->
                                    @if($status == 'edit')


                                        <textarea name="summernotethproduct"  id="summernotethproduct">{{ $product->detail_th }}</textarea>
                                    @else
                                        <textarea name="summernotethproduct"  id="summernotethproduct"></textarea>
                                    @endif
                                </div>







                                <div class="box-footer col-md-12">
                                    <button type="button" class="btn btn-success btn-save btn-loading" style="width: 120px;">Save</button>
                                </div>

                                <div class="form-group">
                                    <label><b>Ingredient</b></label>
                                </div>


                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-bordered full-ta" id="department-table">
                                            <thead>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Link</th>
                                            <th>#</th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label for="category" class="col-3" style="margin-top: .5rem;padding-right:0px;"><B>Category : </B></label>
                                        <select class="form-control col-6" name="categorie_id_filter">
                                            <option value="0">All</option>
                                            @foreach($itemCategory as $category)
                                                <option @if($idCategory==$category->id){{'selected'}}@endif value="{{$category->id}}"> {{$category->name}} </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered full-ta" id="dataTable"  class="display" width="100%" cellspacing="0">
                                        <thead>
                                        <tr class="center">
                                            <th>Id</th>
                                            <th style="width:250px;">Name</th>
                                            <th>Link</th>
                                            <th style="width:130px;">Action</th>
                                        </tr>
                                        </thead>

                                        <tbody id="tBody">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <script src="https://cdn.ckeditor.com/4.14.1/full/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('summernotenproduct', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form',

        });
        CKEDITOR.replace('summernotethproduct', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form',

        });
    </script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.js"></script>
    <script>
        if (!window.moment) {
            document.write('<script src="assets/plugins/moment/moment.min.js"><\/script>');
        }
    </script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.5/daterangepicker.css" rel="stylesheet">

    <script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/js/bootstrapValidator.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.5/daterangepicker.js"></script>
    <link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

    <script>

        $(".daterangepicker-field").daterangepicker({
            forceUpdate: true,
            callback: function(startDate, endDate, period){
                var title = startDate.format('L') + ' – ' + endDate.format('L');
                $(this).val(title)
            }
        });


        $('#price').keypress(function(event){
            if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
                event.preventDefault();
            }
        });

        var $link = "<?php echo url('/public/product/'); ?>";
        var $backoffice_url = <?php echo json_encode(url('cms')); ?>

            /*
            var $link = <?php echo json_encode(Storage::url('')); ?>
            */

        $('body').on('click', '.btn-save', function () {

            var name_en = $('#name_en').val();
            var price = $('#price').val();
            var size = $('#size').val();
            var category = $('#idCategory').val();
            var is_active = $('#is_active').val();
            var startdate = $('#startdate').val();
            var enddate = $('#enddate').val();
            var summernoten = $('#summernotenproduct').val();
            var summernoteth = $('#summernotethproduct').val();
            var imagezoom = $('#imagezoom').val();
            var image = $('#image').val();
            var file = $('#multifile').val();
            var file_th = $('#multifile2').val();
            var display_type = $('#display_type').val();
            var status = $('#status').val();
            var id = $('#id').val();
            var deletefile_id = $('#deletefile_id').val();
            var deletefile_name = $('#deletefile_name').val();
            var date = $('#daterange').val();
            var start_date = date.substr(0, 10);
            var end_date = date.substr(13, 24);
            var best = $('#best').val();
            var promotion = $('#promotion').val();

            //alert(promotion);

            

            var summerth = CKEDITOR.instances.summernotethproduct.getData();
            var summeren = CKEDITOR.instances.summernotenproduct.getData();

            if((name_en == '' ) || (price == '') || (category == '') || (is_active =='') || (imagezoom == '') || (image == '') || (display_type == '' ) || (best =="")){

                return swal("เกิดข้อผิดพลาด!", "กรุณากรอกข้อมูลให้ครบ", "error");
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            if(status == 'edit'){

                $.ajax({
                    dataType: 'json',
                    type:'PUT' ,
                    data:{id:id,name_en:name_en,price:price,size:size,is_active:is_active,file:file,detail_th:summerth,detail_en:summeren,category:category,imagezoom:imagezoom,image:image,display_type:display_type,deletefile_id:deletefile_id,deletefile_name:deletefile_name,file:file,file_th:file_th,start_date:start_date,end_date:end_date,is_bestseller:best,promotion:promotion},
                    url: '/cms/products/' + id,
                    success: function(datas){
                        if(datas.code_return == 1){
                            swal("Good job!", "Edit Complate", "success");


                            var base_url = window.location.origin;
                            window.location.replace(base_url+'/cms/products')
                        }else{
                            swal("Error!", datas.msg_return, "error");
                        }
                    }
                });

            }else{

                $.ajax({
                    dataType: 'json',
                    type:'POST',
                    data:{name_en:name_en,price:price,size:size,is_active:is_active,file:file,file_th:file_th,summernoteth:summerth,summernoten:summeren,category:category,imagezoom:imagezoom,image:image,display_type:display_type,start_date:start_date,end_date:end_date,is_bestseller:best,promotion:promotion,},
                    url: '/cms/products',
                    success: function(datas){
                        swal("Good job!", "You clicked the button!", "success");
                        window.location.href = '/cms/products'
                    }
                })
            }
        });



        ///------ สินค้าหลัก ------///
        $('body').on('change', 'input[name= "file_upload"]', function () {
            if ($('input[name ="file_upload"]').val() != '') {
                var _URL = window.URL || window.webkitURL;
                var file, img;
                var file_data = $('input[name= "file_upload"]').prop('files')[0];
                var form_data = new FormData();
                if ((file = this.files[0])) {
                    img = new Image();
                    img.onload = function() {
                        if((this.width != '600') && (this.height != '600')){
                            return  swal("Cancelled", "Your imaginary file is safe :)", "error");
                        }
                        form_data.append('file_upload', file_data);
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: '/cms/products/uploadimage',
                            dataType: 'json',
                            type: 'POST',
                            data: form_data,
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function success(resp) {
                                $('input[name=image]').val(resp.data);
                                $('#showImage').attr("src", $link +'/'+ resp.data);
                                swal("Good job!", "You clicked the button!", "success");
                            },
                            error: function error(xhr, textStatus, errorThrown) {
                                showFormErrors(xhr, $('#customerForm'));
                                console.log(errorThrown);
                            }
                        });
                    };
                    img.onerror = function() {
                        alert( "not a valid file: " + file.type);
                    };
                    img.src = _URL.createObjectURL(file);
                }
            }
        });

        ///------สินค้าzoom------///
        $('body').on('change', 'input[name="img_zoom"]', function () {
            if ($('input[name ="img_zoom"]').val() != '') {
                var _URL = window.URL || window.webkitURL;
                var file, img;
                var file_data = $('input[name= "img_zoom"]').prop('files')[0];
                var form_data = new FormData();
                if ((file = this.files[0])) {
                    img = new Image();
                    img.onload = function() {
                        if((this.width != '1000') && (this.height != '1000')){
                            return  swal("Cancelled", "Your imaginary file is safe :)", "error");
                        }
                        form_data.append('img_zoom', file_data);
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: '/cms/products/uploadimage',
                            dataType: 'json',
                            type: 'POST',
                            data: form_data,
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function success(resp) {
                                $('input[name=imagezoom]').val(resp.data);
                                $('#showImageZoom').attr("src", $link +'/'+ resp.data);
                                swal("Good job!", "You clicked the button!", "success");
                            },
                            error: function error(xhr, textStatus, errorThrown) {
                                showFormErrors(xhr, $('#customerForm'));
                                console.log(errorThrown);
                            }
                        });
                    };
                    img.onerror = function() {
                        alert( "not a valid file: " + file.type);
                    };
                    img.src = _URL.createObjectURL(file);
                }
            }
        });



        // exist image file dropzone

        if($('input[name=id]').val()) {
            axios.get($backoffice_url + '/products/' + $('input[name=id]').val() + '/images')
                .then(function (response) {
                    response.data['detail_en'].forEach(function (data) {
                        console.log('detail_en',data);
                        let thumbFile = {
                            id: data.id,
                            name: data.img_en,
                            size: 0,
                            dataURL: $link+ '/' + data.img_en
                        };
                        myDropzone.emit('addedfile', thumbFile);
                        myDropzone.createThumbnailFromUrl(thumbFile,
                            myDropzone.options.thumbnailWidth,
                            myDropzone.options.thumbnailHeight,
                            myDropzone.options.thumbnailMethod, true,
                            function (thumbnail) {
                                myDropzone.emit('thumbnail', thumbFile, thumbnail);
                            });
                        myDropzone.emit('complete', thumbFile);
                    });

                    response.data['detail_th'].forEach(function (data) {
                        console.log('xxxxxxxxxxxxxxth',data);
                        let thumbFile = {
                            id: data.id,
                            name: data.img_th,
                            size: 0,
                            dataURL: $link+ '/' + data.img_th
                        };
                        myDropzone2.emit('addedfile', thumbFile);
                        myDropzone2.createThumbnailFromUrl(thumbFile,
                            myDropzone2.options.thumbnailWidth,
                            myDropzone2.options.thumbnailHeight,
                            myDropzone2.options.thumbnailMethod, true,
                            function (thumbnail) {
                                myDropzone2.emit('thumbnail', thumbFile, thumbnail);
                            });
                        myDropzone2.emit('complete', thumbFile);
                    });
                })
                .catch(function (error) {
                    console.log(error);
                });
        }






        function CopyMyLeftTd(e) {
            var leftTdIndex= $(e).parent().index()-1;
            var leftTd= $(e).closest("tr").find("td:eq(" + leftTdIndex + ")");
            copyToClipboard($(leftTd).text());
        }

        function copyToClipboard(txt) {
            var el = document.createElement('textarea');

            el.value = txt;
            el.setAttribute('readonly', '');
            el.style.position = 'absolute';
            el.style.left = '-9999px';
            document.body.appendChild(el);
            el.select();
            document.execCommand('copy');
            document.body.removeChild(el);
        }



        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#department-table').DataTable({
            processing: true,
            serverSide: true,
            searching: false,
            lengthChange: false,
            ajax: {
                url: "/cms/ingredient",
                type: 'POST',
            },
            columns: [
                {data: 'id', name:'id'},
                {data: 'title_en', name:'title_en'},
                {data: 'link', name:'link'},
                {data: 'action', name: 'action'}
            ]
        });

        function myFunction() {
            alert('ok');
        }

        var searchData = {};

        var table = $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url:  "{!! route('product.data') !!}",
                method: 'POST',
                data: RefreshTable,
            },
            columns: [
                {data: 'id'},
                {data: 'name_en'},
                {data: 'link'},
                {data: 'action', name: 'action', orderable:false, serachable:false}
            ],catch (Error) {
                if (typeof console != "undefined") {
                    console.log(Error);
                }
            },

            columnDefs: [{
                targets: [0],
                orderable: false,
                searchable: false
            },


                {
                    targets: 3,
                    orderable: false,
                    searchable: false,
                    render: function (data, type, row) {
                        var dataName = row.name_en;
                        var dataid = row.id;
                        // var btnDetail = '<a role="button"  href="products/sku/' + dataid +'" class="btn btn-outline-primary btn-sm"><i class="fa fa-server"></i> Detail</a> ';
                        var btnEdit = '<a role="button"      class="btn btn-outline-dark btn-sm btn-edit" onclick="CopyMyLeftTd(this)"><i class="fa fa-edit"></i> Copy Link</a> ';
                        return btnEdit;


                    }
                },
            ]
        });

        $('select').on('change', function() {
            searchData.categorie_id = $('select[name=categorie_id_filter]').val();
            // alert(searchData.categorie_id);
            table.ajax.reload();

        });


        function RefreshTable(data) {

            data._token = "{{ csrf_token() }}";
            data.categorie_id = $('select[name=categorie_id_filter]').val();

            return data;

        }

        function reloadData() {

            table.ajax.reload(null, false);
        }





    </script>





@endsection
