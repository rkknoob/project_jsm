@extends('backend.layouts.content')

@section('content')

<style>
        .btn-icon-split .icon {
            padding-top: 5%;
        }
        .table th {
            padding: .5rem;
        }
        .center{
            text-align:center;
        }
        a{
            text-decoration:none;
        }
        .dataTable{
            margin-bottom: 1.5%!important;
        }
        .right{
            text-align: right;
        }

        /* Alert */
        .label {
            display: inline;
            font-size: 75%;
            font-weight: 700;
            line-height: 1;
            color: rgb(255, 255, 255);
            text-align: center;
            white-space: nowrap;
            vertical-align: baseline;
            padding: 0.2em 0.6em 0.3em;
            border-radius: 0.25em;
        }
        .label-default {
            background-color: rgb(210, 214, 222);
            color: rgb(68, 68, 68);
        }
        .label-success {
            background-color: rgb(0, 166, 90) !important;
            color: rgb(255, 255, 255) !important;
        }
        .label-warning{
            background-color: rgb(243, 156, 18) !important;
            color: rgb(255, 255, 255) !important;
        }
    </style>

    <div>
        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800">{{ $item->cart_number }}</h1>
        <div class="card shadow col-md-12" style="margin-bottom: 7%;">
            <div class="collapse show" id="collapseCardExample" >
                <div class="card-body">
                    <div class="box">
                        {{ csrf_field() }}

                        <h4 class="h4 mb-4 text-gray-800">รายการสินค้า</h4>
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable"  class="display" width="100%" cellspacing="0">
                                <thead>
                                    <tr class="center">
                                        <th colspan="2">สินค้า</th>
                                        <th style="width:150px">ราคา(ต่อชิ้น)</th>
                                        <th style="width:100px">จำนวน</th>
                                        <th style="width:150px">ราคา</th>
                                    </tr>
                                </thead>
                                <tbody id="tBody">
                                    @foreach($itemProduct as $product)
                                    <tr>
                                        <td style="width:100px"> 
                                            <img src="/public/product/{{ $product->cover_img }}" style="width:100%;" alt="Product thumbnail" title="Product thumbnail" >
                                        </td>
                                        <td>
                                            <span>
                                                {{ $product->name_en }} <br/>

                                                @foreach($itemSku as $sku)
                                                    @if($product->id_sku == $sku->id )
                                                        Option : {{$sku->name_en}} 
                                                    @endif
                                                @endforeach
                                            </span>
                                        </td>
                                        <td> ฿{{ $product->price }} </td>
                                        <td> {{ $product->qty }} </td>
                                        <td> ฿{{ $product->sumPrice }}</td>
                                    </tr>               
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-md-12 right">
                                <h5>ยอดรวม ฿{{ $item->totalOrder }}</h5>
                                <h5>ค่าส่ง ฿{{ $item->totalShipping }}</h5>
                                <h5>รวมทั้งสิ้น ฿{{ $item->sumPrice }}</h5><br><br>
                            </div>
                        </div>
                        <h4 class="h4 mb-4 text-gray-800">ข้อมูลการส่งสินค้า</h4>
                        <div class="row">
                            @foreach($itemUser as $user)
                            <div class="form-group col-md-6">
                                <label for="nameen">ชื่อ</label>
                                <input type="text" class="form-control" value="{{$user->fname}}" readonly>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="name_th">นามสุกล</label>
                                <input type="text" class="form-control" value="{{$user->lname}}" readonly>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="name_th">อีเมล</label>
                                <input type="text" class="form-control" value="{{$user->email}}" readonly>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="name_th">เบอร์โทร</label>
                                <input type="text" class="form-control" value="{{$user->phone}}" readonly>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="name_th">ที่อยู่</label>
                                <input type="text" class="form-control" value="{{$user->address}}" readonly>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="name_th">แขวง/ตำบล</label>
                                <input type="text" class="form-control" value="{{$itemAddress->district}}" readonly>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="name_th">เขต/อำเภอ</label>
                                <input type="text" class="form-control" value="{{$itemAddress->amphoe}}" readonly>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="name_th">จังหวัด</label>
                                <input type="text" class="form-control" value="{{$itemAddress->province}}" readonly>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="name_th">รหัสไปรษณีย์</label>
                                <input type="text" class="form-control" value="{{$itemAddress->zipcode}}" readonly>
                            </div>
                            <div class="form-group col-md-6">
                            </div>
                            @endforeach
                        </div>

                        <div class="row">   
                            @if($item->deliveryStatus === 'sent')
                            <div class="form-group col-md-6">
                                <label for="name_th"><b>Track Number</b><font color="red">*</font></label>
                                <input type="text" class="form-control" value="{{$item->trackNumber}}" readonly>
                            </div>

                            <div class="form-group col-md-6" hidden>
                                <label for="name_th"><b>Ax Free</b><font color="red">*</font></label>
                                <input type="text" class="form-control" value="{{$item->axFree}}" readonly>
                            </div>

                            @elseif($item->deliveryStatus === 'inprogress')
                            <form name="myForm" action="#" id="productForm" onsubmit="return validateForm()" method="post" style="width:100%">
                                <input type="hidden" class="form-control" name="id" id="id" value="{{$item->id}}">
                                <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
                                
                                <div class="form-group col-md-6" style="float:left;">
                                    <label for="name_th"><b>Track Number</b><font color="red">*</font></label>
                                    <input type="text" class="form-control" name="track" id="track" placeholder="Enter Track Number">
                                </div>

                                <div class="form-group col-md-6"  style="float:right;" hidden>
                                    <label for="name_th"><b>Ax Free</b><font color="red">*</font></label>
                                    <input type="text" class="form-control" name="axFree" id="axFree" placeholder="Enter Ax Free">
                                </div>
                                
                                <div class="form-group col-md-6" style="float:right; margin-top: 2%;">
                                    <button type="button" class="btn btn-success btn-save" style="width: 120px;margin-top: 3%;">Save</button> 
                                </div>
                            </form>

                            @endif
                        </div>
                     
                    <div>

                </div>
            </div>
        </div>

    </div>

    <script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.js"></script>
    <script>
        if (!window.moment) {
            document.write('<script src="assets/plugins/moment/moment.min.js"><\/script>');
        }
    </script>
    <script type="text/javascript">

        $('body').on('click', '.btn-save', function () {
            var id = $('#id').val();
            var track= $('#track').val();
            var axFree= $('#axFree').val();
            

            if((track == '') || (id == '')){
               return swal("Error", "กรุณากรอกข้อมูลให้ครบ", "error");
            }
    
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                dataType: 'json',
                type:'put' ,
                data:{id:id,trackNumber:track,axFree:axFree},
                url: '/cms/order/update' + id,
                success: function(datas){
                    swal("Good job!", "Successfully saved data", "success");
                    window.location.href = '/cms/order'
                }
            });

        });
    </script>

    


@endsection