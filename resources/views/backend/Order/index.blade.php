@extends('backend.layouts.content')

@section('content')
    <style>
        .btn-icon-split .icon {
            padding-top: 5%;
        }
        .box-footer{
            clear:both;
            margin-top: 15px;
            text-align : left;
            padding-top: 20px;
            padding-bottom: 15px;
        }
        .note-popover{
            display:none;
        }
        .padding-zero{
            padding-left: 0px!important;
        }
        .profile-image{
            width: 450px;
            padding-top: 25px;
            margin-bottom: 25px;
        }
         /* Alert */
         .label {
            display: inline;
            font-size: 75%;
            font-weight: 700;
            line-height: 1;
            color: rgb(255, 255, 255);
            text-align: center;
            white-space: nowrap;
            vertical-align: baseline;
            padding: 0.2em 0.6em 0.3em;
            border-radius: 0.25em;
        }
        .label-default {
            background-color: rgb(210, 214, 222);
            color: rgb(68, 68, 68);
        }
        .label-success {
            background-color: rgb(0, 166, 90) !important;
            color: rgb(255, 255, 255) !important;
        }
        .label-warning{
            background-color: rgb(243, 156, 18) !important;
            color: rgb(255, 255, 255) !important;
        }

        .input-group>.custom-select:not(:last-child), .input-group>.form-control:not(:last-child) {
            border-top-right-radius: 5px!important;
            border-bottom-right-radius: 5px!important;
        }
        .input-group>.custom-select:not(:last-child), .input-group>.form-control:not(:last-child) {
            border-top-right-radius: 5px!important;
            border-bottom-right-radius: 5px!important;
        }
    </style>
    <div>



        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800">Order</h1>

        <div class="card shadow col-md-12" style="margin-bottom: 7%;">
            <div class="collapse show" id="collapseCardExample" >
                <div class="card-body">

                    <form name="myForm" action="/cms/order" id="productForm" method="get">
                        <div class="box"> 
                  
                            <div class="row">
                                <div class="box-body col-md-6">
                                    <div class="form-group">
                                        <label for="fname"><b>Order Number</b></label>
                                        <input class="form-control" type="hidden" name="statuspayment" id="statuspayment" value="{{ $statuspayment }}" />
                                        <input type="text" class="form-control" name="order_number" id="order_number" placeholder="Order Number" value="{{ $order_number }}">
                                    </div>
                                </div>
                                <div class="box-body col-md-6">
                                    <div class="form-group">
                                        <label for="is_active"><b>Payment Status</b></label>
                                        <select class="form-control" name="statuspayment" id="statuspayment">
                                            <option value="">--- เลือกสถานะการชำระเงิน ---</option>
                                            <option value="confirm">ยืนยันแล้ว</option>
                                            <option value="wait">ยังไม่ชำระเงิน</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="box-body col-md-6">
                                    <div class="form-group">
                                        <label for="is_active"><b>Order Date Start</b></label>
                                        <div id="datepicker" class="input-group date" data-date-format="yyyy-mm-dd">
                                            <input class="form-control" type="text" name="dayfisrt" readonly  value="{{ $dayfisrt }}"/>
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-body col-md-6">
                                    <div class="form-group">
                                        <label for="is_active"><b>Order Date End</b></label>
                                        <div id="datepicker2" class="input-group date" data-date-format="yyyy-mm-dd">
                                            <input class="form-control" type="text" name="daylast" readonly value="{{ $daylast }}"/>
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>

                          

                                <div class="box-body col-md-6">
                                    <div class="form-group">
                                        <label for="is_active"><b>Payment Date</b></label>
                                        <div id="datepicker3" class="input-group date" data-date-format="yyyy-mm-dd">
                                            <input class="form-control" type="text" name="daypayment" readonly value="{{ $daypayment }}"/>
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>

                            @php
                                if(!$order_number){
                                 $order_number = "nodata";
                                }
                                if(!$dayfisrt){
                                    $dayfisrt = "nodata";
                                }
                                if(!$daylast){
                                    $daylast = "nodata";
                                }
                                if(!$statuspayment){
                                    $statuspayment = "nodata";
                                }
                                if(!$daypayment){
                                    $daypayment = "nodata";
                                }
                              
                            @endphp

                                <div class="box-body col-md-6">
                                    <div class="form-group">
                                        <div class="row" style="margin-top: 7.5%;">
                                            <div class="col-md-4">
                                                <button type="submit" class="btn btn-success btn-save btn-loading" style="width: 120px;">Search</button>
                                            </div>
                                            <div class="col-md-4">
                                                <a class="btn btn-warning" style="width: 120px;" href="{!! route('export',['order_number' => $order_number,'dayfisrt' => $dayfisrt,'daylast' => $daylast,'statuspayment' => $statuspayment, 'daypayment' => $daypayment]) !!}">Export</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">

                            </div>

                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable"  class="display" width="100%" cellspacing="0">
                                    <thead>
                                    <tr class="center">
                                        <th>Id</th>
                                        <th>Order Number</th>
                                        <th>Order Date</th>
                                        <th>Customer Name</th>
                                        <th>Retail Price</th>
                                        <th>Payment Status</th>
                                        <th>Delivery Status</th>
                                        <th style="width:90px">Tracking Number</th>
                                    </tr>
                                    </thead>

                                    <tbody id="tBody">
                                    @foreach ($data as $item)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $item->cart_number }}</td>
                                            <td>
                                                {{ date('d/m/Y', strtotime($item->created_at)) }}
                                            </td>
                                            <td>
                                                {{ $item->fname.'  '.$item->lname  }}

                                            </td>
                                            <td> {{ number_format($item->sumPrice,2) }}</td>

                                            <td>
                                                @if ($item->paymentStatus === "wait")
                                                    <span class="label label-default">ยังไม่ชำระเงิน</span>
                                                @elseif ($item->paymentStatus === "confirm")
                                                    <span class="label label-success">ยืนยันแล้ว</span>
                                                @endif
                                            </td>
                                            <td>
                                                @if ($item->deliveryStatus === "wait")
                                                    <span class="label label-default">รอการชำระเงิน</span>
                                                @elseif ($item->deliveryStatus === "inprogress")
                                                    <span class="label label-warning">กำลังดำเนินการ </span>
                                                @elseif ($item->deliveryStatus === "sent")
                                                    <span class="label label-success">ส่งแล้ว</span>
                                                @endif
                                            </td>
                                            <td>
                                                @if ($item->deliveryStatus == "sent" || $item->deliveryStatus == "inprogress")
                                                    <a role="button"  href="/cms/order/edit/{{$item->id}}" class="btn btn-outline-info btn-sm">
                                                        <i class="fas fa-calendar-week"></i>
                                                        Click
                                                    </a>
                                                @endif
                                                @if($item->deliveryStatus === "wait")
                                                    -
                                                @endif
                                            </td>

                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>


    </div>

    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.js"></script>
    <script>
        if (!window.moment) {
            document.write('<script src="assets/plugins/moment/moment.min.js"><\/script>');
        }
    </script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" /> -->

    <script>



        $("#datepicker").datepicker({
            autoclose: true,
            todayHighlight: true
        }).datepicker({format: 'yyyy-mm-dd'});

        $("#datepicker2").datepicker({
            autoclose: true,
            todayHighlight: true
        }).datepicker({format: 'yyyy-mm-dd'});

        $("#datepicker3").datepicker({
            autoclose: true,
            todayHighlight: true
        }).datepicker({format: 'yyyy-mm-dd'});



        $('body').on('click', '.btn-excel', function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                dataType: 'json',
                type:'get' ,
                url: '/cms/export',
                success: function(datas){

                }
            });

        });


    </script>

@endsection
