@extends('backend.layouts.content')

@section('content')
<style>
        .btn-icon-split .icon {
            padding-top: 5%;
        }
        .box-footer{
            clear:both;
            margin-top: 15px;
            text-align : left;
            padding-top: 20px;
            padding-bottom: 15px;
        }
        .note-popover{
            display:none;
        }
        .padding-zero{
            padding-left: 0px!important;
        }
        .profile-image{
            width: 620px;
            padding-top: 25px;
            margin-bottom: 25px;
            margin-left: 0!important;
        }
    </style>


<div>

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Edit Video Index Form</h1>

    <!-- DataTales Example -->
    <div class="card shadow col-md-12" style="margin-bottom: 7%;" >
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">

                    <input type="hidden" name="image" id="image" value="{{ $item->img }}">
                    <input type="hidden" name="id" id="id" value="{{ $item->id }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">


                    <input type="hidden" name="imagemobile" id="imagemobile" value="{{ $item->imagemobile }}">

                    <div class="box">
                        <form method="post" action="{{$item->id}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="box-body">

                                <div class="form-group">
                                    <label for="fname"><b>Name</b></label>
                                    <input type="text" class="form-control col-md-8" name="name" id="name" placeholder="Enter Name Category"  value="{{ $item->name_en }}" readonly>
                                </div>

                                <div class="form-group">
                                    <label for="fname"><b>Link Youtube</b><font color="red">*</font></label>
                                    <textarea rows="4" cols="50" type="text" class="form-control col-md-8" name="link_video" id="link_video" placeholder="Enter Link Video">{{ $item->link_video }}</textarea>
                                </div>

                                <div class="col-sm-12 col-12">
                                    <div class="row">
                                        <div class="col-sm-5 col-12 padding-zero">
                                            <label><b>Picture Product</b><font color="red"></font></label>
                                            <input type="file" name="file_upload" id="file_upload">

                                            <img src="/public/popup/{{ $item->imagemobile }}" alt="Picture Brand" class="img-fluid rounded mx-auto d-block profile-image" id="showImageEn" >
                                        </div>
                                    </div>
                                </div>

                                <div class ="loading" style="text-align:center">
                                    <img src="/img/1.gif" style="width: 100px;height:100px">
                                </div>
                                <div class="box-footer col-md-12 padding-zero" >
                                    <button type="button" class="btn btn-success btn-save btn-loading" style="width: 120px;">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript">
$('.loading').hide();
    var $link = "<?php echo url('/public/popup/'); ?>";

    $('body').on('click', '.btn-save', function () {
        var id = $('#id').val();
        var link_video = $('#link_video').val();
        var imagemobile = $('#imagemobile').val();



        if((link_video == '')){
            return swal("เกิดข้อผิดพลาด!", "กรุณากรอกข้อมูลให้ครบ", "error");
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });



        $.ajax({
            dataType: 'json',
            type:'PUT' ,
            data:{id:id,link_video:link_video,imagemobile:imagemobile},
            url: '/cms/video/update',
            success: function(datas){
                console.log(datas);
                swal("Good job!", "You clicked the button!", "success");
                window.location.href = '/cms/video'
            }
        })
    });

    ///--------- Banner Picture ---------///
    $('body').on('change', 'input[name= "file_upload"]', function () {

        if ($('input[name ="file_upload"]').val() != '') {
            $('.loading').show();
            var _URL = window.URL || window.webkitURL;
            var file, img;
            var file_data = $('input[name= "file_upload"]').prop('files')[0];
            var form_data = new FormData();
            if ((file = this.files[0])) {
                img = new Image();
                img.onload = function() {

                // if((this.width != '420')){
                //     $('.loading').hide();
                //     // return  swal("Cancelled", "Your imaginary file is safe :)", "error");
                // }
                // // if((this.width < '479')){
                // //         $('.loading').hide();
                // //         return  swal("Cancelled", "Your imaginary file is safe :)", "error");
                // // }
                // if((this.height != '263')){
                //         $('.loading').hide();
                //         return  swal("Cancelled", "Your imaginary file is safe :)", "error");
                // }
                // if((this.height < '269')){
                //         $('.loading').hide();
                //         return  swal("Cancelled", "Your imaginary file is safe :)", "error");
                // }

                form_data.append('file_upload', file_data);
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });


                    $.ajax({
                        url: '/cms/popup/uploadimage',
                        dataType: 'json',
                        type: 'POST',
                        data: form_data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function success(resp) {

                            $('input[name=imagemobile]').val(resp.data);
                            $('#showImageEn').attr("src", $link +'/'+ resp.data);

                            swal("Good job!", "You clicked the button!", "success");
                            $('.loading').hide();
                        },
                        error: function error(xhr, textStatus, errorThrown) {
                            showFormErrors(xhr, $('#customerForm'));
                            console.log(errorThrown);
                        }
                    });

                };
                img.onerror = function() {
                    alert( "not a valid file: " + file.type);
                };
                img.src = _URL.createObjectURL(file);
            }
        }
    });
</script>

@endsection
