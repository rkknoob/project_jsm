@extends('backend.layouts.content')

@section('content')

<style>
    .text-gray-800{
        font-weight: 600;
    }
    .shadow{
        margin-bottom: 7%;
    }
    .btn-title{
        text-align:right; 
        margin: 0 0 2% 0;
    }
    label {
        color: #333;
    }
    .center{
        text-align:center;
    }
</style>

<div>

    <h1 class="h3 mb-4 text-gray-800">Manage Promotion</h1>
    <div class="card shadow col-md-12">
        <div class="card-body">

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
                <div class="col-md-12 btn-title">
                    <a href="{{ url('cms/promotion/add') }}" class="btn btn-success">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                        Add Promotion
                    </a>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable"  class="display" width="100%" cellspacing="0">
                    <thead>
                        <tr class="center">
                            <th>ID</th>
                            <th>Title(En)</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody id="tBody">

                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">
    var searchData = {};
    var table = $('#dataTable').DataTable({
        processing: true,
        serverSide: true,

        ajax: {
            url:  "{!! route('promotion.data') !!}",
            method: 'POST', 
            data: RefreshTable,
        },
        columns: [
            {data: 'id'},
            {data: 'title_en'},
            {data: 'is_active', name: 'is_active', orderable:false, serachable:false},
            {data: 'action', name: 'action', orderable:false, serachable:false}
        ],catch (Error) {
            if (typeof console != "undefined") {
                console.log(Error);
            }
        },

        columnDefs: [
            {
                targets: 0,
                orderable: false,
                searchable: false,                    
                className: "text-center",
                width: "8%",
            },
            {
                targets: 2,
                orderable: false,
                searchable: false,
                className: "text-center",
                width: "12%",
                render: function (data, type, row) {
                    if(row.is_active == 'Y'){
                        var result = data ?  '  <center><span class="badge badge-primary badge-pill m-b-5">Active</span></center>' : '<center><span class="badge badge-danger badge-pill m-b-5">Active</span></center>';
                        return result;
                    }else{
                        var result = data ?  '  <center><span class="badge badge-danger badge-pill m-b-5">Inactive</span></center>' : '<center><span class="badge badge-danger badge-pill m-b-5">Inactive</span></center>';
                        return result;
                    }
                }
            },
            {
                targets: 3,
                orderable: false,
                searchable: false,
                className: "text-center",
                width: "20%",                    
                render: function (data, type, row) {
                    var dataName = row.title_en;
                    var dataid = row.id;
                    var btnEdit = '<a role="button"  href="promotion/edit/' + dataid +'" class="btn btn-outline-dark btn-sm btn-edit"><i class="fa fa-edit"></i> Edit</a> ';
                    var btnDelete = '<a href="#" data-href="/promotion/' + data + '" data-name="' + dataName + '"  data-id="' + dataid + '" role="button" class="btn btn-outline-danger btn-sm btn-delete"><i class="fa fa-trash"></i> Delete</a>';
                    return btnEdit + btnDelete;
                }
            },
        ]
    });
        
    function RefreshTable(data) {
        data._token = "{{ csrf_token() }}";
        return data;
    }


    $('body').on('click', '.btn-delete', function (e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        var name = $(this).attr('data-name');
        console.log(id,name);
        deleteConf(id, name);
    });

    $('body').on('click', '.btn-view', function (e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        $("#myModal").modal();
    });

    function deleteConf(id,name) {
        swal({
            title: "You want to delete, right?",
            text: name,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){
            if (isConfirm) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    dataType: 'json',
                    type:'DELETE',
                    data:{id:id},
                    url: '/cms/promotion/' + id,

                    success: function(datas){
                        swal("Good job!", "Data was deleted successfully.", "success");
                        reloadData();
                    }
                })

            }else{
                swal("Cancelled", "Your files are safe :)", "error");
            }
        });
    } // error form show text

    function reloadData() {
        table.ajax.reload(null, false);
    }
</script>
@endsection

