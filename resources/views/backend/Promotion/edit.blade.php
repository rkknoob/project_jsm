@extends('backend.layouts.content')

@section('content')
<style>
    .text-gray-800{
        font-weight: 500;
    }
    .shadow{
        margin-bottom: 7%;
    }
    label {
        color: #333;
    }
    .box-footer{
        clear:both;
        text-align : left;
        padding: 15px 0px 15px 0px;
    }    
    .cke_button__save, .cke_button__newpage, .cke_button__preview, .cke_button__print, 
    .cke_button__templates, .cke_button__pastetext, .cke_button__pastefromword, .cke_button__find,
    .cke_button__replace, .cke_button__anchor, .cke_button__removeformat, .cke_button__copyformatting,
    .cke_button__flash, .cke_button__pagebreak, .cke_button__form, .cke_button__checkbox, 
    .cke_button__radio, .cke_button__textfield, .cke_button__textarea, .cke_button__select,
    .cke_button__button, .cke_button__imagebutton, .cke_button__hiddenfield, .cke_button__selectall,
    .cke_button__scayt, .cke_button__showblocks     {
        display: none!important; 
    }
</style>


<div>

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Edit Promotion Form</h1>

    <div class="card shadow col-md-12">
        <div class="collapse show" id="collapseCardExample">
            <div class="card-body">

                <form name="myForm" action="#" id="productForm" onsubmit="return validateForm()" method="post">
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" id="id" value="{{$item->id}}">

                    <div class="box">
                        {{ csrf_field() }}

                        <div class="box-body col-md-12">
                           
                      


                            <div class="form-group">
                                <label for="fname"><b>Title(En)</b><font color="red">*</font></label>
                                <input type="text" class="form-control" name="title_en" id="title_en" placeholder="Enter Title(En)" value="{{ $item->title_en }}">
                            </div>

                            <div class="form-group">
                                <label for="fname"><b>Title(Th)</b><font color="red">*</font></label> 
                                <input type="text" class="form-control" name="title_th" id="title_th" placeholder="Enter Title(Th)" value="{{ $item->title_th }}">
                            </div>

                            
                            <div class="form-group">
                                <label for="is_active"><b>Type</b><font color="red">*</font></label>
                                    <select class="form-control" name="type" id="type">                                    
                                            <option value="percent" @if($item->type=="percent"){{'selected'}}@endif>Percent</option>
                                            <option value="bath" @if($item->type=="bath"){{'selected'}}@endif>Baht</option>
                                    </select>
                                </div>

                            <div class="form-group">
                                <label for="fname"><b>Discount</b><font color="red">*</font></label>
                                <input type="number" class="form-control" name="discount" id="discount" placeholder="Enter Discount" value="{{ $item->discount }}">
                            </div>


                            <div class="form-group">
                                <label><B>Start Date - End Date</B></label>
                                <div class="input-group">
                                    <input type="text" class="form-control daterangepicker-field" id="daterange"  value="{{$date}}"/>
                                </div>
                            </div>

                            <div class="box-footer col-md-12">
                                <button type="button" class="btn btn-success btn-save btn-loading" style="width: 150px;">Save</button>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    if (!window.moment) {
        document.write('<script src="assets/plugins/moment/moment.min.js"><\/script>');
    }
</script>

<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

<script src="https://cdn.ckeditor.com/4.14.1/full/ckeditor.js"></script>
<script>
    CKEDITOR.replace('editor1', {
        filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form',

    });
    CKEDITOR.replace('editor_th', {
        filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form',

    });
</script>

<script>

    $('body').on('click', '.btn-save', function () {


        
        var id = $('#id').val();
        var title_en = $('#title_en').val();
        var title_th = $('#title_th').val();
        var type = $('#type').val();
        var discount = $('#discount').val();        
        var date = $('#daterange').val();
        var startdate = date.substr(0, 10);
        var enddate = date.substr(13, 24);


        if((title_en == '') || (title_th == '') || (discount == '') || (startdate == '') || (enddate == '') || (type == '')){
            return swal("Wrong!", "Please fill in all required information.", "error");
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            dataType: 'json',
            type:'PUT',
            data:{id:id,title_en:title_en,title_th:title_th,discount:discount,startdate:startdate,enddate:enddate,type:type},
            url: '/cms/promotion/' + id,
            success: function(datas){
                swal("Good job!", "Successfully saved data", "success");
                window.location.href = '/cms/promotion'
            }
        })
    });

</script>
@endsection
