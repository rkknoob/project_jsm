@extends('backend.layouts.content')

@section('content')
<style>

    .text-gray-800{
        font-weight: 500;
    }
    .shadow{
        margin-bottom: 7%;
    }
    label {
        color: #333;
    }
    .btn-icon-split .icon {
        padding-top: 5%;
    }
    .box-footer{
        clear:both;
        text-align : left;
        padding: 15px 0px 15px 0px;
    }
    .note-popover{
        display:none;
    }
    .padding-zero{
        padding-left: 0px!important;
    }
    .profile-image{
        width: 450px;
        padding-top: 25px;
        margin-bottom: 25px;
        border-radius: 0!important;
    }
</style>


<div>

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Add Banner Form</h1>

    <div class="card shadow col-md-12">
        <div class="collapse show" id="collapseCardExample">
            <div class="card-body">

                <form name="myForm" action="#" id="productForm" onsubmit="return validateForm()" method="post">
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" value="">
                    <input type="hidden" name="imagecolor" id="imagecolor">
                    <input type="hidden" name="imageproduct" id="imageproduct" >
                    <input type="hidden" name="imagemobileen" id="imagemobileen" >
                    <input type="hidden" name="imagemobileth" id="imagemobileth" >

                    <div class="box">
                        {{ csrf_field() }}

                        <div class="box-body col-md-12">
                            <div class="form-group">
                                <label for="fname"><b>Name(En)</b><font color="red">*</font></label>
                                <input type="text" class="form-control" name="name_en" id="name_en" placeholder="Enter Name(En)">
                            </div>

                            <div class="form-group">
                                <label for="fname"><b>Name(Th)</b><font color="red">*</font></label>
                                <input type="text" class="form-control" name="name_th" id="name_th" placeholder="Enter Name(Th)">
                            </div>

                            <div class="form-group">
                                <label for="category"><b>Category</b><font color="red">*</font></label>
                                <select class="form-control idcategory" name="idcategory" id="idcategory" >
                                        <option value="">Choose A Category</option>
                                    @foreach($itemCategory as $category)
                                        <option value="{{$category->id}}"> {{$category->name}} </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="category"><b>Product</b><font color="red">*</font></label>
                                <select name="idproduct" class="form-control" id="idproduct" required>
                                    <option value="">Choose A Product</option>
                                </select>
                            </div>

                            <div class="col-md-12 col-12">
                                <div class="row">
                                    <div class="col-sm-5 col-12 padding-zero">
                                        <label><b>Banner Desktop(En)</b><font color="red">*(ขนาดรูป 1200*630)</font></label>
                                        <input type="file" name="img_color" id="img_color">
                                        <img src="{{ $image_display }}" alt="No Image" class="img-fluid rounded mx-auto d-block profile-image" id="showImageColor" >
                                    </div>
                                    <div class="col-sm-1 padding-zero">
                                    </div>

                                    <div class="col-sm-5 col-12 padding-zero">
                                        <label><b>Banner Desktop(Th)</b><font color="red">*(ขนาดรูป 1200*630)</font></label>
                                        <input type="file" name="img_product" id="img_product">
                                        <img src="{{ $image_display }}" alt="No Image" class="img-fluid rounded mx-auto d-block profile-image" id="showImageProduct" >
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-12 col-12">
                                <div class="row">
                                    <div class="col-sm-5 col-12 padding-zero">
                                        <label><b>Banner Mobile(En)</b><font color="red">*(ขนาดรูป 1200*630)</font></label>
                                        <input type="file" name="img_banner_en" id="img_banner_en">
                                        <img src="{{ $image_display }}" alt="No Image" class="img-fluid rounded mx-auto d-block profile-image" id="showImageMobileen" >
                                    </div>
                                    <div class="col-sm-1 padding-zero">
                                    </div>

                                    <div class="col-sm-5 col-12 padding-zero">
                                        <label><b>Banner Mobile(Th)</b><font color="red">*(ขนาดรูป 1200*630)</font></label>
                                        <input type="file" name="img_banner_th" id="img_banner_th">
                                        <img src="{{ $image_display }}" alt="No Image" class="img-fluid rounded mx-auto d-block profile-image" id="showImageMobileth" >
                                    </div>
                                </div>
                            </div>

                            <div class="box-footer col-md-12">
                                <button type="button" class="btn btn-success btn-save btn-loading" style="width: 150px;">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    if (!window.moment) {
        document.write('<script src="assets/plugins/moment/moment.min.js"><\/script>');
    }
</script>

<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

<script>

    $('.idcategory').change(function(){
        if($(this).val !='' ){
            var select = $(this).val();
            var _token = $('input[name="_token"]').val();

            $.ajax({
                url: "/cms/banner/add/fetch",
                method: "POST",
                data: {select:select,_token:_token},
                success:function(data){
                    // console.log(data.datas)
                    // $("#idproduct").html(data);
                    $('#idproduct').empty();
                    $.each(data.datas, function(i, item) {
                        $('#idproduct').append('<option value="'+ item.id +'">'+ item.name_en +'</option>');
                    })
                }
            })
        }
    });

    $('body').on('click', '.btn-save', function () {

        var name_en = $('#name_en').val();
        var name_th = $('#name_th').val();
        var id_category = $('#idcategory').val();
        var id_product = $('#idproduct').val();
        var img_en = $('#imagecolor').val();
        var img_th = $('#imageproduct').val();
        var image_mobile_en = $('#imagemobileen').val();
        var image_mobile_th = $('#imagemobileth').val();

        if((name_en == '') || (name_th == '') || (img_en == '') || (img_th == '') || (image_mobile_en == '') || (image_mobile_th == '')|| (id_category == '') || (id_product == '')){
            return swal("Wrong!", "Please fill in all required information.", "error");
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            dataType: 'json',
            type:'POST',
            data:{name_en:name_en,name_th:name_th,img_en:img_en,img_th:img_th,image_mobile_en:image_mobile_en,image_mobile_th:image_mobile_th,id_category:id_category,id_product:id_product},
            url: '/cms/banner',
            success: function(datas){
                swal("Good job!", "Successfully saved data", "success");
                window.location.href = '/cms/banner'
            }
        })
    });

    ///--- Image Desktop Eng ---///
    $('body').on('change', 'input[name= "img_color"]', function () {
        if ($('input[name ="img_color"]').val() != '') {

            var _URL = window.URL || window.webkitURL;
            var file, img;
            var file_data = $('input[name= "img_color"]').prop('files')[0];
            var form_data = new FormData();
            if ((file = this.files[0])) {
                img = new Image();
                img.onload = function() {

                    if((this.width != '1200') && (this.height != '630')){
                        return  swal("Cancelled", "Image upload failed.", "error");
                    }

                    form_data.append('img_color', file_data);
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: '/cms/banner/uploadimage',
                        dataType: 'json',
                        type: 'POST',
                        data: form_data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function success(resp) {
                            var $link = "<?php echo url('/public/banner/'); ?>";
                            $('input[name=imagecolor]').val(resp.data);
                            $('#showImageColor').attr("src", $link +'/' + resp.data);
                            swal("Good job!", "The photo was uploaded successfully.", "success");
                        },
                        error: function error(xhr, textStatus, errorThrown) {
                            showFormErrors(xhr, $('#customerForm'));
                            console.log(errorThrown);
                        }
                    });
                };
                img.onerror = function() {
                    alert( "not a valid file: " + file.type);
                };
                img.src = _URL.createObjectURL(file);
            }
        }
    });

    ///--- Image Dektop Thai ---///
    $('body').on('change', 'input[name= "img_product"]', function () {
        if ($('input[name ="img_product"]').val() != '') {

            var _URL = window.URL || window.webkitURL;
            var file, img;
            var file_data = $('input[name= "img_product"]').prop('files')[0];
            var form_data = new FormData();
            if ((file = this.files[0])) {
                img = new Image();
                img.onload = function() {

                    if((this.width != '1200') && (this.height != '630')){
                        return  swal("Cancelled", "Image upload failed.", "error");
                    }

                    form_data.append('img_product', file_data);
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: '/cms/banner/uploadimage',
                        dataType: 'json',
                        type: 'POST',
                        data: form_data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function success(resp) {
                            var $link = "<?php echo url('/public/banner/'); ?>";
                            $('input[name=imageproduct]').val(resp.data);
                                $('#showImageProduct').attr("src", $link +'/'+ resp.data);

                                swal("Good job!", "The photo was uploaded successfully.", "success");
                            },
                            error: function error(xhr, textStatus, errorThrown) {
                                showFormErrors(xhr, $('#customerForm'));
                                console.log(errorThrown);
                            }
                        });

                    };
                    img.onerror = function() {
                        alert( "not a valid file: " + file.type);
                    };
                    img.src = _URL.createObjectURL(file);
                }
            }
        });

        //--- Image Mobile Eng ---// 
        $('body').on('change', 'input[name= "img_banner_en"]', function () {
            if ($('input[name ="img_banner_en"]').val() != '') {

                var _URL = window.URL || window.webkitURL;
                var file, img;
                var file_data = $('input[name= "img_banner_en"]').prop('files')[0];
                var form_data = new FormData();
                if ((file = this.files[0])) {
                    img = new Image();
                    img.onload = function() {

                        if((this.width != '1200') && (this.height != '630')){
                            return  swal("Cancelled", "Image upload failed.", "error");
                        }

                        form_data.append('img_banner_en', file_data);
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: '/cms/banner/uploadimage',
                            dataType: 'json',
                            type: 'POST',
                            data: form_data,
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function success(resp) {
                                var $link = "<?php echo url('/public/banner/'); ?>";
                                $('input[name=imagemobileen]').val(resp.data);
                                $('#showImageMobileen').attr("src", $link +'/'+ resp.data);

                                swal("Good job!", "The photo was uploaded successfully.", "success");
                            },
                            error: function error(xhr, textStatus, errorThrown) {
                                showFormErrors(xhr, $('#customerForm'));
                                console.log(errorThrown);
                            }
                        });

                    };
                    img.onerror = function() {
                        alert( "not a valid file: " + file.type);
                    };
                    img.src = _URL.createObjectURL(file);
                }
            }
        });

        //--- Image Mobile Thai ---// 
        $('body').on('change', 'input[name= "img_banner_th"]', function () {
            if ($('input[name ="img_banner_th"]').val() != '') {

                var _URL = window.URL || window.webkitURL;
                var file, img;
                var file_data = $('input[name= "img_banner_th"]').prop('files')[0];
                var form_data = new FormData();
                if ((file = this.files[0])) {
                    img = new Image();
                    img.onload = function() {

                        if((this.width != '1200') && (this.height != '630')){
                            return  swal("Cancelled", "Image upload failed.", "error");
                        }

                        form_data.append('img_banner_th', file_data);
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: '/cms/banner/uploadimage',
                            dataType: 'json',
                            type: 'POST',
                            data: form_data,
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function success(resp) {
                                var $link = "<?php echo url('/public/banner/'); ?>";
                                $('input[name=imagemobileth]').val(resp.data);
                                $('#showImageMobileth').attr("src", $link +'/'+ resp.data);

                                swal("Good job!", "The photo was uploaded successfully.", "success");
                            },
                            error: function error(xhr, textStatus, errorThrown) {
                                showFormErrors(xhr, $('#customerForm'));
                                console.log(errorThrown);
                            }
                        });

                    };
                    img.onerror = function() {
                        alert( "not a valid file: " + file.type);
                    };
                    img.src = _URL.createObjectURL(file);
                }
            }
        });

</script>
@endsection
