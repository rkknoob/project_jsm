<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<style>
	li{
		list-style: none !important;
	}
	.hidden{
		display:none !important;
	}
</style>

<body>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center mt-5">
				<a href="https://www.jungsaemmool-th.com/">
					<img src="http://18.138.103.125/public/Email/logo3.jpg" style="list-style: none !important;" width="10%" alt="">
				</a>
			</div>

			<div class="col-lg-12 text-center">
				<div class="card p-3">
					<div class="col-lg-12 text-center mt-3">
						<h3>ได้รับคำสั่งซื้อของคุณเรียบร้อยแล้ว</h3>
					</div>

					<div class="col-lg-12 text-left mt-3">
						<p style="font-size: 1.5rem;">สวัสดี คุณ {{$name}} </p>
						<p style="font-size: 1.1rem;">ขอขอบคุณที่ไว้วางใจในการเลือกผลิตภัณฑ์ของเรา
							JUNGSAEMMOOL </p>
						<p style="font-size: 1.1rem;">เราขอยืนยันว่าได้รับคำสั่งซื้อหมายเลข
							<span style="font-size: 1.3rem;">{{$trackNumber}}</span>
							เรียบร้อยแล้ว 
							<a href="https://www.scgexpress.co.th/tracking">
								<img src="http://18.138.103.125/public/Email/scg-express.png" width="3%" alt="">
							</a>
						</p>
						<p style="font-size: 1.1rem;">เราหวังว่าคุณจะมีความสุขกับการใช้สินค้าชิ้นใหม่ของคุณจาก
							<a href="https://www.jungsaemmool-th.com/">
								JUNGSAEMMOOL Thailand
							</a>
						</p>
					</div>

					<div class="col-lg-12 mt-5">
						<img src="http://18.138.103.125/public/Email/tick.png" width="3%"
							alt="">
						<span class="ml-4" style="font-size: 1.6rem;">ขั้นตอนต่อไป</span>
						<ul class="mt-3">
							<li style="font-size: 1.2rem;">กำลังตรวจสอบสินค้าและจัดส่งสินค้าของคุณ</li>
						</ul>
					</div>
				</div>
			</div>

			<div class="col-lg-12">
				<div class="card p-3 mt-3">
					<div class="col-lg-12 mt-5">
						<img src="http://18.138.103.125/public/Email/location.png" width="3%"
							alt="">
						<span class="ml-4" style="font-size: 1.6rem;">ที่อยู่การจัดส่งของคุณ</span>
						<table class="mt-3 w-100">
							<tbody>
								<tr>
									<td style="font-size: 1.2rem;">ชื่อ-นามสกุล :
									</td>
									<td width="70%" style="font-size: 1.2rem;">
										{{$name}}
									</td>
								</tr>
								<tr>
									<td style="font-size: 1.2rem;">ที่อยู่ : </td>
									<td width="70%" style="font-size: 1.2rem;">
										{{$address}}
									</td>
								</tr>
								<tr>
									<td style="font-size: 1.2rem;"> เบอร์โทร :</td>
									<td width="70%" style="font-size: 1.2rem;">{{$phone}}</td>
								</tr>
								<tr>
									<td style="font-size: 1.2rem;"> อีเมล :</td>
									<td width="70%" style="font-size: 1.2rem;">{{$email}}</td>
								</tr>
							</tbody>
						</table>

					</div>
				</div>
			</div>

			<div class="col-lg-12">
				<div class="card p-3 mt-3 mb-5">
					<div class="col-lg-12 mt-5">
						<img src="http://18.138.103.125/public/Email/box.png" width="3%" alt="">
						<span class="ml-4" style="font-size: 1.6rem;">คำสั่งซื้อ</span>
						<table class="mt-3 w-100">
							<tbody>
								@foreach($listProduct as $product)
								<tr>
									<td width="40%">
										<img src="http://18.138.103.125/public/product/{{$product->cover_img}}" style="width:50%;">
										<!-- <img src="public/Email/8137.jpg" width="80%" alt=""> -->
									</td>
									<td width="60%" style="font-size: 1.2rem;">
										{{ $product->name_en }}

										[Option] :
										@foreach($listSku as $sku)
											@if($product->id_sku == $sku->sku )
												{{$sku->name_en}}
											@endif 
                                    	@endforeach
										<br> 
										ราคา(ต่อหน่วย)  THB{{$product->price}}
										<br>
										จำนวน {{ $product->qty }} ชิ้น
										<br>
										ราคารวม THB{{$product->sumPrice}}
									</td>
								</tr>
								<!-- <tr>
									<td></td>
									<td class="text-danger" style="font-size:1.2rem;">
										
									</td>
								</tr>
								<tr>
									<td></td>
									<td class="text-black-50" style="font-size: 1.2rem;">
										
									</td>
								</tr>
								<tr>
									<td></td>
									<td class="text-black-50" style="font-size: 1.2rem;">
										
									</td>
								</tr> -->
								@endforeach
							</tbody>
						</table>

						<hr class="border-darken-2">
						<table class="mt-3 w-100">
							<tbody>
								<tr>
									<td width="50%" style="font-size: 1.2rem;">
										ยอดรวม:</td>
									<td style="font-size: 1.2rem;">
										{{$totalOrder}}
									</td>
									<td style="font-size: 1.2rem;">THB</td>
								</tr>
								<tr>
									<td width="50%" style="font-size: 1.2rem;">
										ค่าธรรมเนียมจัดส่ง:</td>
									<td style="font-size: 1.2rem;">
										{{$totalShipping}}
									</td>
									<td style="font-size: 1.2rem;">THB</td>
								</tr>
								<!-- <tr hidden>
									<td width="50%" style="font-size: 1.2rem;">
										คูปองส่วนลด:</td>
									<td style="font-size: 1.2rem;">
										20.00
									</td>
									<td style="font-size: 1.2rem;">THB</td>
								</tr> -->
								<tr>
									<td width="50%" class="text-danger"
										style="font-size: 1.2rem;">
										ยอดรวม (รวม VAT ถ้ามี):</td>
									<td style="font-size: 1.2rem;"
										class="text-danger">
										{{$sumPrice}}
									</td>
									<td style="font-size: 1.2rem;"
										class="text-danger">THB</td>
								</tr>
							</tbody>
						</table>
						<hr class="border-darken-2">
						<!-- <table class="mt-3 w-100 hidden"  >
							<tbody>
								<tr>
									<td width="50%" style="font-size: 1.2rem;">
										วิธีการจัดส่ง:</td>
									<td style="font-size: 1.2rem;">
										STANDARD
									</td>
								</tr>
								<tr>
									<td width="50%" style="font-size: 1.2rem;">
										ช่องทางการชำระเงิน:</td>
									<td style="font-size: 1.2rem;">
										Kasikorn Bank ATM
									</td>
								</tr>
							</tbody>
						</table> -->

						<div class="col-lg-12 text-center mt-5 mb-4">
							<button class="btn btn-light p-1 text-white w-50" style="font-size: 1.5rem; background-color: #E42498; text-align: center;">
								<img src="http://18.138.103.125/public/Email/shopping-cart.png" width="5%" alt="">
									Shopping ต่อเลย
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
