@extends('layouts.template.frontend')

<style type="text/css">

    #ju-container .ju-page-title {
        margin-top: 73px!important;
        margin-bottom: 40px!important;
    }

    .fot {
        font-size: 16px;
    }

    .text_product {
        color: #FF33A5;
        font-weight: 700;
        margin-left: 0.3%;
    }

    .prd-list {
        margin-top: 10px;
    }

    /*-------- pagination --------*/
    .pagination>li>a,
    .pagination>li>span {
        color: #333 !important;
    }

    .pagination>.active>a,
    .pagination>.active>a:focus,
    .pagination>.active>a:hover,
    .pagination>.active>span,
    .pagination>.active>span:focus,
    .pagination>.active>span:hover {
        z-index: 3;
        color: #fff !important;
        cursor: default;
        background-color: #333 !important;
        border-color: #333 !important;
    }

    .pagination>li:first-child>a,
    .pagination>li:first-child>span {
        border-top-left-radius: 0px !important;
        border-bottom-left-radius: 0px !important;
    }

    .pagination>li:last-child>a,
    .pagination>li:last-child>span {
        border-top-right-radius: 0px !important;
        border-bottom-right-radius: 0px !important;
    }

    /*-------- pagination --------*/

    .form-control-static{
        font-family: "Exo2Regular" !important;
    }
    .woocommerce-ordering{
        font-family: "Exo2Regular" !important;
    }
    .nav-tabs {
        font-family: "Exo2Regular" !important;

    }
    .fot{
        font-family: "Exo2Regular" !important;
    }

    /* Vertical IPad */
    @media screen and (max-width: 768px) {
        .media-m {
            height: 100%;
        }
    }
    /* Horizontal IPhone X */
    @media  screen and (max-width: 812px) {
        .media-m {
            height: 100%;
        }
    }

    /* Horizontal 6/7/8 Plus*/
    @media screen and (max-width: 736px) {
        .media-m {
            height: 45%;
        }
        #ju-container .ju-page-title {
            margin-top: 10px!important;
            text-align: center;
            margin-bottom: 40px;
        }
    }

    /* Vertical IPhone X/ XS/ 6/7/8  */
    @media only screen and (max-width: 375px) {
        .media-m {
            height: 50%;
        }
    }


</style>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> JSM PRODUCT </title>
    <meta name="description" content="">
    <meta name="keywords" content="">
</head>
@php
    $locale = session()->get('locale');
@endphp
@section('content')

    <div id="ju-container">
        <div id="ju-content" class="container">
            <div class="ju-page-title" id="demo">
                <h1 class="entry-title text-thai text-center">Search : "{{$datasearch}}"</h1>
            </div>
            <div class="woocommerce_categoy_result clearfix">
                <div class="form-control-static pull-left">
                    TOTAL
                </div>
                <div class="form-control-static pull-left text_product">
                    {{$Total}}
                </div>
                <div class="form-control-static pull-left">
                    PRODUCTS
                </div>

                <div class="pull-right">
                    <form action="/search" class="woocommerce-ordering" method="get">
                        <input type="hidden" name="product_type" id="product_type" value="{{$id}}" />
                        <input type="hidden" name="orderby" id="orderby" value="{{$forma}}" />
                        <label class="">
                            <select name="orderby"  id="orderby"  onchange="this.form.submit()">
                                <option value="1"
                                    {{ ($forma == '1') ? "selected" : "" }}>Lowest Price</option>
                                <option value="2"
                                    {{ ($forma == '2') ? "selected" : "" }}>Highest Price</option>
                                <option value="3"
                                    {{ ($forma == '3') ? "selected" : "" }}>Product Name</option>
                                <option value="0"
                                    {{ ($forma == '0') ? "selected" : "" }}>New Item</option>
                            </select>
                        </label>
                        <input type="hidden" name="datasearch" id="datasearch" value="{{$datasearch}}" />
                    </form>
                </div>
            </div>
            <ul class="nav nav-tabs">


            </ul>
            <br/>

            <div class="tab-content prd-list">
                <div id="product" class="tab-pane fade in active">
                    <div id="all" class="product">
                        @foreach ($products as $chunk)
                            <div class="row">
                                @foreach ($chunk as $Products)
                                    <div class="col-lg-3 col-md-3 col-6 col-xs-6 media-m">
                                        <a class="product_link_wrap" href="/product/detail/view/{{$Products->id}}">
                                            <img src="{{ url('/public/product/'.$Products->cover_img) }}" alt="product"
                                                 style="width:100%;">
                                        </a>
                                        <div class="text-center prd-info">

                                            <div class="preview-box clear bold">
                                    <span class="right">
                                        <span class="quick_basket" rel="1182" style="cursor:pointer">
                                            <i class="fa fa-shopping-cart" hidden></i>
                                            <br/>
                                        </span>
                                    </span>
                                            </div>
                                            <div class="fot" style="cursor:hand">
                                                @if($locale==='th')
                                                    {{$Products->name_en}}
                                                @else
                                                    {{$Products->name_en}}
                                                @endif
                                            </div>
                                            <p class="subname"></p>
                                            <div class="fot">
                                                <b> {{ number_format($Products->price,2) }} @lang('lang.baht')</b>
                                            </div>
                                            <span class="amount">

                                    <span>
                                    </span></span>
                                            </strong>
                                        </div>
                                        <hr class="clear">
                                    </div>
                                @endforeach
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="text-center">
                    <ul class="pagination">
                        {!! $Product_type->render() !!}
                    </ul>
                </div>
            </div>

        </div>
        <!--#ju-content-->
    </div>
    <!--#ju-container-->
    <!-- //content -->

@endsection
