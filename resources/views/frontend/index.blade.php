@extends('layouts.template.frontend')



<style type="text/css">
    .home-product-tab .wpb_tabs .wpb_tabs_nav {
        font-family: "Exo2Regular"!important;
    }
    .cate-hover{
        font-family: "Exo2Regular"!important;
    }
    .column-index{
        margin-top: 0;
        padding-right: 0px !important;
        padding-left: 0px !important;
    }
    .carousel-indicators li {
        display: inline-block !important;
        box-sizing: content-box !important;
        width: 8px !important;
        height: 8px !important;
        background: black !important;
        border: solid 3px white !important;
        border-color: rgba(255, 255, 255, 0.44) !important;
        margin: 4px !important;
        border-radius: 15px !important;
    }
    .carousel-indicators .active {
        display: inline-block !important;
        box-sizing: content-box !important;
        width: 8px !important;
        height: 8px !important;
        background: white !important;
        border: solid 3px white !important;
        border-color: rgba(0, 0, 0, 0.44) !important;
        margin: 4px !important;
        border-radius: 15px !important;
    }
    li.active {
        color: red;
    }
    @media not all,(-webkit-transform-3d){
        .carousel-inner>.item.active.left, .carousel-inner>.item.prev{
            transform: translate3d(-100%,0,0) !important;
            left: 0;
        }
        .carousel-inner>.item.active.right, .carousel-inner>.item.next{
            left: 0;
            -webkit-transform: translate3d(100%,0,0)!important;
            transform: translate3d(100%,0,0)!important;

        }
    }
    .home-product-tab .wpb_tabs .wpb_tabs_nav li.active a {
        color: #000;
        border: 4px solid #fff;
        padding: 10px 35px;
    }
    .home-product-tab .wpb_content_element .wpb_tabs_nav li.ui-tabs-active,
    .home-product-tab .wpb_content_element .wpb_tabs_nav .active {
        background-color: #fff;
    }
    .home-product-tab .wpb_content_element .wpb_tabs_nav li.ui-tabs-active,
    .home-product-tab .wpb_content_element .wpb_tabs_nav li a .active {
        color: black !important;
    }
    #banner {
        position: fixed;
        z-index: 999;
        top: 100px;
        left: 50px;
        height: 300px;
        width: 342px;
        background: #0d0d0d;
    }

    #close {
        position: absolute;
        top: 0px;
        right: 0px;
        font-family: Arial, Helvetica;
        font-size: 14px;
        color: #f4524d;
        cursor: pointer;
        font-weight: bold;
    }
    #shadowbox {
        position: fixed;
        z-index: 998;
        height: 100%;
        width: 100%;
    }

    /* Slide img */
    .carousel-inner {
    > .item {
        opacity: 0;
        top: 0;
        left: 0;
        width: 100%;
        display: block;
        position: absolute;
        z-index:0;
        transition: none;
        transform: translate3d(0,0,0) !important;
    &:first-of-type {
         position:relative;
     }
    }
    > .active {
        opacity: 1;
        z-index:3;
    }

    > .next.left,
    > .prev.right {
        transition: opacity 0.6s ease-in-out;
        opacity: 1;
        left: 0;
        z-index:2;
    }
    > .active.left,
    > .active.right {
        z-index:1;
    }
    }
    .carousel-control {
        z-index:4;
    }

    /* Make the youtube video responsive */
    .iframe-container{
        position: relative;
        width: 100%;
        padding-bottom: 56.25%;
        height: 0;
    }
    .iframe-container iframe{
        position: absolute;
        top:0;
        left: 0;
        width: 100%;
        height: 100%;
    }

    @media screen and (min-width: 1024px) {
        /* #mobile-video{
            display:none;
        } */
    }
    @media screen and (max-width: 768px) {
        /* #mobile-video{
            display:none;
        } */
        .vc_row {
            margin-top: -45px!important;
        }
        /* #video_bg {
            margin-top: 4%;
        } */
    }
    @media screen and (min-width: 768px) {
        /* #mobile-video{
            display:none;
        } */
    }
    @media screen and (min-width: 501px) {
        .type_mobile{
            display:none!important;
        }
        div.menu{
            position: absolute !important;
        }
    }
    @media screen and (max-width: 500px) {
        .type_mobile {
            display: block!important;
        }
        .type_desktop{
            display: none!important;
        }
        .list_shopping2x {
            text-align: left;
            padding: 0 10px;
        }
        .tit_bg {
            width: 100%;
            background: url('/jsmbeauty/src/Icon/tit_bg.png') repeat-x 0 55px;
            text-align: center;
        }
        .more {
            text-align: center;
            width: 100%;
            margin: 30px auto 60px;
        }
        .btn_moreGray {
            width: 100px;
            height: 38px;
            line-height: 38px;
            color: #fff;
            background-color: #333;
            border: 0px none;
            font-weight: normal;
            display: block;
            margin: 0 auto;
            font-size: 16px;
        }
        .list_shopping2x .list_shoppingInfo {
            margin-top: 5px;
            text-align: center;
            font-size: 12px;
            color: #000; 
            font-weight: bold;
        }
        .none-mobile{
            display:none !important;
        }
    }
    @media screen and (min-width: 320px) {
        /* #mobile-video{
            margin-top: 8%;
            margin-right: -2%;
        } */
    }

    div.content{
        position: relative;
    }

</style>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <title>JUNG SAEM MOOL｜Cosmetics - Official Site</title>
</head>

@section('content')

<div class="content">

    <!-- pop-up -->
    @if($popup->is_active=="Y")
        <div id="banner"
             style="z-index: 1562207196; position: absolute;top: -35px; left: 0px; width: auto; background-color: white;">
            <img src="/public/popup/{{$popup->img_en}}">
            <div id="close" onclick="yourfunction()">
                <img border="0"
                    src="//www.jsmbeauty.us/storage/jsmbeautyEN/www/frontstore/default_5/EN/Frontend/Layout/img/btn_popup_close.gif"
                    style="margin: 5px 6px -2px 2px;">
            </div>
        </div>
    @endif
    <!-- pop-up -->

    <!-- Code For Desktop -->
    <div id="ju-container">
        <div id="ju-content">
            <div class="vc_row wpb_row vc_row-fluid vc_custom_1464829003762 vc_row-has-fill vc_row-o-full-height vc_row-o-columns-bottom vc_row-flex" style="min-height: 4.57821vh;">
                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-has-fill">
                    <div id="video_bg" class="type_desktop mobile" style="background-color: black;">
                        <iframe
                            src="{{$video->link_video}}&controls=0&autoplay=1&loop=1&mute=1&rel=0"
                            frameborder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen>
                        </iframe>

                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 type_mobile" id="mobile-video" style="padding-right: 0px; padding-left: 0px;margin-bottom: 9%;">        
                        <img src="public/popup/{{$video->imagemobile}}">
                    </div>
                </div>
                
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner vc_custom_1446006821477">
                        <div class="wpb_wrapper">

                            <!-- Desktop Banner Index -->
                            <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-has-fill">
                                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    
                                        <!-- Indicators -->
                                        <ol class="carousel-indicators">
                                            @foreach($banner as $i => $banners )
                                                <li class="@if($i == 0) active @endif"  data-target="#myCarousel" data-slide-to="{{$i}}"></li>
                                            @endforeach
                                        </ol>

                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner">
                                            @foreach($banner as $i => $banners )
                                                <div class="item @if($i == 0) active @endif" ><!--style="width: 100%;"-->
                                                    <a href="{{ url('/product/detail/view', ['id' => $banners->id_product]) }}" style="cursor:hand">
                                                        <img src="/public/banner/{{$banners->img_en}}" style="width: 1200px;margin-left: auto;margin-right: auto;">
                                                    </a>
                                                </div>
                                            @endforeach
                                        </div>

                                        <!-- Left and right controls -->
                                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left "></span>                       
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>         
                                </div>
                            </div><!-- .vc_row wpb_row vc_inner vc_row-fluid-->
                            <!-- End Desktop Banner Index -->

                            <!-- Desktop Event -->
                            <h1 style="font-size: 50px;color: #ffffff;line-height: 60px;text-align: center; padding-top: 40px !important;" class="vc_custom_heading vc_custom_1467341574385">
                                <a href="/event">
                                    @lang('lang.event')
                                </a>
                            </h1>
                    
                            <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1467342034471 vc_row-has-fill">
                                @foreach($event as $events)
                                <div class="col-6 col-xs-6 col-md-3 col-lg-3 col-sm-6" style="padding-top: 35px;">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_single_image wpb_content_element vc_align_center">
                                                <figure class="wpb_wrapper vc_figure">
                                                    <a href="{{ url('/event/details', ['id' => $events->id]) }}" target="_self" class="vc_single_image-wrapper vc_box_border_grey"> 
                                                        <img width="385" height="452" src="/public/event/{{$events->banner1}}" class="vc_single_image-img attachment-full" alt="event">
                                                    </a>
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div><!-- .vc_row wpb_row vc_inner vc_row-fluid vc_custom_1467342034471 vc_row-has-fill-->
                            <!-- End Desktop Event -->

                        </div>
                    </div><!--.vc_column-inner vc_custom_1446006821477-->
                </div><!--.wpb_column vc_column_container vc_col-sm-12-->
            </div><!--.vc_row wpb_row vc_row-fluid vc_custom_1464829003762 vc_row-has-fill vc_row-o-full-height vc_row-o-columns-bottom vc_row-flex-->

            <div class="vc_row wpb_row vc_row-fluid home-product-tab vc_custom_1467341696348 vc_row-has-fill">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">

                            <h1 style="font-size: 50px;color: #ffffff;text-align: center; margin-top: 4%!important; line-height: 50px; padding-top: 40px !important;" style=""class="vc_custom_heading vc_custom_1467342620922">
                                <a href="/Product/Category/list/cid/0">
                                    @lang('lang.product')
                                </a>
                            </h1>
                        
                            <div class="wpb_tabs wpb_content_element" data-interval="0">
                                <div class="wpb_wrapper wpb_tour_tabs_wrapper ui-tabs vc_clearfix ui-widget ui-widget-content ui-corner-all">

                                    <ul class="wpb_tabs_nav ui-tabs-nav vc_clearfix ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" role="tablist">
                                        <li class="ui-state-default ui-corner-top navMenus active" role="tab" tabindex="0" aria-controls="tab-1b1d9d2f-6ba7-0" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true">
                                            <a  data-toggle="tab" href="#best" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-1"> BEST PRODUCTS </a>
                                        </li>
                                        @foreach($Category as $Categorys)
                                        <li class="ui-state-default ui-corner-top navMenus" role="tab" tabindex="-1" aria-controls="tab-a2934e91-9508-3" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false">
                                            <a data-toggle="tab" href="#base"  onclick="products({{$Categorys->id}});" > {{$Categorys->name}} </a>
                                        </li>
                                        @endforeach
                                    </ul>

                                    <div class="tab-content">
                                        <div id="best" class="tab-pane fade in active" >
                                            <div class="wpb_tab ui-tabs-panel wpb_ui-tabs-hide vc_clearfix ui-widget-content ui-corner-bottom" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false" style="display: block;">
                                                <div class="woocommerce columns-4">
                                                    <div class="row row-products" style="margin-right: -6px; margin-left: -6px;"> 
                                                        @if(! $Product->isEmpty())
                                                            @foreach($Product as $Products)
                                                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-6 post-18124 product type-product status-publish has-post-thumbnail product_cat-tools product_cat-227 shipping-taxable purchasable product-type-simple product-cat-tools instock" style="padding-right: 6px; padding-left: 6px;">
                                                                    <a href=""></a>
                                                                    <a class="product_link_wrap"
                                                                    href="{{ url('product/detail/view/'.$Products->id) }}">
                                                                        <img width="600" height="600"
                                                                            src="{{ url('/public/product/'.$Products->cover_img) }}"
                                                                            class="MS_prod_img_l attachment-shop_catalog size-shop_catalog wp-post-image"
                                                                            alt="JUNGSAEMMOOL Pre-tect Sun Waterfull">
                                                                        <div class="cate-hover">
                                                                            <h3>{{$Products->name_en}}</h3>
                                                                            <strong class="price">
                                                                                <span class="amount">
                                                                                    {{ number_format($Products->price,2) }} @lang('lang.baht')
                                                                                </span>
                                                                            </strong>
                                                                            <div class="small post_excerpt" hidden>{{$Products->name_en}}
                                                                            </div>
                                                                        </div>
                                                                    </a>
                                                                    <hr class="clear">
                                                                </div>
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="base" class="tab-pane fade" >
                                            <div class="wpb_tab ui-tabs-panel wpb_ui-tabs-hide vc_clearfix ui-widget-content ui-corner-bottom" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false" style="display: block;">
                                                <div class="woocommerce columns-4">
                                                    <div class="row row-products" style="margin-right: -6px; margin-left: -6px;">
                                                        <div id="product_base">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- .wpb_wrapper -->
                    </div><!-- .vc_column-inner -->
                </div><!-- .wpb_column vc_column_container vc_col-sm-12 -->
            </div><!-- .vc_row wpb_row vc_row-fluid home-product-tab vc_custom_1467341696348 vc_row-has-fill product 끝-->
         
            <div class="vc_row wpb_row vc_row-fluid vc_custom_1464829672746 vc_row-has-fill">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner vc_custom_1446006821477">
                        <div class="wpb_wrapper">
                            <div class="wpb_single_image wpb_content_element vc_align_center">
                                <figure class="wpb_wrapper vc_figure">
                                    <a href="/artisttip" target="_self" class="vc_single_image-wrapper vc_box_border_grey">
                                        <img width="1602" height="836" src="{!!asset('jsmbeauty/src/FINDYOURBEAUTY.jpg')!!}" class="vc_single_image-img attachment-full" alt="FINDYOURBEAUTY"></a>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Code For Desktop -->



</div>
    



   
@endsection


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
<script src='{{asset('jsmbeauty/js/jquery.zoom.min.js')}}'></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
</script>
<script>
    // pop-up//



    var locale = '{{ config('app.locale') }}';
    console.log(locale);
    if(locale == 'th'){
        var ba = "บาท";
    }else {
        var ba = "Baht";
    }

    //Your Function
    function yourfunction() {
        $('#banner').hide();
    }
    $('#click').click(function() {
        $('#shadowbox, #banner').show();
    });

    $('#test').click(function() {
        alert('Button was clicked');
    })
    // pop-up //

    function products(id) {



        $.ajax({
            dataType: 'json',
            type: 'get',
            data: {
                id: id
            },
            url: '/product/list/' + id,
            success: function(data) {

                var html = '';
                $.each(data.datas, function(index, itemdata) {
                    html +=
                        '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-6 post-18124 product type-product status-publish has-post-thumbnail product_cat-tools product_cat-227 shipping-taxable purchasable product-type-simple product-cat-tools instock" style="padding-right: 6px; padding-left: 6px;">';
                    html += 
                        '<a class="product_link_wrap" href="/product/detail/view/' + itemdata.id + '">';
                    html += '<img src="/public/product/' + itemdata.cover_img + '"/>';
                    html += '<div class="cate-hover">';
                    html += '<h3>' + itemdata.name_en + '</h3>';
                    html += '<strong class="price"><span class="amount">' + formatNumber(itemdata.price) +
                        ' </span></strong>' + ba ;
                    // html += '<div class="small post_excerpt">' + itemdata.name_en + '</div>';
                    html += '</div>';
                    html += '</div>';
                });
                $('#product_base').html(html);

            }
        })


    }

    // 2. This code loads the IFrame Player API code asynchronously.
    // 2. This code loads the IFrame Player API code asynchronously.
    var tag = document.createElement('script');

    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    // 3. This function creates an <iframe> (and YouTube player)
    //    after the API code downloads.
    var player;
    function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
            width: '100%',
            videoId: 'T9CrVR9HJQQ',
            playerVars: { 'autoplay': 1, 'playsinline': 1 },
            events: {
                'onReady': onPlayerReady
            }
        });
    }

    // 4. The API will call this function when the video player is ready.
    function onPlayerReady(event) {
        event.target.mute();
        event.target.playVideo();
    }

    // 5. The API calls this function when the player's state changes.
    //    The function indicates that when playing a video (state=1),
    //    the player should play for six seconds and then stop.
    var done = false;
    function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.PLAYING && !done) {
            setTimeout(stopVideo, 10000);
            done = true;
        }
    }
    function stopVideo() {
        player.stopVideo();
    }


    function formatNumber (num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
    }

</script>
