
<form id="paymentform" method="post" action="{{ $payment_url }}">
    <input type="hidden" name="version" value="{{ $version }}">
    <input type="hidden" name="merchant_id" value="{{ $merchant_id }}">
    <input type="hidden" name="currency" value="{{ $currency }}">
    <input type="hidden" name="result_url_1" value="{{ $result_url_1 }}">
    <input type="hidden" name="hash_value" value="{{ $hash_value }}">
    <input type="hidden" name="payment_description" value="{{ $payment_description }}" >
    <input type="hidden" name="order_id" value="{{ $order_id }}" >
    <input type="hidden" name="amount" value="{{ $amount }}" >
    <input type="hidden" name="user_defined_1" value="{{ $user_defined_1 }}" >

</form>  

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
<script>
    $(document).ready(function() {
       document.forms.paymentform.submit();
    //   setTimeout(() => {
    //     document.getElementById("paymentform").submit();
    //   }, 10);
    });
</script>