@extends('layouts.template.frontend')

<style type="text/css">

    @font-face {
        font-family:'Exo2-Bold';
        src: url({!! asset("fonts/Exo2/Exo2-Bold.ttf") !!}) ;
    }
    @font-face {
        font-family:'Exo2-Medium';
        src: url({!! asset("fonts/Exo2/Exo2-Medium.ttf") !!}) ;
    }

    /* ภาษาไทย */
    @font-face {
        font-family: 'SukhumvitSet';
        src: url('/fonts/Sukhumvit/SukhumvitSet-Text.ttf');
    }
    .text-thai {
        font-family: 'SukhumvitSet';
        font-style: normal;
        letter-spacing: 0px;
    }

    #ju-container {
        padding: 50px 0!important;
    }
    .exo2-bold{
        font-family: 'Exo2-Bold';
    }
    .bolder{
        font-weight: bolder;
    }
    .link_del a {
        width: 37px;
        margin: 0px auto;
    }
    .remove, .modify, .empty, .continue, .order{
        border-radius: 0!important;
        font-family: "Exo2-Medium" ;
    }
    .table{
        font-family: "Exo2-Medium" ;
    }
    .name-product {
        font-family: "Exo2-Bold";
        color: rgb(229, 26, 146)!important;
        text-decoration: none;
    }
    .uni-opt{
        margin-top:2%;
    }
    .btn-options{
        color: #fff;
        background-color: #000;
        padding: 3px 8px;
    }
    .form-control{
        border-radius: 0!important;
    }
    .continue{
        padding-bottom: 8px!important;
    }

    .btn-mobile{
        width: 100%;
        border-radius: 0px !important;
        height: 35px!important;
        padding: 7px 12px !important;
    }
    .fl {
        float: left !important;
    }
    .fr {
        float: right !important;
    }
    .price-total-info-top p {
        margin: 0;
        padding: 8px;
        font-weight: bold;
        border-bottom: 1px solid #ddd;
    }

    .price-total-info-top {
        border-top: 2px solid #464646;
        border-bottom: 2px solid #464646;
        margin-bottom: 25px;
    }

    @media screen and (min-width: 501px) {
        .type_mobile{
            display:none!important;
        }
    }
    @media screen and (max-width: 500px) {
        .type_mobile {
            display: block!important;
        }
        .type_desktop{
            display: none!important;
        }
    }

</style>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>JUNG SAEM MOOL｜Cosmetics - Official Site</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
</head>
@php
    $locale = session()->get('locale');
    if($locale == 'th'){
        $a = 'text-thai';
    }else{
        $a = '';
    }
@endphp

@section('content')

    <div id="ju-container">
        <div id="ju-content" class="container type_desktop">
            <div class="ju-page-title">
                <h1 class="entry-title">
                    <font style="vertical-align: inherit;">
                        <font class="{{$a}}" style="vertical-align: inherit;">@lang('lang.page_cart')</font>
                    </font>
                </h1>
            </div>
            <div class="woocommerce">
                <div class="row step_point">
                    <div class="col-lg-offset-3 col-lg-2 col-xs-4">
                        <div class="text-center">
                        <span class="fa-stack fa-lg fa-3x active">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-shopping-cart fa-stack-1x"></i>
                        </span>
                            <p class="h5 letters">
                                <font style="vertical-align: inherit;">
                                    <font class="bolder exo2-bold {{$a}}" style="vertical-align: inherit;font-weight: bolder;">@lang('lang.page_cart_basket')</font>
                                </font>
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-2 col-xs-4">
                        <div class="text-center">
                        <span class="fa-stack fa-lg fa-3x">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-credit-card fa-stack-1x"></i>
                        </span>
                            <p class="h5 letters">
                                <font style="vertical-align: inherit;">
                                    <font class="bolder exo2-bold {{$a}}" style="vertical-align: inherit;font-weight: bolder;">@lang('lang.page_cart_payment')</font>
                                </font>
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-2 col-xs-4">
                        <div class="text-center">
                        <span class="fa-stack fa-lg fa-3x">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-check fa-stack-1x"></i>
                        </span>
                            <p class="h5 letters">
                                <font style="vertical-align: inherit;">
                                    <font class="bolder exo2-bold {{$a}}" style="vertical-align: inherit;font-weight: bolder;">@lang('lang.page_cart_order')</font>
                                </font>
                            </p>
                        </div>
                    </div>
                </div>
                <hr class="clear">
                <div id="cartWrap">
                    <div class="page-body">
                        <h3 class="letters">
                            <font style="vertical-align: inherit;">
                                <font class="exo2-bold bolder {{$a}}" style="vertical-align: inherit;">@lang('lang.page_cart_title')</font>
                            </font>
                        </h3>
                        <div class="table-responsive table-cart">
                            <table summary="" class="table">
                                <colgroup>
                                    <col width="70">
                                    <col width="*">
                                    <col width="70">
                                    <col width="70" style="width: 5%">
                                    <col width="90">
                                    <col width="90">
                                    <col width="90">
                                </colgroup>
                                <thead>
                                <tr>
                                    <th scope="col" class="displaynone">
                                        <div class="tb-center">
                                            <input type="checkbox" name="__allcheck" onclick="all_basket_check(this);" class="MS_input_checkbox" checked="">
                                        </div>
                                    </th>
                                    <th scope="col">
                                        <div class="tb-center">
                                            <font style="vertical-align: inherit;">
                                                <font class="bolder {{$a}}" style="vertical-align: inherit;">@lang('lang.page_cart_photo')</font>
                                            </font>
                                        </div>
                                    </th>
                                    <th scope="col">
                                        <div class="tb-center">
                                            <font style="vertical-align: inherit;">
                                                <font class="bolder {{$a}}" style="vertical-align: inherit;">@lang('lang.page_cart_product')</font>
                                            </font>
                                        </div>
                                    </th>
                                    <th scope="col">
                                        <div class="tb-center" style="width: 85px;">
                                            <font style="vertical-align: inherit;" >
                                                <font class="bolder {{$a}}" style="vertical-align: inherit;">@lang('lang.page_cart_unit')</font>
                                            </font>
                                        </div>
                                    </th>
                                    <th scope="col">
                                        <div class="tb-center">
                                            <font style="vertical-align: inherit;">
                                                <font class="bolder {{$a}}" style="vertical-align: inherit;">@lang('lang.page_cart_quantity')</font>
                                            </font>
                                        </div>
                                    </th>
                                    <th scope="col">
                                        <div class="tb-center" style="width: 105px;">
                                            <font style="vertical-align: inherit;">
                                                <font class="bolder {{$a}}" style="vertical-align: inherit;">@lang('lang.page_cart_price')</font>
                                            </font>
                                        </div>
                                    </th>
                                    <th scope="col" style="width: 310px;">
                                        <div class="tb-center">
                                            <font style="vertical-align: inherit;">
                                                <font class="bolder {{$a}}" style="vertical-align: inherit;">@lang('lang.page_cart_shipping')</font>
                                            </font>
                                        </div>
                                    </th>
                                    <th scope="col">
                                        <div class="tb-center">
                                            <font style="vertical-align: inherit;">
                                                <font class="bolder {{$a}}" style="vertical-align: inherit;">@lang('lang.page_cart_cancel')</font>
                                            </font>
                                        </div>
                                    </th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <td colspan="7">
                                        <div class="tb-right">
                                            <font style="vertical-align: inherit;">
                                                <font class="{{$a}}" style="vertical-align: inherit;">
                                                    @lang('lang.page_cart_amount')  (<span id="number_sum">0</span>  @lang('lang.page_cart_items')) =
                                                </font>
                                            </font>
                                            <strong>
                                                <font style="vertical-align: inherit;">
                                                    <font class="{{$a}}" style="vertical-align: inherit;"><span id="sumprice">0</span> @lang('lang.page_cart_baht')</font>
                                                </font>
                                            </strong>
                                        </div>

                                        <div class="tb-right">
                                            <font style="vertical-align: inherit;">
                                                <font class="{{$a}}" style="vertical-align: inherit;">
                                                    @lang('lang.page_cart_amount_ems') =
                                                </font>
                                            </font>
                                            <strong>
                                                <font style="vertical-align: inherit;">
                                                    <font class="{{$a}}" style="vertical-align: inherit;"><span id="ems_sum">0</span> @lang('lang.page_cart_baht')</font>
                                                </font>
                                            </strong>
                                        </div>

                                        <div class="tb-right">
                                            <font style="vertical-align: inherit;">
                                                <font class="{{$a}}" style="vertical-align: inherit;">
                                                    @lang('lang.page_cart_amount_sum') =
                                                </font>
                                            </font>
                                            <strong>
                                                <font style="vertical-align: inherit;">
                                                    <font class="{{$a}}" style="vertical-align: inherit;"><span id="total_sum">0</span> @lang('lang.page_cart_baht')</font>
                                                </font>
                                            </strong>
                                        </div>
                                    </td>
                                </tr>
                                </tfoot>
                                <tbody id="product-incard">
                                </tbody>
                            </table>
                        </div><!-- .table-fill-prd -->



                        <div class="clearfix">
                            <p class="pull-left">
                                <a href="#" class="btn btn-primary empty" >
                                    <i class="fa fa-trash"></i>
                                    <font style="vertical-align: inherit;">
                                        <font class="{{$a}}" style="vertical-align: inherit;"> @lang('lang.page_cart_empty')</font>
                                    </font>
                                </a>
                            </p>
                            <p class="pull-right">
                                <a href="/Product/Category/list/cid/0" class="btn btn-default continue">
                                    <font style="vertical-align: inherit;">
                                        <font class="{{$a}}" style="vertical-align: inherit;">@lang('lang.page_cart_continue')</font>
                                    </font>
                                </a>
                            </p>
                        </div>
                        <div class="wc-proceed-to-checkout">
                            <hr class="sm clear">
                        <!-- <a href="/order/checkout" class="btn btn-danger btn-block btn-lg alt wc-forward order">
                            <i class="fa fa-credit-card"></i>
                            <span class="letters">
                                <font style="vertical-align: inherit;">
                                    <font class="{{$a}}" style="vertical-align: inherit;">@lang('lang.page_cart_place')</font>
                                </font>
                            </span>
                        </a> -->
                            <form name="myForm" action="#" id="productForm" onsubmit="return validateForm()" method="get">
                                <input type="hidden" name="_method" value="PUT">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                {{ csrf_field() }}

                                <button type="button" class="btn btn-success btn-save btn btn-danger btn-block btn-lg alt wc-forward order">
                                    <i class="fa fa-credit-card"></i>
                                    <span class="letters">
                                    <font style="vertical-align: inherit;">
                                        <font class="{{$a}}" style="vertical-align: inherit;">@lang('lang.page_cart_place')</font>
                                    </font>
                                </span>
                                </button>
                            </form>

                        </div>
                        <div class="cart-ft2"></div>

                    </div><!-- .page-body -->
                </div><!-- #cartWrap -->
            </div><!-- .woocommerce -->
        </div><!-- #contentWrap -->




        <!-- /////Start Mobile ///// -->
        <div class="container type_mobile">
            <div class="row">
                <div class="col-xs-12">
                    <h3 class="letters" style="padding-bottom:8px; margin-bottom:0px; ">
                        <font style="vertical-align: inherit;">
                            <font class="exo2-bold bolder {{$a}}" style="vertical-align: inherit;">@lang('lang.page_cart_title')</font>
                        </font>
                    </h3>
                </div>

                <div class="col-xs-12" >
                    <div style="border-bottom: 2px solid #333; border-top: 2px solid #333;">
                      
                        <div id="product-incard_mobile">

                        </div>

                      
                        <div class="col-xs-12" style="margin: 10px 0 50px 0;" >
                            <div class="row">
                                <div class="col-xs-6">
                                    <a href="javascript:basket_multidel()" class="btn btn-default btn-mobile" style="height: 4.4%!important;">
                                        <span>@lang('lang.page_cart_continue')</span>
                                    </a>
                                </div>
                                <div class="col-xs-6">
                                    <a href="#" class="btn btn-primary btn-mobile empty {{$a}}" style="height: 4.4%!important;">
                            <span>
                                <i class="fa fa-trash"></i>
                                @lang('lang.page_cart_empty')
                            </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="price-total-info-top">

                                <p class="clearFix">
                                    <span class="fl">@lang('lang.page_cart_amount')</span>
                                    <span class="fr">

                                        <b>
                                            <span id="sumprice_mobile">0 </span> 
                                            <span>@lang('lang.page_cart_baht')</span>
                                        </b>
                                    </span>
                                </p>
                                <p class="clearFix">
                                    <span class="fl">@lang('lang.page_cart_amount_ems')</span>
                                    <span class="fr">
                                        <span id="ems_sum_mobile">0 </span> 
                                        <span>@lang('lang.page_cart_baht')</span>
                                    </span>
                                </p>
                                <p class="clearFix">
                                    <span class="fl">@lang('lang.page_cart_amount_sum')</span>
                                    <span class="fr">
                                        <span id="total_sum_mobile">
                                            <strong class="text-danger">
                                                <b>0 </b> 
                                            </strong>
                                        </span>
                                     <span>@lang('lang.page_cart_baht')</span>
                                    </span>
                                </p>
                            </div>

                            <div class="col-xs-12" style="margin-top:15px; padding-left: 0; padding-right: 0;">
                                <form name="myForm" action="#" id="productForm" onsubmit="return validateForm()" method="get">
                                    <input type="hidden" name="_method" value="PUT">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    {{ csrf_field() }}

                                    <button type="button" class="btn btn-success btn-save btn btn-danger btn-block btn-lg alt wc-forward order">
                                        <i class="fa fa-credit-card"></i>
                                        <span class="letters">
                                <font style="vertical-align: inherit;">
                                    <font class="{{$a}}" style="vertical-align: inherit;">สั่งซื้อสินค้า</font>
                                </font>
                            </span>
                                    </button>
                                </form>
                            </div>
                        </div>

                        <div style="border-bottom: 1px solid #eaeaea;"></div>
                    </div>
                </div>

            </div>
        </div>
        <!-- /////End Mobile ///// -->



    </div>


    <script src="https://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>

    <script>

        $('body').on('click', '.btn-save', function () {

            var save = $('#save').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                dataType: 'json',
                type:'POST',
                data:{save:save},
                url: '/savecart',
                success: function(datas){
                    console.log(datas.code_return);
                    if(datas.code_return === 501){
                        swal("Error!!", "จำนวนสินค้าไม่พอ", "error");
                    }
                    else if(datas.code_return === 1){
                        window.location.href = '/order/checkout/'+ datas.id_cart ;
                    }

                }
            })
        });

    </script>

@endsection
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.min.js"></script>
<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
<script>


</script>

@push('scripts')
    <script src="{{ mix('js/cart.js') }}"></script>
@endpush
