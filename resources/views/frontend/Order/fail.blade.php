@extends('layouts.template.frontend')

@section('content')
    <style>
        i.bigger-size {
            margin-top:50px;
            font-size: 200px;
            color: #BA0900;
            background-color: #fff;
        }
        .text-success{
            margin-bottom:50px;
        }
    </style>

    <div>
        <!-- Page Heading -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald|Roboto">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css">


        <div class="container">
            <div class="row">
                <div class="col-12 col-md-3 center">
                    <div class="top-container">
                        <i class="fa fa-close bigger-size" aria-hidden="true"></i>
                    </div>
                    <h3 class="text-success">Fail</h3>
                </div>
                <div class="col-12 col-md-9">
                    <h3 class="text-success" style="margin-bottom: 10px;">List Order Number</h3>
                    <table class="table" style="margin-top:30px;">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Order Number</th>
                                <th scope="col">Customer Name</th>
                                <th scope="col">Total Order</th>
                                <th scope="col">Total Shipping</th>
                                <th scope="col">Sum Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">1</th>
                                <td> {{$listOrder->cart_number}} </td>
                                <td> {{$listUser->fname.'  '.$listUser->lname}} </td>
                                <td> {{number_format($listOrder->totalOrder ,2,'.',',')}}</td>
                                <td> {{number_format($listOrder->totalShipping ,2,'.',',')}} </td>
                                <td> {{number_format($listOrder->sumPrice ,2,'.',',')}}  </td>
                            </tr>
                        </tbody>
                    </table>
                    <br/>
                    <h3 class="text-success" style="margin-bottom: 10px;">List Product </h3>
                    <table class="table">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col" width="80px">#</th>
                                <th scope="col">Product</th>
                                <th scope="col">Unit Price</th>
                                <th scope="col">Quantity</th>
                                <th scope="col">Price</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($listProduct as $product)
                            <tr>
                                <th scope="row">
                                    <img src="/public/product/{{ $product->cover_img }}" style="width:100%;" alt="Product thumbnail" title="Product thumbnail" >
                                </th>
                                <td>
                                    {{ $product->name_en }}<br/>
                                    Option :
                                    @foreach($listSku as $sku)
                                        @if($product->id_sku == $sku->sku )
                                            {{$sku->name_en}}
                                        @endif 
                                    @endforeach
                                </td>
                                <td>{{number_format($product->price ,2,'.',',')}}</td>
                                <td>{{ $product->qty }}</td>
                                <td>{{number_format($product->sumPrice ,2,'.',',')}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row justify-content-md-center" style="float: right; margin-right:0px!important;">
                <a href="/Product/Category/list/cid/0">
                    <button style="margin-bottom:40px; float:right;" type="button" class="btn btn-success">Shopping Continues</button>  
                </a>
            </div>
            
          
           
        </div>
    </div>
    
@endsection