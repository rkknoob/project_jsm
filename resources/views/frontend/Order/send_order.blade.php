@extends('layouts.template.frontend')
<style type="text/css">

    @font-face {
        font-family:'Exo2-Bold';
        src: url({!! asset("fonts/Exo2/Exo2-Bold.ttf") !!}) ;
    }
    @font-face {
        font-family:'Exo2-Medium';
        src: url({!! asset("fonts/Exo2/Exo2-Medium.ttf") !!}) ;
    }

    /* ภาษาไทย */
    @font-face {
        font-family: 'SukhumvitSet';
        src: url('/fonts/Sukhumvit/SukhumvitSet-Text.ttf');
    }
    .text-thai {
        font-family: 'SukhumvitSet';
        font-style: normal;
        letter-spacing: 0px;
    }

  
    #ju-container {
        padding: 50px 0!important;
    }
    .exo2-bold{
        font-family: 'Exo2-Bold';
    }
    .bolder{
        font-weight: bolder;
    }
    .link_del a {
        width: 37px;
        margin: 0px auto;
    }
    .remove, .modify, .empty, .continue, .order{
        border-radius: 0!important;
        font-family: "Exo2-Medium" ;
    }
    .table{
        font-family: "Exo2-Medium" ;
    }
    .name-product {
        font-family: "Exo2-Bold";
        color: rgb(229, 26, 146)!important;
        text-decoration: none;
    }
    .uni-opt{
        margin-top:2%;
    }
    .btn-options{
        color: #fff;
        background-color: #000;
        padding: 3px 8px;
    }
    .form-control{
        border-radius: 0!important; 
    }
    .continue{
        padding-bottom: 8px!important;
    }

    .table-order-info {
        border-top: 2px solid #333!important;
        border-bottom: 2px solid #333!important;
    }
    .MS_input_txt, .MS_input_tel, select {
        width: 80%!important;
    }
    input:invalid, textarea:invalid {
        background-color: transparent !important;
    }
    @media screen and (max-width: 500px) {
        .type_mobile {
            display: block!important;
        }
        .type_desktop{
            display: none!important;
        }
        .table_title{
            text-align: left!important;
            border: none!important;
        }
        .from_row {
            margin-right: 0px !important;
            margin-left: 0px !important;
        }
        .table_title{
            padding: 10px 8px !important;
        }
        .mobile {
            padding-right: 0px !important;
            padding-left: 0px !important;
        }
    }

    @media screen and (min-width: 501px) {
        .type_mobile{
            display:none!important;
        }
        .mobile{
            border-bottom: 1px solid #e9e9e9;
        }
        .table_title{
            text-align: left!important;
        }
    }


    .table_title{
        color: #000;
        border-right: 1px solid #e9e9e9;
        font-weight: normal;
        background: #fbfafa!important;
        letter-spacing: 0;
        vertical-align: middle;
        text-align: right;
        padding: 16.5px 20px;
    }
    .detail_input {
        padding: 8px 7px;
    }

    
    @media screen and (min-width: 501px) {
        .type_mobile{
            display:none!important;
        }
    }
    @media screen and (max-width: 500px) {
        .type_mobile {
            display: block!important;
        }
        .type_desktop{
            display: none!important;
        }
    }

    .fl {
        float: left !important;
    }
    .fr {
        float: right !important;
    }
    .price-total-info-top p {
        margin: 0;
        padding: 8px;
        font-weight: bold;
        border-bottom: 1px solid #ddd;
    }
    
    .price-total-info-top {
        border-top: 2px solid #464646;
        border-bottom: 2px solid #464646;
        margin-bottom: 25px;
    }

</style>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <title>JUNG SAEM MOOL｜Cosmetics - Official Site</title>
    <meta name="description" content="">
    <meta name="keywords" content="">

    
    <meta name="csrf-token" content="{{ csrf_token() }}">
   
</head>

@php
    $locale = session()->get('locale');
    if($locale == 'th'){
        $a = 'text-thai';
    }else{
        $a = '';
    }
@endphp

@section('content')




<div id="ju-container">
    <div id="ju-content" class="container">
        <div class="ju-page-title">
            <h1 class="entry-title">
                <font style="vertical-align: inherit;">
                    <font class="{{$a}}" style="vertical-align: inherit;">@lang('lang.page_checkout')</font>
                </font>
            </h1>
        </div>
        <div class="woocommerce">
            <div class="row step_point">
                <div class="col-lg-offset-3 col-lg-2 col-xs-4">
                    <div class="text-center">
                        <span class="fa-stack fa-lg fa-3x">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-shopping-cart fa-stack-1x"></i>
                        </span>
                        <p class="h5 letters">
                            <font style="vertical-align: inherit;">
                                <font class="bolder exo2-bold {{$a}}" style="vertical-align: inherit;font-weight: bolder;">@lang('lang.page_cart_basket')</font>
                            </font>
                        </p>
                    </div>
                </div>
                <div class="col-lg-2 col-xs-4">
                    <div class="text-center">
                        <span class="fa-stack fa-lg fa-3x active">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-credit-card fa-stack-1x"></i>
                        </span>
                        <p class="h5 letters">
                            <font style="vertical-align: inherit;">
                                <font class="bolder exo2-bold {{$a}}" style="vertical-align: inherit;font-weight: bolder;">@lang('lang.page_cart_payment')</font>
                            </font>
                        </p>
                    </div>
                </div>
                <div class="col-lg-2 col-xs-4">
                    <div class="text-center">
                        <span class="fa-stack fa-lg fa-3x">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-check fa-stack-1x"></i>
                        </span>
                        <p class="h5 letters">
                            <font style="vertical-align: inherit;">
                                <font class="bolder exo2-bold {{$a}}" style="vertical-align: inherit;font-weight: bolder;">@lang('lang.page_cart_order')</font>
                            </font>
                        </p>
                    </div>
                </div>
            </div>
            <hr class="clear">
            <div id="cartWrap">
                <div class="page-body">
                    <h3 class="letters">
                        <font style="vertical-align: inherit;">
                            <font class="exo2-bold bolder {{$a}}" style="vertical-align: inherit;">
                            @lang('lang.page_checkout_title')
                            </font>
                        </font>
                    </h3>


                    <div class="table-responsive table-cart type_desktop">
                        <table summary="" class="table">
                            <colgroup>
                                <col width="100">
                                <col width="800">
                                <col width="120">
                                <col width="120">
                                <col width="120">
                            </colgroup>
                            <thead>
                                <tr>
                                    <!-- <th scope="col" class="displaynone">
                                        <div class="tb-center">
                                            <input type="checkbox" name="__allcheck" onclick="all_basket_check(this);" class="MS_input_checkbox" checked="">
                                        </div>
                                    </th> -->
                                    <!-- <th scope="col">
                                        <div class="tb-center">
                                            <font style="vertical-align: inherit;">
                                                <font class="bolder {{$a}}" style="vertical-align: inherit;"></font>
                                            </font>
                                        </div>
                                    </th> -->
                                    <th scope="col" colspan="2">
                                        <div class="tb-center">
                                            <font style="vertical-align: inherit;">
                                                <font class="bolder {{$a}}" style="vertical-align: inherit;">@lang('lang.page_checkout_purchase')</font>
                                            </font>
                                        </div>
                                    </th>
                                    <th scope="col">
                                        <div class="tb-center">
                                            <font style="vertical-align: inherit;">
                                                <font class="bolder {{$a}}" style="vertical-align: inherit;">@lang('lang.page_checkout_unit')</font>
                                            </font>
                                        </div>
                                    </th>
                                    <th scope="col">
                                        <div class="tb-center" style="width: 105px;">
                                            <font style="vertical-align: inherit;">
                                                <font class="bolder {{$a}}" style="vertical-align: inherit;">@lang('lang.page_checkout_quantity')</font>
                                            </font>
                                        </div>
                                    </th>
                                    <th scope="col">
                                        <div class="tb-center">
                                            <font style="vertical-align: inherit;">
                                                <font class="bolder {{$a}}" style="vertical-align: inherit;">@lang('lang.page_checkout_price') </font>
                                            </font>
                                        </div>
                                    </th>
                                
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <td colspan="7">
                                        <div class="tb-right">
                                            <font style="vertical-align: inherit;">
                                                <font class="{{$a}}" style="vertical-align: inherit;">
                                                    @lang('lang.page_cart_amount')  (<span id="number_sum">{{$item}}</span>  @lang('lang.page_cart_items')) = 
                                                </font>
                                            </font>
                                            <strong>
                                                <font style="vertical-align: inherit;">
                                                    <font class="{{$a}}" style="vertical-align: inherit;"> {{number_format($data->totalOrder,2,'.',',')}} @lang('lang.page_cart_baht')</font>
                                                </font>
                                            </strong>
                                        </div>

                                        <div class="tb-right">
                                            <font style="vertical-align: inherit;">
                                                <font class="{{$a}}" style="vertical-align: inherit;">
                                                    @lang('lang.page_cart_amount_ems') = {{number_format($data->totalShipping,2,'.',',')}}
                                                </font>
                                            </font>
                                            <strong>
                                                <font style="vertical-align: inherit;">
                                                    <font class="{{$a}}" style="vertical-align: inherit;">@lang('lang.page_cart_baht')</font>
                                                </font>
                                            </strong>
                                        </div>

                                        <div class="tb-right">
                                            <font style="vertical-align: inherit;">
                                                <font class="{{$a}}" style="vertical-align: inherit;">
                                                    @lang('lang.page_cart_amount_sum') = 
                                                </font>
                                            </font>
                                            <strong>
                                                <font style="vertical-align: inherit;">
                                                    <font class="{{$a}}" style="vertical-align: inherit;">{{number_format($data->sumPrice,2,'.',',')}} @lang('lang.page_cart_baht')</font>
                                                </font>
                                            </strong>
                                        </div>
                                        
                                    </td>
                                </tr>
                            </tfoot>
                            <tbody id="product-incard">
                                @foreach($query as $item)
                                <tr>
                                    <td>
                                        <div class="tb-center">
                                            <div class="thumb">
                                                <a href="#">
                                                    <img src="/public/product/{{ $item->cover_img }}" alt="Product thumbnail" title="Product thumbnail">
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="tb-left">
                                            <h5 class="media-heading">
                                                <a href="#">
                                                    <font class="name-product" style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">{{ $item->productname }} </font>
                                                    </font>
                                                </a>
                                            </h5>
                                            <span class="MK-product-icons"></span>
                                            <div id="2191468_1" class="tb-left tb-opt">
                                                <strong>
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;"></font>
                                                    </font>
                                                </strong>
                                                <font style="vertical-align: inherit;">
                                                    <font style="vertical-align: inherit;"> {{ $item->name_en }}</font>
                                                </font>
                                            </div>
                                            <div class="uni-opt">
                                                <a href="javascript:modify_option('2191468', '1','');" class="modify">
                                                    <font style="vertical-align: inherit;">
                                                    </font>
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="tb-center">
                                            <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">฿ {{number_format($item->price,2,'.',',')}}</font>
                                            </font>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="tb-center">
                                            <div class="num_c">
                                                <font style="vertical-align: inherit;">{{ $item->qty }}</font>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="tb-center tb-bold">
                                            <span>
                                                <font style="vertical-align: inherit;">
                                                    <font class="{{$a}}" style="vertical-align: inherit;">฿ {{number_format($item->sumPrice,2,'.',',')}}</font>
                                                </font>
                                            </span>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div><!-- .table-fill-prd -->
                    <div class="cart-ft2"></div>


                    <!-- /////Start Mobile ///// -->
                    <div class="container-fluid type_mobile">
                        <div class="row">
                            <div class="col-xs-12" style="padding: 0" >
                                <div style="border-top: 2px solid #333;">
                                    @foreach($query as $item)
                                    <div class="row" style="margin: 10px 0px;">
                                        <div class="col-xs-5">
                                            <div class="basketLeft">
                                                <a href="#">
                                                    <img src="/public/product/{{ $item->cover_img }}" alt="상품명" class="response100">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-xs-7">
                                            <figcaption class="basketRight">
                                                <p class="pname clearFix">
                                                    <font class="name-product" style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">{{ $item->productname }}</font>
                                                    </font>
                                                </p>
                                                <p class="h5 bold" style="font-size: 13px!important; margin: 5px 0 10px; font-weight: inherit;">
                                                    ฿ {{number_format($item->price,2,'.',',')}} (@lang('lang.page_cart_unit'))
                                                </p>        
                                                <p>
                                                    <span class="num_c">
                                                        <font style="vertical-align: inherit;">{{ $item->qty }} @lang('lang.page_checkout_pieces')</font> 
                                                    </span>
                                              
                                                </p>
                                                <p style="font-size: 12px; margin-bottom: 5px;">
                                                    <strong><b>[Option]</b></strong>
                                                    : {{ $item->name_en }}
                                                </p>
                                            </figcaption>
                                        </div>
                                    </div>
                                    <div style="border-bottom: 1px solid #eaeaea;"></div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-xs-12" style="padding:0px;">
                                <div class="price-total-info-top">
                                    <p class="clearFix">
                                        <span class="fl">@lang('lang.page_cart_amount')</span>
                                        <span class="fr">
                                        
                                            <b>{{number_format($data->totalOrder,2,'.',',')}} @lang('lang.page_cart_baht')</b>                                                                                                              
                                        </span>
                                    </p>
                                    <p class="clearFix">
                                        <span class="fl">@lang('lang.page_cart_amount_ems')</span>
                                        <span class="fr">{{number_format($data->totalShipping,2,'.',',')}} @lang('lang.page_cart_baht')</span>
                                    </p>
                                    <p class="clearFix">
                                        <span class="fl">@lang('lang.page_cart_amount_sum')</span>
                                        <span class="fr">
                                            <strong class="text-danger"><b>{{number_format($data->sumPrice,2,'.',',')}} @lang('lang.page_cart_baht')</b></strong>
                                        </span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /////End Mobile ///// -->


                </div><!-- .page-body -->
            </div><!-- #cartWrap -->

            <div id="cartWrap">
                <div class="page-body">
                    <h3 class="letters">
                        <font style="vertical-align: inherit;">
                            <font class="exo2-bold bolder {{$a}}" style="vertical-align: inherit;">@lang('lang.page_checkout_order')</font>
                        </font>
                    </h3>
                </div>

                <form name="myForm" id="productForm" onsubmit="return validateForm()" action="/payment/checkout/{{$id_order}}" method="post">
                   {{ csrf_field() }}
                    <div class="row from_row" style="border-top: 2px solid #333!important;">
                        
                        <div class="col-md-2 col-4 mobile">
                            <div class="table_title {{$a}}">
                                <b>@lang('lang.page_checkout_mail')</b><font color="red"> *</font>
                            </div>
                            
                        </div>
                        <div class="col-md-4 col-8 mobile">
                            <div class="detail_input">
                                <input type="text" value="" name="email" id="email" class="MS_input_txt txt-input2 width150" maxlength="65" required>
                                
                            </div>
                        </div>
                        <div class="col-md-2 col-4 mobile">
                            <div class="table_title {{$a}}">
                                <b>@lang('lang.page_checkout_phone')  </b><font color="red"> *</font>
                            </div>
                        </div>
                        <div class="col-md-4 col-8 mobile">
                            <div class="detail_input">
                                <input type="text" value="" name="phone" id="phone"  class="MS_input_txt txt-input2 width150" maxlength="65" required>  
                            </div>
                        </div>
                    </div>
                    <div class="row from_row" >
                        <div class="col-md-2 col-4 mobile">    
                            <div class="table_title {{$a}}">
                                <b>@lang('lang.page_checkout_firstname')</b>
                            </div>
                        </div>
                        <div class="col-md-4 col-8 mobile">
                            <div class="detail_input">
                                <input type="text" value="" name="fname" id="fname" class="MS_input_txt txt-input2 width150" maxlength="16" required>
                            </div>
                        </div>
                        <div class="col-md-2 col-4 mobile">  
                            <div class="table_title {{$a}}">
                                <b>@lang('lang.page_checkout_lastname') </b>  
                            </div>
                        </div>
                        <div class="col-md-4 col-8 mobile">
                            <div class="detail_input">
                                <input type="text" value="" name="lname" id="lname" class="MS_input_txt txt-input2 width150" maxlength="16" required>
                            </div>
                        </div>
                    </div>
                    <div class="row from_row" >
                        <div class="col-md-2 col-4 mobile">   
                            <div class="table_title {{$a}}">
                                <b>@lang('lang.page_checkout_address') </b> 
                            </div>
                        </div>
                        <div class="col-md-4 col-8 mobile">
                            <div class="detail_input">
                                <input type="text" value="" name="address" id="address" class="MS_input_txt txt-input2 width150" maxlength="100" required>
                            </div>
                        </div>
                        <div class="col-md-2 col-4 mobile">   
                            <div class="table_title {{$a}}">
                                <b>@lang('lang.page_checkout_province') </b> 
                            </div>
                        </div>
                        <div class="col-md-4 col-8 mobile">
                            <div class="detail_input">
                                <select id="input_province" onchange="showAmphoes()" class="MS_input_txt txt-input2" required>
                                    <option value="">กรุณาเลือกจังหวัด</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row from_row" >
                        <div class="col-md-2 col-4 mobile"> 
                            <div class="table_title {{$a}} ">
                                <b>@lang('lang.page_checkout_district') </b>   
                            </div>
                        </div>
                        <div class="col-md-4 col-8 mobile">
                            <div class="detail_input">
                                <select id="input_amphoe" onchange="showDistricts()" class="MS_input_txt txt-input2" required>
                                    <option value="">กรุณาเลือกอำเภอ</option> 
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2 col-4 mobile">    
                            <div class="table_title {{$a}}">
                                <b>@lang('lang.page_checkout_subdistrict') </b>
                            </div>
                        </div>
                        <div class="col-md-4 col-8 mobile">
                            <div class="detail_input">
                                <select id="input_district" onchange="showZipcode()" class="MS_input_txt txt-input2" required>
                                    <option value="">กรุณาเลือกตำบล</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row from_row"  style="border-bottom: 2px solid #333!important;">
                        <div class="col-md-2 col-4 mobile"> 
                            <div class="table_title {{$a}}">
                                <b>@lang('lang.page_checkout_code')</b> 
                            </div>  
                        </div>
                        <div class="col-md-4 col-8 mobile">
                            <div class="detail_input ">
                                <input id="input_zipcode"  placeholder="รหัสไปรษณีย์" class="MS_input_txt txt-input2" required/>
                                <input id="input_id" hidden name="input_id"  placeholder="รหัสไปรษณีย์" class="MS_input_txt txt-input2" />
                                <input type="hidden" name="id_order" id="id_order" value="{{$id_order}}" />  
                            </div>
                        </div> 

                    </div>
                
                    <div class="wc-proceed-to-checkout" style="margin-top:25px;">
                        <hr class="sm clear">
                        <button  type="submit" name="submit" value="Confirm" class="btn btn-danger btn-block btn-lg alt wc-forward order btn-save">
                            <i class="fa fa-credit-card"></i>
                                CheckOut
                            </span>
                        </button>
                    </div>
                </form>
            </div>
        </div><!-- .woocommerce -->
    </div><!-- #contentWrap -->
</div>

<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> -->

<!-- <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script> -->
<!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script> -->

<script>
    if (!window.moment) {
        document.write('<script src="assets/plugins/moment/moment.min.js"><\/script>');
    }
</script>

<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

<script>
   
    $('body').on('click', '.btn-save', function () {

        //Customer
        var order_id = $('#id_order').val();
        var fname = $('#fname').val();
        var lname = $('#lname').val();
        var email = $('#email').val();
        var phone = $('#phone').val();
        var address = $('#address').val();
        var input_id = $('#input_id').val();
        var id_order = $('#id_order').val();
        

        // alert(email);
        // alert(fname);
        // alert(lname);

        if((fname == '') || (lname == '') || (email == '') || (phone == '') || (address == '') || (input_id == '')  ){
            return swal("เกิดข้อผิดพลาด!", "กรุณากรอกข้อมูลให้ครบ", "error");
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            dataType: 'json',
            type:'POST',
            data:{order_id:order_id,email:email, fname:fname, lname:lname,phone:phone,address:address,input_id:input_id,order_id:id_order},
            url: '/order/checkout',
            success: function(datas){
                //swal("Good job!", "จ่ายตังสำเร็จ", "success");
                //window.location.href = '/payment/success'
            }
        })
    });

    $(document).ready(function(){
        console.log("HELLO");
        showProvinces();
    });

    function ajax(url, callback){
        // alert('ajax');
        $.ajax({  
            "url" : url,
            "type" : "GET",
            "dataType" : "json",
        })
        .done(callback); //END AJAX 
    }


    function showProvinces(){
        //PARAMETERS
        var url = "{{ url('/') }}/api/province";
        var callback = function(result){
            $("#input_province").empty();
            for(var i=0; i<result.length; i++){
            $("#input_province").append(
                $('<option></option>')
                .attr("value", ""+result[i].province_code)
                .html(""+result[i].province)
            );
            }
            showAmphoes();
        };
        //CALL AJAX
        ajax(url,callback);
    }

    function showAmphoes(){
        //INPUT
        var province_code = $("#input_province").val();
        //PARAMETERS
        var url = "{{ url('/') }}/api/province/"+province_code+"/amphoe";
        var callback = function(result){
            //console.log(result);
            $("#input_amphoe").empty();
            for(var i=0; i<result.length; i++){
            $("#input_amphoe").append(
                $('<option></option>')
                .attr("value", ""+result[i].amphoe_code)
                .html(""+result[i].amphoe)
            );
            }
            showDistricts();
        };
        //CALL AJAX
        ajax(url,callback);
    }

    function showDistricts(){
        //INPUT
        var province_code = $("#input_province").val();
        var amphoe_code = $("#input_amphoe").val();
        //PARAMETERS
        var url = "{{ url('/') }}/api/province/"+province_code+"/amphoe/"+amphoe_code+"/district";
        var callback = function(result){
            //console.log(result);
            $("#input_district").empty();
            for(var i=0; i<result.length; i++){
            $("#input_district").append(
                $('<option></option>')
                .attr("value", ""+result[i].district_code)
                .html(""+result[i].district)
            );
            }
            showZipcode();
        };
        //CALL AJAX
        ajax(url,callback);
    }

    function showZipcode(){
        //INPUT
        var province_code = $("#input_province").val();
        var amphoe_code = $("#input_amphoe").val();
        var district_code = $("#input_district").val();
        //PARAMETERS
        var url = "{{ url('/') }}/api/province/"+province_code+"/amphoe/"+amphoe_code+"/district/"+district_code;
        var callback = function(result){
            //console.log(result);
            for(var i=0; i<result.length; i++){
                $("#input_zipcode").val(result[i].zipcode);
                $("#input_id").val(result[i].id);
            }
        };
        //CALL AJAX
        ajax(url,callback);
    }
</script>
@endsection

<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.min.js"></script>
<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>


</script>



