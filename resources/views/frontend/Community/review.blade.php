@extends('layouts.template.frontend')

<style type="text/css">
#ju-container .ju-page-title {
    margin-top: 179px;
}
#ju-container .ju-page-title {
    margin-top: 73px!important;
    margin-bottom: 40px!important;
}

.review{
    font-family: "Exo2Regular" !important;
}

.bbs-table-list thead th {
    color: #9d9d9d;
    font-weight: bold;
    font-size: 11px;
    border-top: 1px solid #e5e5e5;
    border-bottom: 1px solid #e5e5e5;
}

.bbs-table-list tbody td {
    padding: 6px 0;
}

.distance{
    padding-top: 16px!important;
}

.review {
    /* color: #777 !important; */
    position: relative;
    padding: 20px 15px;
    border-bottom: 1px solid #dadada;
    margin-right: 0px!important;
    margin-left: 0px !important;
}

.icon {
    font-size: 20px;
    color: #939393;
    letter-spacing: -5px;
}

.img-product{
    border: 1px solid #c9c9c9;
}
.img-icon{
    width: 24px;
    margin-right: 3px;
    vertical-align: top;
}
.survey {
    color: #515368;
    font-weight: bold;
    letter-spacing: -1px;
    width: 80px;
    margin-left: 5px;
}
.product-name{
    color: #444;
}
.ul{
    list-style-type: none!important;
}
.yes,.no {
    display: inline-block;
    width: 57px;
    height: 22px;
    line-height: 24px;
    color: #000;
    border: 1px solid #ccc;
    text-align: center;
}
a{
    text-decoration:none;
}

normal {
    font-weight: normal!important;
}



/* Horizontal 6/7/8 Plus*/
@media screen and (max-width: 736px) {
    #ju-container .ju-page-title {
        margin-top: 10px !important;
        text-align: center;
        margin-bottom: 40px;
    }
    .cont-mobile{
        margin-top: 0px;
    }
    .review {
        padding: 10px 0px !important;
        border: 1px solid #dadada;
        margin-right: 0px!important;
        margin-left: 0px !important;
        margin-bottom: 10px;
    }
    .img-product{
        width: 80px !important;
        height: 80px !important;
    }
    .mpd-5{
        padding-left:0px !important;
        padding-right:0px !important;
    }
    .mpd-1{
        padding-right: 2px !important;
        padding-left: 2px !important;
        margin-bottom:50px !important;
    }
    .m-none{
        display:none;
    }
    .m-show{
        display: block !important;
    }
    .icon {
        font-size: 14px;
        letter-spacing: -3px;
    }
}

</style>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> JSM ARTIST </title>
    <meta name="description" content="">
    <meta name="keywords" content="">
</head>
@php
$locale = session()->get('locale');
@endphp
@section('content')

<div id="ju-container">
    <div id="ju-content" class="container mpd-1" style="margin-bottom:100px">
        <div class="ju-page-title">
            @if($locale==='th')
            <h1 class="entry-title text-thai text-center">{{$subject->subject_th}}</h1>
            <div class="text-center text-thai" hidden>{{$subject->summary_th}}</div>
            @else
            <h1 class="entry-title text-gotham text-center">{{$subject->subject_en}}</h1>
            <div class="text-center text-gotham-detail" hidden>{{$subject->summary_en}}</div>
            @endif

        </div>

        <div class="search-wrap well" hidden>
            <div class="item-search input-group col-sm-6 col-sm-offset-3">
                <label class="displaynone">
                    <select class="MS_input_select select-category custom-blue" id="search-category">
                        <option value="">전체검색</option>
                        <option value="1">배송관련</option>
                        <option value="2">주문,결제</option>
                        <option value="3">취소,환불,교환</option>
                        <option value="4">쿠폰,포인트</option>
                    </select>
                </label>
                <span class="input-group-addon letters" style="border-right:0;"><i class="fa fa-search"></i></span>
                <input id="faqSearch" class="MS_input_txt form-control" onkeypress="" type="text" value=""> <span class="input-group-btn"><a href="javascript:faqSearch('keyword')" class="btn btn-default letters">SEARCH</a></span>
            </div>
        </div>

        @foreach ($review as $reviews)
        <div class="row review">
            <div class="col-xs-3 col-sm-2 col-md-2 mpd-5">
                <div class="tb-center">
                    <a href="/product/detail/view/{{$reviews['product_id']}}">
                        <img src="/public/product/{{$reviews['product']['cover_img']}}" height="106"
                        width="106" class="img-fluid rounded mx-auto img-product">
                    </a>
                </div>
            </div>
            <div class="col-xs-9 col-sm-7 col-md-8">
                <div class="cont cont-mobile">
                    <div class="star-icon">
                        <span class="product-name m-none">{{$reviews['product']['name_en']}}</span>
                        <span class="icon" hidden>
                            <img src="/jsmbeauty/src/Icon/ico_new_h38.png" alt="new" class="img-icon">
                            <img src="/jsmbeauty/src/Icon/ico_mobile_h38.png" alt="mobile" class="img-icon">
                            <img src="/jsmbeauty/src/Icon/ico_camera_h38.png" alt="mobile" class="img-icon">
                            <img src="/jsmbeauty/src/Icon/ico_pc_h38.png" alt="mobile" class="img-icon">
                        </span>
                    </div>

                    <div class="star-icon">
                        @for($i = 0; $i < $reviews['score']; $i++)
                            <span class="icon">★</span>
                        @endfor
                        <span class="survey">
                        @if($locale==='th')
                            @if($reviews['score']==5)
                                ดีเยี่ยม
                            @elseif($reviews['score']==4)
                                ดีมาก
                            @elseif($reviews['score']==3)
                                พอใช้
                            @elseif($reviews['score']==2)
                                ปรับปรุง
                            @elseif($reviews['score']==1)
                                ไม่พอใจ
                            @endif
                        @else
                            @if($reviews['score']==5)
                                Excellence
                            @elseif($reviews['score']==4)
                                Good
                            @elseif($reviews['score']==3)
                                Fair
                            @elseif($reviews['score']==2)
                                Improvement
                            @elseif($reviews['score']==1)
                                Poor
                            @endif

                        @endif
                        </span>
                    </div>
                    <div class="normal" style="font-weight: normal">
                            {{$reviews['content']}}
                        <div class="m-show" style="display:none;">
                            {{ substr($reviews['created_at'],0,10) }}  {{$reviews['user_name']}}
                        </div>
                    </div>

                    <br/>
                    <div class="reply" hidden>
                        <strong class="cnt">0</strong>
                        @if($locale==='th')
                            ความคิดเห็น
                        @else
                            Comment
                        @endif
                        <span>
                        @if($locale==='th')
                            มีประโยชน์หรือไม่?
                        @else
                            Is it useful?
                        @endif
                        </span>

                        <a class="yes" href="#">
                            <img src="/jsmbeauty/src/Icon/001.png" style="width: 20px; margin-top: -4px;">
                            <span>0</span>
                        </a>

                        <a class="no" href="#">
                            <img src="/jsmbeauty/src/Icon/002.png" style="width: 20px; margin-top: -6px;">
                            <span>0</span>
                        </a>

                    </div>


                </div>

            </div>
            <div class="col-12 col-sm-3 col-md-2 m-none">
                <ul class="desc" style="list-style-type: none!important; padding-left:0px;">
                    <li>
                        @if($locale==='th')
                            ผู้ใช้งาน :
                        @else
                            User :
                        @endif

                        {{$reviews['user_name']}}
                    </li>
                    <li>
                        @if($locale==='th')
                            วันที่ :
                        @else
                            Date :
                        @endif
                        {{ substr($reviews['created_at'],0,10) }}
                    </li>
                    <li>
                        @if($locale==='th')
                            ยอดคนดู :
                        @else
                            Views :
                        @endif
                       <span id="power_review_showhits"> {{$reviews['hit']}} </span>
                    </li>
                </ul>
            </div>
        </div>

        @endforeach

        <!-- main -->
        <main id="review-board-list">
            <div style="padding-bottom: 3%;">
                <!-- <div class="text-center">
                    <ul class="pagination">

                    </ul>
                </div> -->
            </div>

        </main>
    </div>
</div>

@endsection
