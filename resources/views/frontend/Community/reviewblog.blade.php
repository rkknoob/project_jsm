@extends('layouts.template.frontend')

<style type="text/css">
#ju-container .ju-page-title {
    margin-top: 179px;
}

.bbs-table-list thead th {
    /* color: #9d9d9d;
    font-weight: bold;
    font-size: 11px;
    border-top: 1px solid #e5e5e5;
    border-bottom: 1px solid #e5e5e5; */
}

.bbs-table-list tbody td {
    padding: 6px 0;
}

.text-review {
    padding-top: 30px;

}

.text-product-name {
    /* border-top: dotted;
    border-bottom: dotted;
    border-width: 1px 1px;*/
    text-align: center;
    border-top: 1px solid #e5e5e5;
    border-bottom: 1px solid #e5e5e5;
}

.text-product-detail {
    border-bottom: 1px solid #e5e5e5;
}

.bord {
    border-top: dotted;
    border-bottom: dotted;
    border-width: 1px 1px;
}

.text-hit {
    text-align: right;
}

.review {
    /* color: #777 !important; */
    position: relative;
    padding: 20px 15px;
    border-bottom: 1px solid #dadada;
    margin-right: 0px!important;
    margin-left: 0px !important;
}

.icon {
    font-size: 20px;
    color: #939393;
    letter-spacing: -5px;
}
.img-product{   
    border: 1px solid #c9c9c9;
}
.img-icon{
    width: 24px;
    margin-right: 3px;
    vertical-align: top;
}
.survey {
    color: #515368;
    font-weight: bold;
    letter-spacing: -1px;
    width: 80px;
    margin-left: 5px;
}
.product-name{
    color: #444;
}
.ul{
    list-style-type: none!important;
}
.yes,.no {
    display: inline-block;
    width: 57px;
    height: 22px;
    line-height: 24px;
    color: #000;
    border: 1px solid #ccc;
    text-align: center;
}
a{
    text-decoration:none;
}


.review:hover {
    /* color: #e51a94 !important; */
}

.distance {
    padding-top: 16px !important;
}

@media(max-width:767px) {
    .text-hit {
        text-align: left;
    }


}

/* Horizontal 6/7/8 Plus*/
@media screen and (max-width: 736px) {

    #ju-container .ju-page-title {
        margin-top: 10px !important;
        text-align: center;
        margin-bottom: 40px;
    }
    .cont-mobile{
        margin-top: 0px;
    }
    .review {
        padding: 10px 0px !important;
        border: 1px solid #dadada;
        margin-right: 0px!important;
        margin-left: 0px !important;
        margin-bottom: 10px;
    }
    .img-product{
        width: 80px !important;
        height: 80px !important;
    }
    .mpd-5{
        padding-left:0px !important;
        padding-right:0px !important;
    }
    .mpd-1{
        padding-right: 2px !important;
        padding-left: 2px !important;
        margin-bottom:50px !important;
    }
    .m-none{
        display:none;
    }
    .m-show{
        display: block !important;
    }
    .icon {
        font-size: 14px;
        letter-spacing: -3px;
    }
}
</style>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> JSM ARTIST </title>
    <meta name="description" content="">
    <meta name="keywords" content="">
</head>
@php
$locale = session()->get('locale');
@endphp
@section('content')

<div id="ju-container">
    <div id="ju-content" class="container">g
        <div class="ju-page-title">
            @if($locale==='th')
            <h1 class="entry-title text-thai text-center">{{$subject->subject_th}}</h1>
            <div class="text-center text-thai" hidden>{{$subject->summary_th}}</div>
            @else
            <h1 class="entry-title text-gotham text-center">{{$subject->subject_en}}</h1>
            <div class="text-center text-gotham" hidden>{{$subject->summary_en}}</div>
            @endif

        </div>
        <!-- main -->
        <main id="review-board-list">

            <div class="row" style="border-style: groove ;border-width: 5px;border: 3px solid #e0e0e0;">
                <div class="col-md-3 col-sm-12">
                    <div>
                        <div style="border-right: groove">
                            <div class="tb-center">
                                <img src="/public/product/{{$product_details->cover_img}}" height="100" width="100"
                                    class="img-fluid rounded mx-auto">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-9 col-sm-12 text-review">
                    <div class="row">
                        <div class="col-md-9 col-sm-12">
                            Item name : {{$product_details->name_en}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9 col-sm-12">
                            Item price : <b>{{$product_details->price}} B</b>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row text-product-name">
                <div class="col-md-12 col-sm-12">
                    <b>{{$revi->title}}</b>
                </div>
            </div>
            <div class="row text-product-detail">
                <div class="col-md-2 col-sm-12">
                    Date:
                    {{ substr($revi->created_at,0,10) }}
                </div>
                <div class="col-md-2 col-sm-12">
                    Score:
                    @for ($i = 0; $i < $revi->score; $i++)
                        <span class="icon">★</span>
                        @endfor
                </div>

                <div class="col-md-8 col-sm-12 text-hit">
                    Hits: {{$revi->hit}}
                </div>
            </div>

            <div class="row text-product-detail">
                <div class="form-group col-md-12">
                    <!-- <div class="tabs-wrap"> -->
                    <div>
                        <br />
                        {!! $revi->content !!}
                        <br />
                    </div>
                    <!-- </div> -->
                </div>
            </div>
            <br />
            <div style="padding-bottom: 3%;">
                <!-- <table>
                    <tr>
                        <th scope="col">No.</th>
                        <th scope="col"></th>
                    </tr>
                    <tr>
                        <td>January</td>
                    </tr>
                </table> -->
                <!-- <div class="bbs-table-list">
                    <table style="width: 100%;">
                        <colgroup>
                            <col width="50px">
                            <col width="130px">
                            <col width="*">
                            <col width="110px">
                            <col width="110px">
                            <col width="50px">
                        </colgroup>
                        <thead>
                            <tr>
                                <th scope="col">
                                    <div class="tb-center">No.</div>
                                </th>
                                <th scope="col">
                                    <div class="tb-center">Product</div>
                                </th>
                                <th scope="col">
                                    <div class="tb-center">Title</div>
                                </th>
                                <th scope="col">
                                    <div class="tb-center">Score</div>
                                </th>
                                <th scope="col">
                                    <div class="tb-center">Date</div>
                                </th>
                                <th scope="col">
                                    <div class="tb-center">Hits</div>
                                </th>
                            </tr>
                        </thead>
                        <tbody>-->
                            @foreach ($reviews as $reviewss)
                            <div class="row review">
                                <div class="col-xs-3 col-sm-2 col-md-2 mpd-5">
                                    <div class="tb-center">
                                        <a href="/product/detail/view/{{$reviewss['product_id']}}">
                                            <img src="/public/product/{{$reviewss['product']['cover_img']}}" height="106"
                                            width="106" class="img-fluid rounded mx-auto img-product">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xs-9 col-sm-7 col-md-8">
                                    <div class="cont cont-mobile">
                                        <div class="star-icon">
                                            <span class="product-name m-none">{{$reviewss['product']['name_en']}}</span>
                                            <span class="icon" hidden>
                                                <img src="/jsmbeauty/src/Icon/ico_new_h38.png" alt="new" class="img-icon">                                                                                                            
                                                <img src="/jsmbeauty/src/Icon/ico_mobile_h38.png" alt="mobile" class="img-icon">
                                                <img src="/jsmbeauty/src/Icon/ico_camera_h38.png" alt="mobile" class="img-icon">
                                                <img src="/jsmbeauty/src/Icon/ico_pc_h38.png" alt="mobile" class="img-icon">
                                            </span>
                                        </div>

                                        <div class="star-icon">
                                            @for($i = 0; $i < $reviewss['score']; $i++) 
                                                <span class="icon">★</span>
                                            @endfor
                                            <span class="survey">
                                            @if($locale==='th')
                                                @if($reviewss['score']==5)
                                                    ดีเยี่ยม
                                                @elseif($reviewss['score']==4)
                                                    ดีมาก
                                                @elseif($reviewss['score']==3)
                                                    พอใช้
                                                @elseif($reviewss['score']==2)
                                                    ปรับปรุง
                                                @elseif($reviewss['score']==1)
                                                    ไม่พอใจ
                                                @endif
                                            @else
                                                @if($reviewss['score']==5)
                                                    Excellence
                                                @elseif($reviewss['score']==4)
                                                    Good
                                                @elseif($reviewss['score']==3)
                                                    Fair
                                                @elseif($reviewss['score']==2)
                                                    Improvement
                                                @elseif($reviewss['score']==1)
                                                    Poor
                                                @endif

                                            @endif
                                            </span>
                                        </div>
                                        <div class="content" >
                                            <p class="content_p">
                                                {!!$reviewss['content']!!}
                                            </p>
                                            <div class="m-show" style="display:none;">
                                                {{ substr($reviewss['created_at'],0,10) }}  {{$reviewss['user_name']}} 
                                            </div>
                                        </div>

                                        <br/>
                                        <div class="reply" hidden>
                                            <strong class="cnt">0</strong> 
                                            @if($locale==='th')
                                                ความคิดเห็น
                                            @else
                                                Comment
                                            @endif
                                            <span>
                                            @if($locale==='th')
                                                มีประโยชน์หรือไม่?
                                            @else
                                                Is it useful?
                                            @endif
                                            </span>
                                            
                                            <a class="yes" href="#">
                                                <img src="/jsmbeauty/src/Icon/001.png" style="width: 20px; margin-top: -4px;">
                                                <span>0</span>
                                            </a>

                                            <a class="no" href="#">
                                                <img src="/jsmbeauty/src/Icon/002.png" style="width: 20px; margin-top: -6px;">
                                                <span>0</span>
                                            </a>     
                                        </div>
                                    </div> 
                                </div>
                                <div class="col-12 col-sm-3 col-md-2 m-none">
                                    <ul class="desc" style="list-style-type: none!important; padding-left:0px;">
                                        <li>
                                            @if($locale==='th')
                                                ผู้ใช้งาน :
                                            @else
                                                User :
                                            @endif
                                            
                                            {{$reviewss['user_name']}} 
                                        </li>
                                        <li>
                                            @if($locale==='th')
                                                วันที่ :
                                            @else
                                                Date :
                                            @endif
                                            {{ substr($reviewss['created_at'],0,10) }}
                                        </li>                            
                                        <li>
                                            @if($locale==='th')
                                                ยอดคนดู :
                                            @else
                                                Views :  
                                            @endif
                                        <span id="power_review_showhits"> {{$reviewss['hit']}} </span>
                                        </li>                        
                                    </ul>
                                </div>
                            </div>
                            @endforeach

                            <!--<tr>
                                <td class="distance">
                                    <div class="tb-center">
                                        {{$reviewss['id']}}
                                    </div>
                                </td>
                                <td>
                                    <div class="tb-center">
                                        <img src="/public/product/{{$reviewss['product']['cover_img']}}" height="42"
                                            width="42" class="img-fluid rounded mx-auto">

                                    </div>
                                </td>
                                <td class="distance">
                                    <div class="tb-left">

                                        <a href='/review/board/view/category/{{$reviewss['id']}}/{{$reviewss['product_id']}}'
                                            class="review">{{$reviewss['title']}}</a>
                                    </div>
                                </td>
                                <td class="distance">
                                    <div class="tb-center">
                                        @for ($i = 0; $i < $reviewss['score']; $i++) <span class="icon">★</span>
                                            @endfor
                                    </div>
                                </td>
                                <td class="distance">
                                    <div class="tb-center">
                                        {{ substr($reviewss['created_at'],0,10) }}
                                    </div>
                                </td>
                                <td class="distance">
                                    <div class="tb-center">
                                        {{$reviewss['hit']}}
                                    </div>
                                </td>
                            </tr>
                         

                        </tbody>
                    </table>
                </div>
                <div class="text-center">
                    <ul class="pagination">

                    </ul>
                </div> -->
            </div>

        </main>
    </div>
</div>

@endsection