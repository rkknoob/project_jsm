@extends('layouts.template.frontend')

@section('content')
    <style>
        i.bigger-size {
            margin-top:50px;
            font-size: 200px;
            color: #4EC44A;
            background-color: #fff;
        }
        .text-success{
            margin-bottom:50px;
        }
         /* Alert */
         .label {
            display: inline;
            font-size: 75%;
            font-weight: 700;
            line-height: 1;
            color: rgb(255, 255, 255);
            text-align: center;
            white-space: nowrap;
            vertical-align: baseline;
            padding: 0.5em 1em !important;
            border-radius: 0.25em;
        }
        .label-default {
            background-color: rgb(210, 214, 222);
            color: rgb(68, 68, 68);
        }
        .label-success {
            background-color: rgb(0, 166, 90) !important;
            color: rgb(255, 255, 255) !important;
        }
        .label-warning{
            background-color: rgb(243, 156, 18) !important;
            color: rgb(255, 255, 255) !important;
        }
    </style>

    <div>
        <!-- Page Heading -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald|Roboto">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css">

        <div class="container" style="margin:5%">
            <div class="row">
                <div class="col-12 col-md-12">
                    <h1 class="text-success" style="margin-bottom: 10px;">List Order Number</h1>
                    <table class="table">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Order Number</th>
                                <th scope="col">Payment Status</th>
                                <th scope="col">Delivery Status</th>
                                <th scope="col">Total Order</th>
                                <th scope="col">Total Shipping</th>
                                <th scope="col">Sum Price</th>
                                <th scope="col" style="width:90px"> Cart Product</th>
                            </tr>
                        </thead>
                        <tbody>
                     
                        @foreach($item as $order)
                            <tr>
                                <td>{{$num++}}</td>
                                <td>{{$order->cart_number}}</td>
                                <td>
                                    @if ($order->paymentStatus === "wait")
                                        <span class="label label-default">ยังไม่ชำระเงิน</span>
                                    @elseif ($order->paymentStatus === "confirm")
                                        <span class="label label-success">รอยืนยัน</span>
                                    @endif
                                </td>
                                <td>
                                    @if ($order->deliveryStatus === "wait")
                                        <span class="label label-default">รอการชำระเงิน</span>
                                    @elseif ($order->deliveryStatus === "inprogress")
                                        <span class="label label-warning">กำลังดำเนินการ </span>
                                    @elseif ($order->deliveryStatus === "sent")
                                        <span class="label label-success">ส่งแล้ว</span>
                                    @endif
                                </td>
                                <td>{{$order->totalOrder}}</td>
                                <td>{{$order->totalShipping}}</td>
                                <td>{{$order->sumPrice}}</td>
                                <td>
                                    @if ($order->deliveryStatus == "sent" || $order->deliveryStatus == "inprogress")
                                    <a role="button"  href="/myorder/edit/{{$order->id}}" class="btn btn-info">
                                        <i class="fas fa-calendar-week"></i>
                                        Click
                                    </a>
                                    @endif 
                                    @if($order->deliveryStatus === "wait")
                                     - 
                                    @endif 
                                
                                </td>
                            </tr>
                        @endforeach   
                        </tbody>
                    </table>
                    <div class="row justify-content-md-center" style="margin-right: 0px; margin-left: 0px;">
                        <a href="/Product/Category/list/cid/0">
                            <button style="margin-bottom:40px; float:right;" type="button" class="btn btn-success">Shopping Continues</button>  
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection