@extends('layouts.template.frontend')

@section('content')
    <style>
        i.bigger-size {
            margin-top:50px;
            font-size: 200px;
            color: #4EC44A;
            background-color: #fff;
        }
        .text-success{
            margin-bottom:50px;
        }
    </style>

    <div>
        <!-- Page Heading -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald|Roboto">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css">

        <div class="container" style="margin:5%">
            <div class="row">
                <div class="col-12 col-md-12">
                    <h1 class="text-success" style="margin-bottom: 10px;">List Product </h1>
                    <table class="table">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col" width="80px">#</th>
                                <th scope="col">Product</th>
                                <th scope="col">Unit Price</th>
                                <th scope="col">Quantity</th>
                                <th scope="col">Price</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($item as $product)
                            <tr>
                                <th scope="row">
                                    <img src="/public/product/{{ $product->cover_img }}" style="width:100%;" alt="Product thumbnail" title="Product thumbnail" >
                                </th>
                                <td style="text-align: left;">
                                    {{ $product->name_en }}<br/>
                                    Option :
                                    @foreach($listSku as $sku)
                                        @if($product->id_sku == $sku->sku )
                                            {{$sku->name_en}}
                                        @endif 
                                    @endforeach
                                </td>
                                <td>{{ $product->price }}</td>
                                <td>{{ $product->qty }}</td>
                                <td>{{ $product->sumPrice }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" style="text-align: right;" >
                    <h5>ยอดรวม ฿{{ $listorder->totalOrder }}</h5>
                    <h5>ค่าส่ง ฿{{ $listorder->totalShipping }}</h5>
                    <h5>รวมทั้งสิ้น ฿{{ $listorder->sumPrice }}</h5><br><br>
                </div>
            </div>
            @if($listorder->deliveryStatus == 'sent')
            <div class="row">   
                <div class="form-group col-md-6">
                    <label for="name_th"><b>Track Number</b><font color="red">*</font></label>
                    <input type="text" class="form-control" value="{{$listorder->trackNumber}}" readonly>
                </div>

                <div class="form-group col-md-6" hidden>
                    <label for="name_th"><b>Ax Free</b><font color="red">*</font></label>
                    <input type="text" class="form-control" value="{{$listorder->axFree}}" readonly>
                </div>              
            </div>
            @endif
            <div class="row justify-content-md-center" style="margin-right: 0px; margin-left: 0px;">
                <a href="/Product/Category/list/cid/0">
                    <button style="margin-bottom:40px; float:right;" type="button" class="btn btn-success">Shopping Continues</button>  
                </a>
            </div>
        </div>
    </div>
    
@endsection