<!DOCTYPE html>
<html>
<head>
    <title>Welcome Email</title>
</head>

<body>
<h2>Welcome to the site JSM Thailand</h2>
<br/>
Your registered email-id is {{$email}} , Please click on the below link to verify your email account
<br/>
<a href="{{url('user/verify', $gen_token)}}">Verify Email</a>

</body>

</html>
