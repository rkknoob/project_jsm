@extends('layouts.ingredient')

@section('content')
<style>
 .container{
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
 }

</style>
<div class="container">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ $data_ingredient->title_en }}</div>
                {!! $data_ingredient->detail_en !!}
            </div>
        </div>
</div>
@endsection
