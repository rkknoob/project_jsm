@extends('layouts.template.frontend')

<style type="text/css">

    @font-face {
        font-family:'Exo2-Bold';
        src: url({!! asset("fonts/Exo2/Exo2-Bold.ttf") !!}) ;
    }
    @font-face {
        font-family:'Exo2-Medium';
        src: url({!! asset("fonts/Exo2/Exo2-Medium.ttf") !!}) ;
    }

    /* ภาษาไทย */
    @font-face {
        font-family: 'SukhumvitSet';
        src: url('/fonts/Sukhumvit/SukhumvitSet-Text.ttf');
    }
    .text-thai {
        font-family: 'SukhumvitSet';
        font-style: normal;
        letter-spacing: 0px;
    }

    #ju-container {
        padding: 50px 0!important;
    }
    .exo2-bold{
        font-family: 'Exo2-Bold';
    }
    .bolder{
        font-weight: bolder;
    }
    .link_del a {
        width: 37px;
        margin: 0px auto;
    }
    .remove, .modify, .empty, .continue, .order{
        border-radius: 0!important;
        font-family: "Exo2-Medium" ;
    }
    .table{
        font-family: "Exo2-Medium" ;
    }
    .name-product {
        font-family: "Exo2-Bold";
        color: rgb(229, 26, 146)!important;
        text-decoration: none;
    }
    .uni-opt{
        margin-top:2%;
    }
    .btn-options{
        color: #fff;
        background-color: #000;
        padding: 3px 8px;
    }
    .form-control{
        border-radius: 0!important;
    }
    .continue{
        padding-bottom: 8px!important;
    }

</style>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>JUNG SAEM MOOL｜Cosmetics - Official Site</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
</head>
@php
    $locale = session()->get('locale');
    if($locale == 'th'){
        $a = 'text-thai';
    }else{
        $a = '';
    }
@endphp

@section('content')

    <div id="ju-container">
        <div id="ju-content" class="container">
            <div class="ju-page-title">
                <h1 class="entry-title">
                    <font style="vertical-align: inherit;">
                        <font class="{{$a}}" style="vertical-align: inherit;">@lang('lang.page_cart')</font>
                    </font>
                </h1>
            </div>
            <div class="woocommerce">
                <div class="row step_point">
                    <div class="col-lg-offset-3 col-lg-2 col-xs-4">
                        <div class="text-center">
                        <span class="fa-stack fa-lg fa-3x active">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-shopping-cart fa-stack-1x"></i>
                        </span>
                            <p class="h5 letters">
                                <font style="vertical-align: inherit;">
                                    <font class="bolder exo2-bold {{$a}}" style="vertical-align: inherit;font-weight: bolder;">@lang('lang.page_cart_basket')</font>
                                </font>
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-2 col-xs-4">
                        <div class="text-center">
                        <span class="fa-stack fa-lg fa-3x">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-credit-card fa-stack-1x"></i>
                        </span>
                            <p class="h5 letters">
                                <font style="vertical-align: inherit;">
                                    <font class="bolder exo2-bold {{$a}}" style="vertical-align: inherit;font-weight: bolder;">@lang('lang.page_cart_payment')</font>
                                </font>
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-2 col-xs-4">
                        <div class="text-center">
                        <span class="fa-stack fa-lg fa-3x">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-check fa-stack-1x"></i>
                        </span>
                            <p class="h5 letters">
                                <font style="vertical-align: inherit;">
                                    <font class="bolder exo2-bold {{$a}}" style="vertical-align: inherit;font-weight: bolder;">@lang('lang.page_cart_order')</font>
                                </font>
                            </p>
                        </div>
                    </div>
                </div>
                <hr class="clear">
                <div id="cartWrap">
                    <div class="page-body">
                        <h3 class="letters">
                            <font style="vertical-align: inherit;">
                                <font class="exo2-bold bolder {{$a}}" style="vertical-align: inherit;">@lang('lang.page_cart_title')</font>
                            </font>
                        </h3>
                        <div class="table-responsive table-cart">
                            <table summary="" class="table">
                                <colgroup>
                                    <col width="70">
                                    <col width="*">
                                    <col width="70">
                                    <col width="70">
                                    <col width="90">
                                    <col width="90">
                                    <col width="90">
                                </colgroup>
                                <thead>
                                <tr>
                                    <th scope="col" class="displaynone">
                                        <div class="tb-center">
                                            <input type="checkbox" name="__allcheck" onclick="all_basket_check(this);" class="MS_input_checkbox" checked="">
                                        </div>
                                    </th>
                                    <th scope="col">
                                        <div class="tb-center">
                                            <font style="vertical-align: inherit;">
                                                <font class="bolder {{$a}}" style="vertical-align: inherit;">@lang('lang.page_cart_photo')</font>
                                            </font>
                                        </div>
                                    </th>
                                    <th scope="col">
                                        <div class="tb-center">
                                            <font style="vertical-align: inherit;">
                                                <font class="bolder {{$a}}" style="vertical-align: inherit;">@lang('lang.page_cart_product')</font>
                                            </font>
                                        </div>
                                    </th>
                                    <th scope="col">
                                        <div class="tb-center" style="width: 85px;">
                                            <font style="vertical-align: inherit;" >
                                                <font class="bolder {{$a}}" style="vertical-align: inherit;">@lang('lang.page_cart_unit')</font>
                                            </font>
                                        </div>
                                    </th>
                                    <th scope="col">
                                        <div class="tb-center">
                                            <font style="vertical-align: inherit;">
                                                <font class="bolder {{$a}}" style="vertical-align: inherit;">@lang('lang.page_cart_quantity')</font>
                                            </font>
                                        </div>
                                    </th>
                                    <th scope="col">
                                        <div class="tb-center" style="width: 105px;">
                                            <font style="vertical-align: inherit;">
                                                <font class="bolder {{$a}}" style="vertical-align: inherit;">@lang('lang.page_cart_price')</font>
                                            </font>
                                        </div>
                                    </th>
                                    <th scope="col" style="width: 310px;">
                                        <div class="tb-center">
                                            <font style="vertical-align: inherit;">
                                                <font class="bolder {{$a}}" style="vertical-align: inherit;">@lang('lang.page_cart_shipping')</font>
                                            </font>
                                        </div>
                                    </th>
                                    <th scope="col">
                                        <div class="tb-center">
                                            <font style="vertical-align: inherit;">
                                                <font class="bolder {{$a}}" style="vertical-align: inherit;">@lang('lang.page_cart_cancel')</font>
                                            </font>
                                        </div>
                                    </th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <td colspan="7">
                                        <div class="tb-right">
                                            <font style="vertical-align: inherit;">
                                                <font class="{{$a}}" style="vertical-align: inherit;">
                                                    @lang('lang.page_cart_amount') (5 @lang('lang.page_cart_items')) =
                                                </font>
                                            </font>
                                            <strong>
                                                <font style="vertical-align: inherit;">
                                                    <font class="{{$a}}" style="vertical-align: inherit;">17,180 @lang('lang.page_cart_baht')</font>
                                                </font>
                                            </strong>
                                        <!-- <font style="vertical-align: inherit;">
                                                <font class="{{$a}}"  style="vertical-align: inherit;">
                                                    (Deposit 4,700 @lang('lang.page_cart_baht'))
                                                </font>
                                            </font> -->
                                        </div>
                                    </td>
                                </tr>
                                </tfoot>

                                <tbody>
                                <tr>
                                    <td class="displaynone">
                                        <div class="tb-center">
                                            <input type="checkbox" name="basketchks" id="basketchks" checked="checked" class="MS_input_checkbox pr_NORMAL_2176668" onclick="cal_basket_chk(this);">
                                            <input type="hidden" name="basket_item" value="{&quot;uid&quot;:&quot;2176668&quot;,&quot;cart_id&quot;:&quot;1&quot;,&quot;cart_type&quot;:&quot;NORMAL&quot;,&quot;pack_uid&quot;:&quot;&quot;}">
                                            <input type="hidden" name="extra_item" value="{&quot;extra_require_uid&quot;:null,&quot;extra_require&quot;:null,&quot;extra_main_brandname&quot;:&quot;&quot;}">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="tb-center">
                                            <div class="thumb">
                                                <a href="#">
                                                    <img src="/public/product/2020042918410448GfI.jpg" alt="Product thumbnail" title="Product thumbnail">
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="tb-left">
                                            <h5 class="media-heading">
                                                <a href="#">
                                                    <font class="name-product" style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">Jeongsaemul Skin Nude Foundation </font>
                                                    </font>
                                                </a>
                                            </h5>
                                            <span class="MK-product-icons"></span>
                                            <div id="2176668_1" class="tb-left tb-opt">
                                                <strong>
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">[Option]</font>
                                                    </font>
                                                </strong>
                                                <font style="vertical-align: inherit;">
                                                    <font style="vertical-align: inherit;">
                                                        Color: Medium, Lip locker Color: 4 Rose petals
                                                    </font>
                                                </font>
                                            </div>
                                            <div class="uni-opt">
                                                <a href="javascript:modify_option('2176668', '1','');">
                                                    <font style="vertical-align: inherit;">
                                                        <font class="btn-options {{$a}}" style="vertical-align: inherit;">@lang('lang.page_cart_options')</font>
                                                    </font>
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="tb-center">
                                            <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">฿4,160</font>
                                            </font>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="tb-center">
                                            <div class="num_c">
                                                <input type="text" name="amount" value="4" class="form-control input-text qty text">
                                                <a href="javascript:count_change(0, 0)" class="num_up">
                                                    <i class="fa fa-caret-up"></i>
                                                </a>
                                                <a href="javascript:count_change(1, 0)" class="num_down">
                                                    <i class="fa fa-caret-down"></i>
                                                </a>
                                            </div>
                                            <a href="javascript:send_basket(0, 'upd')" class="modify btn btn-gray btn-block btn-sm" style="margin-top: 3px;">
                                                <i class="fa fa-repeat"></i>
                                                <span class="letters">
                                                    <font style="vertical-align: inherit;">
                                                        <font class="{{$a}}" style="vertical-align: inherit;">@lang('lang.page_cart_modified')</font>
                                                    </font>
                                                </span>
                                            </a>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="tb-center tb-bold">
                                            <span>
                                                <font style="vertical-align: inherit;">
                                                    <font class="{{$a}}" style="vertical-align: inherit;">฿16,640</font>
                                                </font>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="tb-center tb-delivery">
                                            <div class="MS_tb_delivery">
                                                <span class="MS_deli_txt" onmouseover="overcase(this, '0')" onmouseout="outcase(this, '0')">
                                                    <span class="MS_deli_title MS_deli_block">
                                                        <font style="vertical-align: inherit;">
                                                            <font style="vertical-align: inherit;">[Basic Shipping]</font>
                                                        </font>
                                                    </span>
                                                    <span class="MS_deli_desc MS_deli_block">
                                                        <font style="vertical-align: inherit;">
                                                            <font style="vertical-align: inherit;">Condition</font>
                                                        </font>
                                                    </span>
                                                </span>
                                                <div id="deliverycase0" class="MS_layer_delivery">
                                                    <dl>
                                                        <dt>
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">Delivery condition: Basic delivery (condition)</font>
                                                            </font>
                                                        </dt>
                                                        <dd>
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">
                                                                    If the order amount is
                                                                </font>
                                                                <font style="vertical-align: inherit;">
                                                                    less than
                                                                </font>
                                                            </font>
                                                            <span class="MS_highlight">
                                                                <font style="vertical-align: inherit;">
                                                                    <font style="vertical-align: inherit;">30,000 won</font>
                                                                </font>
                                                            </span>
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;"> ,</font>
                                                            </font>
                                                            <br>
                                                            <br style="line-height:3px">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">shipping fee of </font>
                                                            </font>
                                                            <span class="MS_highlight">
                                                                <font style="vertical-align: inherit;">
                                                                    <font style="vertical-align: inherit;">2,500 won</font>
                                                                </font>
                                                            </span>
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">
                                                                    will be charged.
                                                                </font>
                                                            </font>
                                                        </dd>
                                                    </dl>
                                                    <span class="bull"></span>
                                                    <!-- <iframe id="deliverycase_iframe0" class="MS_layer_delivery_iframe" frameborder="no" border="0"></iframe> -->
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="tb-center">
                                            <span class="d-block link_del">
                                                <a href="javascript:send_basket(0, 'del')" data-product_id="16828" data-product_sku="" data-toggle="tooltip" data-placement="top" data-original-title="@lang('lang.page_cart_remove')" class="remove btn btn-primary btn-block">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            </span>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="displaynone">
                                        <div class="tb-center">
                                            <input type="checkbox" name="basketchks" id="basketchks" checked="checked" class="MS_input_checkbox pr_NORMAL_2191468" onclick="cal_basket_chk(this);">
                                            <input type="hidden" name="basket_item" value="{&quot;uid&quot;:&quot;2191468&quot;,&quot;cart_id&quot;:&quot;1&quot;,&quot;cart_type&quot;:&quot;NORMAL&quot;,&quot;pack_uid&quot;:&quot;&quot;}">
                                            <input type="hidden" name="extra_item" value="{&quot;extra_require_uid&quot;:null,&quot;extra_require&quot;:null,&quot;extra_main_brandname&quot;:&quot;&quot;}">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="tb-center">
                                            <div class="thumb">
                                                <a href="#">
                                                    <img src="/public/product/20200429183458eC9Bq.jpg" alt="Product thumbnail" title="Product thumbnail">
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="tb-left">
                                            <h5 class="media-heading">
                                                <a href="#">
                                                    <font class="name-product" style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">2 boxes of double clean crystal makeup pads (2 sheets x 40 packs/80 sheets in total)</font>
                                                    </font>
                                                </a>
                                            </h5>
                                            <span class="MK-product-icons"></span>
                                            <div id="2191468_1" class="tb-left tb-opt">
                                                <strong>
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">[Option]</font>
                                                    </font>
                                                </strong>
                                                <font style="vertical-align: inherit;">
                                                    <font style="vertical-align: inherit;"> Option: 2 boxes 1</font>
                                                </font>
                                            </div>
                                            <div class="uni-opt">
                                                <a href="javascript:modify_option('2191468', '1','');" class="modify">
                                                    <font style="vertical-align: inherit;">
                                                        <font class="btn-options {{$a}}" style="vertical-align: inherit;">@lang('lang.page_cart_options')</font>
                                                    </font>
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="tb-center">
                                            <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">฿540</font>
                                            </font>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="tb-center">
                                            <div class="num_c">
                                                <input type="text" name="amount" value="1" class="form-control input-text qty text">
                                                <a href="javascript:count_change(0, 1)" class="num_up">
                                                    <i class="fa fa-caret-up"></i>
                                                </a>
                                                <a href="javascript:count_change(1, 1)" class="num_down">
                                                    <i class="fa fa-caret-down"></i>
                                                </a>
                                            </div>
                                            <a href="javascript:send_basket(1, 'upd')" class="modify btn btn-gray btn-block btn-sm" style="margin-top: 3px;">
                                                <i class="fa fa-repeat"></i>
                                                <span class="letters">
                                                    <font style="vertical-align: inherit;">
                                                        <font class="{{$a}}" style="vertical-align: inherit;">@lang('lang.page_cart_modified')</font>
                                                    </font>
                                                </span>
                                            </a>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="tb-center tb-bold">
                                            <span>
                                                <font style="vertical-align: inherit;">
                                                    <font class="{{$a}}" style="vertical-align: inherit;">฿540</font>
                                                </font>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="tb-center tb-delivery">
                                            <div class="MS_tb_delivery">
                                                <span class="MS_deli_txt" onmouseover="overcase(this, '1')" onmouseout="outcase(this, '1')">
                                                    <span class="MS_deli_title MS_deli_block">
                                                        <font style="vertical-align: inherit;">
                                                            <font style="vertical-align: inherit;">[Basic Shipping]</font>
                                                        </font></span><span class="MS_deli_desc MS_deli_block">
                                                        <font style="vertical-align: inherit;">
                                                            <font style="vertical-align: inherit;">Condition</font>
                                                        </font>
                                                    </span>
                                                </span>
                                                <div id="deliverycase1" class="MS_layer_delivery">
                                                    <dl>
                                                        <dt>
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">Delivery condition: Basic delivery (condition)</font>
                                                            </font>
                                                        </dt>
                                                        <dd>
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">If the order amount is </font>
                                                                <font style="vertical-align: inherit;">less than </font>
                                                            </font>
                                                            <span class="MS_highlight">
                                                                <font style="vertical-align: inherit;">
                                                                    <font style="vertical-align: inherit;">30,000 won</font>
                                                                </font>
                                                            </span>
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;"> , </font>
                                                            </font>
                                                            <br>
                                                            <br style="line-height:3px">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">shipping fee of </font>
                                                            </font>
                                                            <span class="MS_highlight">
                                                                <font style="vertical-align: inherit;">
                                                                    <font style="vertical-align: inherit;">2,500 won</font>
                                                                </font>
                                                            </span>
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;"> will be charged.</font>
                                                            </font>
                                                        </dd>
                                                    </dl>
                                                    <span class="bull"></span>
                                                    <!-- <iframe id="deliverycase_iframe1" class="MS_layer_delivery_iframe" frameborder="no" border="0"></iframe> -->
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="tb-center">
                                            <span class="d-block link_del">
                                                <a href="javascript:send_basket(1, 'del')" data-product_id="16828" data-product_sku="" data-toggle="tooltip" data-placement="top" data-original-title="@lang('lang.page_cart_remove')" class="remove btn btn-primary btn-block">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            </span>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div><!-- .table-fill-prd -->

                        <div class="clearfix">
                            <p class="pull-left">
                                <a href="javascript:basket_clear();" class="btn btn-primary empty">
                                    <i class="fa fa-trash"></i>
                                    <font style="vertical-align: inherit;">
                                        <font class="{{$a}}" style="vertical-align: inherit;"> @lang('lang.page_cart_empty')</font>
                                    </font>
                                </a>
                            </p>
                            <p class="pull-right">
                                <a href="/html/mainm.html" class="btn btn-default continue">
                                    <font style="vertical-align: inherit;">
                                        <font class="{{$a}}" style="vertical-align: inherit;">@lang('lang.page_cart_continue')</font>
                                    </font>
                                </a>
                            </p>
                        </div>
                        <div class="wc-proceed-to-checkout">
                            <hr class="sm clear">
                            <a href="/order/checkout" class="btn btn-danger btn-block btn-lg alt wc-forward order">
                                <i class="fa fa-credit-card"></i>
                                <span class="letters">
                                <font style="vertical-align: inherit;">
                                    <font class="{{$a}}" style="vertical-align: inherit;">@lang('lang.page_cart_place')</font>
                                </font>
                            </span>
                            </a>
                        </div>
                        <div class="cart-ft2"></div>
                    </div><!-- .page-body -->
                </div><!-- #cartWrap -->
            </div><!-- .woocommerce -->
        </div><!-- #contentWrap -->
    </div>

@endsection
