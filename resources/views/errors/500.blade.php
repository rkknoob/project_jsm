@extends('layouts.template.frontend')



<style type="text/css">


    .home-product-tab .wpb_tabs .wpb_tabs_nav {
        font-family: "Exo2Regular"!important;
    }
    .cate-hover{
        font-family: "Exo2Regular"!important;
    }
    .column-index{
        margin-top: 0;
        padding-right: 0px !important;
        padding-left: 0px !important;
    }
    .vdo--youtube{
        margin-top: -141px;
    }


    .carousel-indicators li {
        display: inline-block !important;
        box-sizing: content-box !important;
        width: 8px !important;
        height: 8px !important;
        background: black !important;
        border: solid 3px white !important;
        border-color: rgba(255, 255, 255, 0.44) !important;
        margin: 4px !important;
        border-radius: 15px !important;
    }
    .carousel-indicators .active {
        display: inline-block !important;
        box-sizing: content-box !important;
        width: 8px !important;
        height: 8px !important;
        background: white !important;
        border: solid 3px white !important;
        border-color: rgba(0, 0, 0, 0.44) !important;
        margin: 4px !important;
        border-radius: 15px !important;
    }
    li.active {
        color: red;
    }
    @media not all,(-webkit-transform-3d){
        .carousel-inner>.item.active.left, .carousel-inner>.item.prev{
            transform: translate3d(-100%,0,0) !important;
            left: 0;
        }
        .carousel-inner>.item.active.right, .carousel-inner>.item.next{
            left: 0;
            -webkit-transform: translate3d(100%,0,0)!important;
            transform: translate3d(100%,0,0)!important;

        }
    }


    .home-product-tab .wpb_tabs .wpb_tabs_nav li.active a {
        color: #000;
        border: 4px solid #fff;
        padding: 10px 35px;
    }

    .home-product-tab .wpb_content_element .wpb_tabs_nav li.ui-tabs-active,
    .home-product-tab .wpb_content_element .wpb_tabs_nav .active {
        background-color: #fff;
    }

    .home-product-tab .wpb_content_element .wpb_tabs_nav li.ui-tabs-active,
    .home-product-tab .wpb_content_element .wpb_tabs_nav li a .active {
        color: black !important;
    }

    #banner {
        position: fixed;
        z-index: 999;
        top: 100px;
        left: 50px;
        height: 300px;
        width: 342px;
        background: #0d0d0d;
    }

    #close {
        position: absolute;
        top: 0px;
        right: 0px;
        font-family: Arial, Helvetica;
        font-size: 14px;
        color: #f4524d;
        cursor: pointer;
        font-weight: bold;
    }

    #shadowbox {
        position: fixed;
        z-index: 998;
        height: 100%;
        width: 100%;
    }


    /* Slide img */
    .carousel-inner {
    > .item {
        opacity: 0;
        top: 0;
        left: 0;
        width: 100%;
        display: block;
        position: absolute;
        z-index:0;
        transition: none;
        transform: translate3d(0,0,0) !important;
    &:first-of-type {
         position:relative;
     }
    }
    > .active {
        opacity: 1;
        z-index:3;
    }

    > .next.left,
    > .prev.right {
        transition: opacity 0.6s ease-in-out;
        opacity: 1;
        left: 0;
        z-index:2;
    }
    > .active.left,
    > .active.right {
        z-index:1;
    }
    }
    .carousel-control {
        z-index:4;
    }

    /* Make the youtube video responsive */
    .iframe-container{
        position: relative;
        width: 100%;
        padding-bottom: 56.25%;
        height: 0;
    }
    .iframe-container iframe{
        position: absolute;
        top:0;
        left: 0;
        width: 100%;
        height: 100%;
    }


    @media screen and (min-width: 1024px) {
        #mobile-video{
            display:none;
        }
    }
    @media screen and (max-width: 991px) {


        .vdo--youtube {
            margin-top: -38px;
        }
    }


    @media screen and (max-width: 768px) {
        #mobile-video{
            display:none;
        }
        .vc_row {
            margin-top: -45px!important;
        }

        #video_bg {
            margin-top: 4%;
        }

    }

    @media screen and (min-width: 768px) {
        #mobile-video{
            display:none;
        }
    }

    @media screen and (max-width: 500px) {
        #video_bg{
            display:none;
        }
        #mobile-video{
            display:block;
            margin-top: 5%;
            margin-right: -1%;
        }
        .mobile{
            display:none !important;
        }
        .vdo--youtube{
            margin-top: -25%;
        }
    }

    @media screen and (min-width: 320px) {

        #mobile-video{
            margin-top: 8%;
            margin-right: -2%;
        }
    }

    .carousel-control{
        /* font-family: fontello;
        font-size: 52px;
        position: absolute;
        top: 48%!important;
        width: auto;
        z-index: 10;
        cursor: pointer;
        -moz-background-size: 27px 44px;
        -webkit-background-size: 27px 44px;
        background-size: 27px 44px;
        background-position: center;
        background-repeat: no-repeat; */
    }






</style>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> JSM </title>
    <meta name="description" content="">
    <meta name="keywords" content="">
</head>

@section('content')



    <!-- pop-up -->

    <!-- content -->
    <div id="ju-container">

        <div id="ju-content">


            <!--.wpb_column vc_column_container vc_col-sm-12-->
        </div>
        <!--.vc_row wpb_row vc_row-fluid vc_custom_1464829003762 vc_row-has-fill vc_row-o-full-height vc_row-o-columns-bottom vc_row-flex-->

        <div class="vc_row wpb_row vc_row-fluid home-product-tab vc_custom_1467341696348 vc_row-has-fill">
            <div class="wpb_column vc_column_container vc_col-sm-12">


                <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 text-center">
                    <div class="h5">You do not have access !</div>
                    <h1 class="m-top-none error-heading">500</h1>



                    <div class="input-group m-bottom-md">


                    </div><!-- /input-group -->
                    <a class="btn btn-success m-bottom-sm" href="/"><i class="fa fa-home"></i> Back to Web</a>
                </div><!-- /.col -->
        </div>
    </div>

@endsection
<!---->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
<script src='{{asset('jsmbeauty/js/jquery.zoom.min.js')}}'></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
</script>
<script>
    // pop-up//



    var locale = '{{ config('app.locale') }}';
    console.log(locale);
    if(locale == 'th'){
        var ba = "บาท";
    }else {
        var ba = "Baht";
    }

    //Your Function
    function yourfunction() {
        $('#banner').hide();
    }
    $('#click').click(function() {
        $('#shadowbox, #banner').show();
    });

    $('#test').click(function() {
        alert('Button was clicked');
    })
    // pop-up //

    function products(id) {



        $.ajax({
            dataType: 'json',
            type: 'get',
            data: {
                id: id
            },
            url: '/product/list/' + id,
            success: function(data) {

                var html = '';
                $.each(data.datas, function(index, itemdata) {
                    html +=
                        '<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 post-18124 product type-product status-publish has-post-thumbnail product_cat-tools product_cat-227 shipping-taxable purchasable product-type-simple product-cat-tools instock" style="padding:0px 2.5px;">';
                    html += '<a class="product_link_wrap" href="/product/detail/view/' + itemdata.id +
                        '">';
                    html += '<img src="/public/product/' + itemdata.cover_img + '"/>';
                    html += '<div class="cate-hover">';
                    html += '<h3>' + itemdata.name_en + '</h3>';
                    html += '<strong class="price"><span class="amount">' + formatNumber(itemdata.price) +
                        ' </span></strong>' + ba ;
                    // html += '<div class="small post_excerpt">' + itemdata.name_en + '</div>';
                    html += '</div>';
                    html += '</div>';
                });
                $('#product_base').html(html);

            }
        })


    }

    // 2. This code loads the IFrame Player API code asynchronously.
    // 2. This code loads the IFrame Player API code asynchronously.
    var tag = document.createElement('script');

    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    // 3. This function creates an <iframe> (and YouTube player)
    //    after the API code downloads.
    var player;
    function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
            width: '100%',
            videoId: 'T9CrVR9HJQQ',
            playerVars: { 'autoplay': 1, 'playsinline': 1 },
            events: {
                'onReady': onPlayerReady
            }
        });
    }

    // 4. The API will call this function when the video player is ready.
    function onPlayerReady(event) {
        event.target.mute();
        event.target.playVideo();
    }

    // 5. The API calls this function when the player's state changes.
    //    The function indicates that when playing a video (state=1),
    //    the player should play for six seconds and then stop.
    var done = false;
    function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.PLAYING && !done) {
            setTimeout(stopVideo, 10000);
            done = true;
        }
    }
    function stopVideo() {
        player.stopVideo();
    }


    function formatNumber (num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
    }

</script>


<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

