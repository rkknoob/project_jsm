<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sendemail extends Model
{

    protected $table = 'sendemail';

    protected $fillable = [
        'id',
        'email',
        'token',
        'type'

    ];
}
