<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\OrderCartModel;

use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use DB;


class OrderExport implements FromCollection,WithHeadings,ShouldAutoSize,WithEvents,WithColumnFormatting
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $ordernumber,$dayfirst,$daylast,$statuspayment,$daypayment;

    function __construct($ordernumber,$dayfirst,$daylast,$statuspayment,$daypayment) {
        $this->ordernumber = $ordernumber;
        $this->dayfirst = $dayfirst;
        $this->daylast = $daylast;
        $this->statuspayment = $statuspayment;
        $this->daypayment = $daypayment;


    }
    public function collection()
    {

        $query = OrderCartModel::query()
            ->select('session_cart.cart_number','session_cart.created_at','response_payment.created_at as createpayment','users.fname','users.lname','users.phone','users.email','users.address','districts.province','districts.zipcode','session_cart.trackNumber','order.id_sku','product.name_en','order.price','order.qty','order.sumPrice','session_cart.totalOrder','session_cart.totalShipping','session_cart.sumPrice as orderpayment','session_cart.deliveryStatus')
            ->leftJoin('users', 'users.id', '=', 'session_cart.id_user')
            ->leftJoin('order', 'order.id_cart', '=', 'session_cart.id')
            ->leftJoin('product', 'product.sku', '=', 'order.id_sku')
            ->leftJoin('response_payment', 'response_payment.order_id', '=', 'session_cart.cart_number')
            ->leftJoin('districts', 'districts.id', '=', 'users.id_address');

        if ($this->ordernumber != "nodata") {
            $query = $query->where('session_cart.cart_number', $this->ordernumber);
        }
        if ($this->statuspayment != "nodata") {
            $query = $query->where('session_cart.paymentStatus', $this->statuspayment);
        }
        if ($this->dayfirst != "nodata") {
            $query->whereDate('session_cart.created_at', '>=',$this->dayfirst);
        }
        if($this->daylast != "nodata"){
            $query->whereDate('session_cart.created_at', '<=',$this->daylast);
        }
        if($this->daypayment != "nodata"){
            $query->whereDate('response_payment.created_at', '>=',$this->daypayment);
        }


        $export = $query->orderBy('session_cart.id', 'desc')->get();
        return $export;
    }

    public function headings(): array
    {
        return [
            'เลขที่สั่งซื้อ',
            'สร้างเมื่อ',
            'ชำระเงินเมื่อ',
            'ชื่อลูกค้า',
            'นามสกุลลูกค้า',
            'เบอร์โทรลูกค้า',
            'อีเมลล์',
            'ที่อยู่',
            'จังหวัด',
            'รหัสไปรษณี',
            'Tracking Number',
            'รหัสสินค้า',
            'ชื่อสินค้า',
            'ราคา(ต่อชิ้น)',
            'จำนวนสินค้า',
            'ราคารวมสินค้า',
            'ราคารายการสินค้า',
            'ราคาขนส่ง',
            'ราคารวม',
            'สถานะการจัดส่ง',
            'ช่องทางการสั่ง',
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'L1:L1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
            },
        ];
    }

    /**
     * @inheritDoc
     */
    public function columnFormats(): array
    {
        return [

            'L' => NumberFormat::FORMAT_NUMBER,
        ];
    }

}
