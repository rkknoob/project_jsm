<?php
namespace App\Http\Controllers\Frontend;
use App\Model\ProductDetailModel;
use App\CoreFunction\Product;
use App\Model\ProductModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cart;
use Session;
use App\Model\OrderModel;
use App\Model\ResponsePaymentModel;
use App\User;
use App\OrderCartModel;
use App\CoreFunction\GenUrl;
use DB;


class ProductpaymentController extends Controller
{

    public function cartlist()
    {
        $data = [
            'item' => session('product_incart'),
        ];

        // $cart = session('product_incart');
        return view('frontend/Payment.shop_basket')->with($data);
    }

    public function savecart()
    {

        // Generate number => เหลือเช็คข้ามวัน แล้วรายการ Order นับ1 ใหม่
        $max_id = OrderCartModel::max('id')+1;
        $cart_number = "JSMW".date('dmY').str_pad($max_id, 6, "0", STR_PAD_LEFT);

        $data_session = new OrderCartModel();
        $data_session->cart_number = $cart_number;
        $data_session->paymentStatus = "wait";
        $data_session->deliveryStatus = "wait";
        $data_session->save();

        $cart = session('product_incart');

        foreach ($cart as $key => $options) {
            // แปลงค่า String ให้เป็น Double


            $sku_total = str_replace(',','', $options['price_discount']);
            $sku_total_1 = floatval($sku_total);

            $query = ProductModel::query()
                ->where('product.sku',$options['sku_id'])
                ->leftJoin('product_details', 'product_details.id', '=', 'product.product_details_id')
                ->leftJoin('map_promotion', 'map_promotion.id_product', '=', 'product_details.id')
                ->leftJoin('promotion', 'map_promotion.id_promotion', '=', 'promotion.id')
                ->select('product_details.price','map_promotion.*','promotion.*')
                ->first();


            $total_sku_1 = Product::discount($query->price,$query->type,$query->discount);
            $total_sku = $total_sku_1 * $options['sku_qty'];

            $data = new OrderModel();
            $data->id_product = $options['product_id'];
            $data->id_sku = $options['sku_id'];
            $data->id_cart = $max_id ;
            $data->price = $sku_total_1 ;
            $data->qty = $options['sku_qty'];
            $data->sumPrice = $total_sku  ;
            $data->save();

            //ตัด Stock
            $datasku = ProductModel::where('sku',$options['sku_id'])->first();
            if($datasku->stock > $options['sku_qty']){
                \Log::info($datasku->stock);

                $stock = $datasku->stock - $options['sku_qty'] ;
                \Log::info($stock);
                $datasku_1 = ProductModel::where('sku',$options['sku_id'])->update(['stock' => $stock]);
            }else{
                return response()->json([
                    'msg_return' => 'บันทึกไม่สำเร็จ',
                    'code_return' => 501,
                ]);
            }

        }

        //update Order -> totalOrder ,totalShipping ,sumPrice
        $order_total = OrderModel::where('id_cart',$max_id)->sum('sumPrice');
        \log::info($order_total);

        $totalShipping = 0;
        if($order_total >= 500){
            $totalShipping = 0;
        } else {
            $totalShipping = 40;
        }

        $totalShipping_1 = floatval($totalShipping);

        $sumPrice = $order_total+$totalShipping;
        $data = OrderCartModel::findOrFail($max_id);
        $data->totalOrder = $order_total;
        $data->totalShipping = $totalShipping_1;
        $data->sumPrice = $sumPrice;
        $data->update();

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
            'id_cart' =>  $max_id,
        ]);

    }

    public function checkout()
    {

        return view('frontend/Order.send_order');
    }

    public function delCart(Request $request)
    {
        $datas =[];
        $carts = session('product_incart');
        unset($carts[$request->data]);
        $datas = null;
        session(['product_incart' => $carts]);
        $cart = session('product_incart');
        return response()->json([
            'status' => true,
            'sum_quantity' => count($cart),
        ]);
        //    return response()->json(['products' => $carts]);
    }

    public function delCartall(Request $request)
    {

        $cart = session()->forget('product_incart');
        \Log::info('1');
        return response()->json([
            'status' => true,
        ]);
        //    return response()->json(['products' => $carts]);
    }

    public function getCart()
    {

        $carts = session('product_incart');
        \Log::info('0');
        return response()->json(['products' => $carts]);
    }



    public function checkoutlist($id)
    {
        $data = OrderCartModel::findOrFail($id);

        $query = OrderCartModel::query()
            ->where('session_cart.id',$id)
            ->select('order.*','product.name_en','product.product_details_id','product_details.name_en as productname','product_details.cover_img')
            ->leftJoin('order', 'order.id_cart', '=', 'session_cart.id')
            ->leftJoin('product', 'product.sku', '=', 'order.id_sku')
            ->leftJoin('product_details', 'product_details.id', '=', 'product.product_details_id')
            ->get();

        $item = $query->sum('qty');
        $data = [
            'id_order' => $id,
            'data' => $data,
            'query' => $query,
            'item' =>  $item
        ];

        return view('frontend/Order.send_order')->with($data);
    }

    public function paymentCheckout($id)
    {

        $data = OrderCartModel::findOrFail($id);

        //Merchant's account information Production
        // $merchant_id = "014011000027591";  //Get MerchantID when opening account with 2C2P
        // $secret_key = "38DC5F550C8EDD903AC669C6D541F7EBA3EAB14DFC446D5C290BDEF37FB811DD";	   //Get SecretKey from 2C2P PGW Dashboard

        //Merchant's account information UAT
        $merchant_id = "014011000027591";
        $secret_key = "C22557F31C95D8ABCEF331F6FBB64416D93EC0ECAF80C6E21939BE71B777772B";

        $payment_description  = $data->cart_number;
        $order_id  = $data->cart_number;
        $currency = "764";

        $num1= substr(number_format($data->sumPrice,2, '.', ''),0,-3);
        $num2= substr(number_format($data->sumPrice,2, '.', ''),-2);

        $sumPrice = str_pad($num1, 10, "0", STR_PAD_LEFT);
        $amount  =  $sumPrice.$num2;

        $version = "8.5";
        $payment_url = "https://demo2.2c2p.com/2C2PFrontEnd/RedirectV3/payment";
        $result_url_1 =  "http://18.138.103.125/payment/success";
        //$result_url_1 =  "http://127.0.0.1:8000/payment/success";


        $simple_string =  $order_id.$amount;
        $ciphering = "AES-128-CTR";
        $encryption_key = "jsm_web";
        $options = 0;
        $encryption_iv = '1234567891011121';

        $encryption = openssl_encrypt($simple_string, $ciphering, $encryption_key, $options, $encryption_iv);

        $user_defined_1 = $encryption;

        Session::put('order_id', $order_id);
        Session::put('amount',$amount);

        // $params = $version.$merchant_id.$payment_description.$order_id.$currency.$amount.$user_defined_1.$result_url_1;
        // $hash_value = hash_hmac('sha256',$params, $secret_key,false);

        //Construct signature string
        // $params = $version.$merchant_id.$payment_description.$order_id.$currency.$amount.$user_defined_1.$result_url_1;
        // $hash_value = hash_hmac('sha256',$params, $secret_key,false);	//Compute hash value

        //Construct signature string
        $params = $version.$merchant_id.$payment_description.$order_id.$currency.$amount.$user_defined_1.$result_url_1;
        // $params = $version.$merchant_id.$payment_description.$order_id.$currency.$amount.$result_url_1;
        $hash_value = hash_hmac('sha256',$params, $secret_key,false);	//Compute hash value

        $data = [
            'merchant_id' => $merchant_id, //
            'payment_description' => $payment_description, //
            'order_id' => $order_id, //
            'currency' => $currency, //
            'amount' => $amount, //
            'version' => $version, //
            'payment_url' => $payment_url, //
            'result_url_1' => $result_url_1, //
            'hash_value' => $hash_value, //
            'user_defined_1' => $user_defined_1,
        ];

        return view('frontend.checkout', [
            'payment_url'  => $payment_url,
            'version'      => $version,
            'merchant_id'  => $merchant_id,
            'currency'     => $currency,
            'result_url_1' => $result_url_1,
            'hash_value'   => $hash_value,
            'payment_description' => $payment_description,
            'order_id'     => $order_id,
            'amount'       => $amount,
            'user_defined_1' => $user_defined_1,
        ]);
    }

    public function payment(Request $request)
    {

        \Log::info( $request->all());
        //Check E-mail Duplicate
        $listUser = User::where('email',$request->email)->first();
        \Log::info($listUser);
        if($listUser != "")
        {
            //Update User in Order
            $data = OrderCartModel::findOrFail($request->order_id);
            $data->id_user = $listUser->id;
            $data->update();
            \Log::info("update user1");
            //Update User in ListOrder->ยังไม่ได้ทำ

        }else{
            //create User
            $data = new User();
            $data->fname = $request->fname;
            $data->lname = $request->lname;
            $data->email = $request->email;
            $data->phone = $request->phone;
            $data->address = $request->address;
            $data->id_address = $request->input_id;
            $data->save();

            //Update User in Order
            $max_id = User::max('id');
            $data = OrderCartModel::findOrFail($request->order_id);
            $data->id_user =  $max_id;
            $data->update();
            \Log::info("update user2");
            //Update User in ListOrder->ยังไม่ได้ทำ
        }
    }

    public function updatecart(Request $request)
    {

        $cart = session('product_incart');

        $productId = $request->sku;
        $cart[$productId]['sku_qty'] = (int)$request->sku_qty;
        $cart[$productId]['sum_total'] = (float)$request->sku_total;

        session(['product_incart' => $cart]);
        $cart = session('product_incart');

        return response()->json([
            'status' => true,
            'carts' => $cart,
        ]);
    }

    public function success(Request $request)
    {

        $order_id = Session::get('order_id');
        $amount = Session::get('amount');

        $simple_string = $order_id.$amount;
        $ciphering = "AES-128-CTR";
        $encryption_key = "jsm_web";
        $options = 0;
        $encryption_iv = '1234567891011121';

        $encryption = openssl_encrypt($simple_string, $ciphering, $encryption_key, $options, $encryption_iv);
        \log::info($encryption);
        \log::info($request->user_defined_1);


        if($encryption == $request->user_defined_1){

            if($request->request_timestamp!=''){
                $data = new ResponsePaymentModel();
                $data->request_timestamp = $request->request_timestamp;
                $data->channel_response_code = $request->channel_response_code;
                $data->channel_response_desc = $request->channel_response_desc;
                $data->payment_status = $request->payment_status;
                $data->ReturnUrl = $request->ReturnUrl;
                $data->version = $request->version;
                $data->localizedMessage = $request->localizedMessage;
                $data->payment_channel = $request->payment_channel;
                $data->merchant_id = $request->merchant_id;
                $data->order_id = $request->order_id;
                $data->invoice_no = $request->invoice_no;
                $data->currency = $request->currency;
                $data->amount = $request->amount;
                $data->masked_pan = $request->masked_pan;
                $data->transaction_ref = $request->transaction_ref;
                $data->approval_code = $request->approval_code;
                $data->eci = $request->eci;
                $data->transaction_datetime = $request->transaction_datetime;
                $data->browser_info = $request->browser_info;
                $data->ippPeriod = $request->ippPeriod;
                $data->ippInterestType = $request->ippInterestType;
                $data->ippInterestRate = $request->ippInterestRate;
                $data->ippMerchantAbsorbRate = $request->ippMerchantAbsorbRate;
                $data->user_defined_1 = $request->user_defined_1;
                $data->user_defined_2 = $request->user_defined_2;
                $data->user_defined_3 = $request->user_defined_3;
                $data->user_defined_4 = $request->user_defined_4;
                $data->user_defined_5 = $request->user_defined_5;
                $data->hash_value = $request->hash_value;
                $data->backend_invoice = $request->backend_invoice;
                $data->stored_card_unique_id = $request->stored_card_unique_id;
                $data->recurring_unique_id = $request->recurring_unique_id;
                $data->payment_scheme = $request->payment_scheme;
                $data->process_by = $request->process_by;
                $data->sub_merchant_list = $request->sub_merchant_list;
                $data->save();

                if($request->channel_response_code == "00"){

                    $listOrder = OrderCartModel::where('cart_number',$request->order_id)->first();

                    //Update Status Payment
                    $data = OrderCartModel::findOrFail($listOrder->id);
                    $data->paymentStatus = "confirm";
                    $data->deliveryStatus = "inprogress";
                    $data->update();

                    $listitem = OrderModel::where('id_cart',$listOrder->id)->get();

                    //Product
                    $listProduct = DB::table('order')
                        ->where('id_cart', $listOrder->id)
                        ->leftJoin('product_details', 'product_details.id', '=', 'order.id_product')
                        ->select('order.*','product_details.id','product_details.name_en','product_details.cover_img')
                        ->get();

                    //ชื่อ Sku
                    $listSku = DB::table('order')
                        ->where('id_cart', $listOrder->id)
                        ->leftJoin('product', 'product.sku', '=', 'order.id_sku')
                        ->select('order.id_sku','product.sku','product.name_en')
                        ->get();

                    //User
                    $listUser = User::where('id', $listOrder->id_user)->first();

                    //clear session
                    session()->forget('product_incart');

                    $data = [
                        'listOrder' => $listOrder,
                        'listitem' => $listitem,
                        'listUser' =>  $listUser,
                        'listProduct' => $listProduct,
                        'listSku' => $listSku,

                    ];
                    return view('frontend/Order.success')->with($data);

                }else{
                    $listOrder = OrderCartModel::where('cart_number',$request->order_id)->first();

                    //Update Status Payment
                    $data = OrderCartModel::findOrFail($listOrder->id);
                    $data->paymentStatus = "cancel";
                    $data->deliveryStatus = "cancel";
                    $data->update();

                    $listitem = OrderModel::where('id_cart',$listOrder->id)->get();

                    //คืน Stock
                    $cart = session('product_incart');
                    foreach ($cart as $key => $options) {
                        $datasku = ProductModel::where('sku',$options['sku_id'])->first();
                        $stock = $datasku->stock + $options['sku_qty'];
                        $datasku_1 = ProductModel::where('sku',$options['sku_id'])->update(['stock' => $stock]);
                    }

                    //clear session
                    session()->forget('product_incart');

                    //Product
                    $listProduct = DB::table('order')
                        ->where('id_cart', $listOrder->id)
                        ->leftJoin('product_details', 'product_details.id', '=', 'order.id_product')
                        ->select('order.*','product_details.id','product_details.name_en','product_details.cover_img')
                        ->get();

                    //ชื่อ Sku
                    $listSku = DB::table('order')
                        ->where('id_cart', $listOrder->id)
                        ->leftJoin('product', 'product.sku', '=', 'order.id_sku')
                        ->select('order.id_sku','product.sku','product.name_en')
                        ->get();

                    //User
                    $listUser = User::where('id', $listOrder->id_user)->first();

                    $data = [
                        'listOrder' => $listOrder,
                        'listitem' => $listitem,
                        'listUser' =>  $listUser,
                        'listProduct' => $listProduct,
                        'listSku' => $listSku,

                    ];
                    return view('frontend/Order.fail')->with($data);
                }

            }else{

                $listOrder = OrderCartModel::where('cart_number',$request->order_id)->first();

                //Update Status Payment
                $data = OrderCartModel::findOrFail($listOrder->id);
                $data->paymentStatus = "cancel";
                $data->deliveryStatus = "cancel";
                $data->update();

                $listitem = OrderModel::where('id_cart',$listOrder->id)->get();

                //คืน Stock
                $cart = session('product_incart');
                foreach ($cart as $key => $options) {
                    $datasku = ProductModel::where('sku',$options['sku_id'])->first();
                    $stock = $datasku->stock + $options['sku_qty'];
                    $datasku_1 = ProductModel::where('sku',$options['sku_id'])->update(['stock' => $stock]);
                }

                //clear session
                session()->forget('product_incart');

                //Product
                $listProduct = DB::table('order')
                    ->where('id_cart', $listOrder->id)
                    ->leftJoin('product_details', 'product_details.id', '=', 'order.id_product')
                    ->select('order.*','product_details.id','product_details.name_en','product_details.cover_img')
                    ->get();

                //ชื่อ Sku
                $listSku = DB::table('order')
                    ->where('id_cart', $listOrder->id)
                    ->leftJoin('product', 'product.sku', '=', 'order.id_sku')
                    ->select('order.id_sku','product.sku','product.name_en')
                    ->get();

                //User
                $listUser = User::where('id', $listOrder->id_user)->first();

                $data = [
                    'listOrder' => $listOrder,
                    'listitem' => $listitem,
                    'listUser' =>  $listUser,
                    'listProduct' => $listProduct,
                    'listSku' => $listSku,

                ];
                return view('frontend/Order.fail')->with($data);
            }


        }else {
            return redirect('/');;
        }


    }
}
