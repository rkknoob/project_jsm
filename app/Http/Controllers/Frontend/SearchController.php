<?php

namespace App\Http\Controllers\Frontend;

use App\Model\CategoryModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ProductDetailModel;


class SearchController extends Controller
{
    //

    public function index(Request $request)
    {

            $products = ProductDetailModel::where('is_active','Y')
                ->Where('name_en', 'like', '%' . $request->datasearch . '%')
                ->orderBy('price', 'asc')->paginate(12);
            $chunk = $products->chunk(4);
            $data['Product_type'] = $products;
            $data['id'] = '1';
            $data['forma'] = 0;
            $data['datasearch'] = $request->datasearch;
            $data['Total'] = $products->count();



        return view('frontend.search')->with('products',$chunk)->with($data);

    }

    public function search(Request $request)
    {

        $mytime = Carbon::now();
        $ldate = date('Y-m-d');



        if($request->orderby == 1){
            $products = ProductDetailModel::where('is_active','Y')
                ->Where('name_en', 'like', '%' . $request->datasearch . '%')
                ->whereDate('start_time','<=',$ldate)
                ->whereDate('end_time','>=',$ldate)
                ->orderBy('price', 'asc')->paginate(12)->appends('datasearch',$request->datasearch);
            $data['forma'] = 1;
        }
        if($request->orderby == 2){
            $products = ProductDetailModel::where('is_active','Y')
                ->Where('name_en', 'like', '%' . $request->datasearch . '%')
                ->whereDate('start_time','<=',$ldate)
                ->whereDate('end_time','>=',$ldate)
                ->orderBy('price', 'desc')->paginate(12)->appends('datasearch',$request->datasearch);
            $data['forma'] = 2;
        }
        if($request->orderby == 3){
            $products = ProductDetailModel::where('is_active','Y')
                ->Where('name_en', 'like', '%' . $request->datasearch . '%')
                ->whereDate('start_time','<=',$ldate)
                ->whereDate('end_time','>=',$ldate)
                ->orderBy('name_en', 'asc')->paginate(12)->appends('datasearch',$request->datasearch);
            $data['forma'] = 3;
        }
        if($request->orderby == 0){
            $products = ProductDetailModel::where('is_active','Y')
                ->whereDate('start_time','<=',$ldate)
                ->whereDate('end_time','>=',$ldate)
                ->Where('name_en', 'like', '%' . $request->datasearch . '%')->paginate(12)->appends('datasearch',$request->datasearch);
            $data['forma'] = 0;
        }
        $chunk = $products->chunk(4);
        $data['Product_type'] = $products;
        $data['id'] = '1';
        $data['datasearch'] = $request->datasearch;
        $data['Total']  = ProductDetailModel::where('is_active','Y')
            ->Where('name_en', 'like', '%' . $request->datasearch . '%')
            ->whereDate('start_time','<=',$ldate)
            ->whereDate('end_time','>=',$ldate)
            ->count();

        return view('frontend.search')->with('products',$chunk)->with($data);
    }




}
