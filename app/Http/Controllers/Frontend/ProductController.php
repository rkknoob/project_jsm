<?php

namespace App\Http\Controllers\Frontend;

use App\CoreFunction\Helper;

use App\Menu;
use App\CoreFunction\Product;
use App\Model\CategoryModel;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ProductModel;
use App\Model\OptionModel;
use App\Model\ProductDetailModel;
use DB;
use Auth;
use Cookie;
use Illuminate\Http\Response;
use Session;
use App\SessionCart;
use App\Product_Incart;





class ProductController extends Controller
{

    public function index()
    {

        return view('frontend.product');
    }
    public function bastseller()
    {
        return view('frontend.bestseller');
    }
    public function base()
    {
        return view('frontend.base');
    }
    public function lip()
    {
        return view('frontend.lip');
    }
    public function skincare()
    {
        return view('frontend.product');
    }
    public function tool()
    {
        return view('frontend.product');
    }
    public function show($id,Request $request)
    {


        $uri = $request->path();

        $data['subject'] = Helper::subjectmenu($uri);

        $mytime = Carbon::now();
        $ldate = date('Y-m-d');

        $data['Category'] = CategoryModel::all();
        $banner = CategoryModel::where('id',$id)->value('img');
        $data['img_banner'] = $banner;

        if($id == '0' || $id == '150'){

            if($id == '150'){
                $products = ProductDetailModel::where('is_active','Y')
                ->whereDate('start_time','<=',$ldate)
                ->whereDate('end_time','>=',$ldate)
                ->where('is_bestseller','Y')
                ->orderBy('created_at', 'desc')->paginate(12);
            $chunk = $products->chunk(4);
            $data['Product_type'] = $products;
            $data['id'] = $id;
            $data['forma'] = 0;
            $data['Total'] = ProductDetailModel::where('is_active','Y')->where('is_bestseller','Y')->count();

            return view('frontend.product')->with('products',$chunk)->with($data);
            }
            $products = ProductDetailModel::where('is_active','Y')
                ->whereDate('start_time','<=',$ldate)
                ->whereDate('end_time','>=',$ldate)
                ->orderBy('created_at', 'desc')->paginate(12);
            $chunk = $products->chunk(4);
            $data['Product_type'] = $products;
            $data['id'] = $id;
            $data['forma'] = 0;
            $data['Total'] = ProductDetailModel::where('is_active','Y')->count();
            return view('frontend.product')->with('products',$chunk)->with($data);
        }else{
            $products = ProductDetailModel::where('is_active','Y')->where('category_id','=',$id)
                ->whereDate('start_time','<=',$ldate)
                ->whereDate('end_time','>=',$ldate)
                ->orderBy('created_at', 'desc')->paginate(12);
            $chunk = $products->chunk(4);
            $data['Product_type'] = $products;
            $data['id'] = $id;
            $data['forma'] = 0;
            $data['Total'] = ProductDetailModel::where('category_id','=',$id)->count();
            if($data['img_banner'] == null){
                return redirect('/');
            }
            return view('frontend.product')->with('products',$chunk)->with($data);

        }
        //return view('frontend.product')->with('products',$chunk)->with($data);

    }

    public function getall(Request $request,$te,$id)
    {

        if($id > 3){
            return redirect('/');
        }
        $uri = $request->path();
        $data['subject'] = Helper::subjectmenu($uri);

        $mytime = Carbon::now();
        $ldate = date('Y-m-d');
        $data['Category'] = CategoryModel::all();
        $banner = CategoryModel::where('id',$te)->value('img');
        $data['img_banner'] = $banner;

        if($te == '0' || $te == '150') {


            if ($id == 1) {
                $products = ProductDetailModel::where('is_active','Y')
                    ->whereDate('start_time','<=',$ldate)
                    ->whereDate('end_time','>=',$ldate)
                    ->orderBy('price', 'asc')->paginate(12);
                $chunk = $products->chunk(4);
                $data['Product_type'] = $products;
                $data['id'] = $te;
                $data['forma'] = $id;
                $data['Total'] = ProductDetailModel::all()->count();

                return view('frontend.product')->with('products', $chunk)->with($data);
            } elseif ($id == 2) {
                $products = ProductDetailModel::where('is_active','Y')
                    ->whereDate('start_time','<=',$ldate)
                    ->whereDate('end_time','>=',$ldate)
                    ->orderBy('price', 'desc')->paginate(12);
                $chunk = $products->chunk(4);
                $data['Product_type'] = $products;
                $data['id'] = $te;
                $data['forma'] = $id;
                $data['Total'] = ProductDetailModel::all()->count();

                return view('frontend.product')->with('products', $chunk)->with($data);
            } elseif ($id == 3) {
                $products = ProductDetailModel::where('is_active','Y')
                    ->whereDate('start_time','<=',$ldate)
                    ->whereDate('end_time','>=',$ldate)
                    ->orderBy('price', 'desc')->orderBy('name_en')->paginate(12);
                $chunk = $products->chunk(4);
                $data['Product_type'] = $products;
                $data['id'] = $te;
                $data['forma'] = $id;
                $data['Total'] = ProductDetailModel::all()->count();

                return view('frontend.product')->with('products', $chunk)->with($data);

            } else {

                $products = ProductDetailModel::where('is_active','Y')
                    ->whereDate('start_time','<=',$ldate)
                    ->whereDate('end_time','>=',$ldate)
                    ->paginate(12);
                $chunk = $products->chunk(4);
                $data['Product_type'] = $products;
                $data['id'] = $te;
                $data['forma'] = 0;
                $data['Total'] = ProductDetailModel::where('category_id','=',$te)->count();

                return view('frontend.product')->with('products',$chunk)->with($data);


            }

        }else{

            if ($id == 1) {

                $products = ProductDetailModel::where('is_active','Y')->where('category_id',$te)
                    ->whereDate('start_time','<=',$ldate)
                    ->whereDate('end_time','>=',$ldate)
                    ->orderBy('price', 'asc')->paginate(12);
                $chunk = $products->chunk(4);
                $data['Product_type'] = $products;
                $data['id'] = $te;
                $data['forma'] = $id;
                $data['Total'] = ProductDetailModel::all()->count();

                return view('frontend.product')->with('products',$chunk)->with($data);
            } elseif ($id == 2) {
                $products = ProductDetailModel::where('category_id',$te)->where('is_active','Y')
                    ->whereDate('start_time','<=',$ldate)
                    ->whereDate('end_time','>=',$ldate)
                    ->orderBy('price', 'desc')->paginate(12);
                $chunk = $products->chunk(4);
                $data['Product_type'] = $products;
                $data['id'] = $te;
                $data['forma'] = $id;
                $data['Total'] = ProductDetailModel::all()->count();

                return view('frontend.product')->with('products',$chunk)->with($data);

            } elseif ($id == 3) {
                $products = ProductDetailModel::where('category_id',$te)->where('is_active','Y')
                    ->whereDate('start_time','<=',$ldate)
                    ->whereDate('end_time','>=',$ldate)
                    ->orderBy('name_en', 'desc')->paginate(12);
                $chunk = $products->chunk(4);
                $data['Product_type'] = $products;
                $data['id'] = $te;
                $data['forma'] = $id;
                $data['Total'] = ProductDetailModel::all()->count();

                return view('frontend.product')->with('products',$chunk)->with($data);

            } else {

                $products = ProductDetailModel::where('category_id',$te)->where('is_active','Y')
                    ->whereDate('start_time','<=',$ldate)
                    ->whereDate('end_time','>=',$ldate)
                    ->orderBy('created_at', 'desc')->paginate(12);
                $chunk = $products->chunk(4);
                $data['Product_type'] = $products;
                $data['id'] = $te;
                $data['forma'] = $id;
                $data['Total'] = ProductDetailModel::all()->count();

                return view('frontend.product')->with('products',$chunk)->with($data);


            }


        }



    }

    public function getdata($id)
    {

        $mytime = Carbon::now();
        $ldate = date('Y-m-d');
        $t = Auth::user();
        if($t == null){
            $user_id = Auth::user();
        }else {
            $user_id = Auth::user()->id;
            $name = Auth::user()->name;
        }
        $datas = [];
 
        $section = ProductDetailModel::query()
            ->where('product_details.id', $id)
            ->where('product_details.is_active','Y')
            ->whereDate('product_details.start_time','<=',$ldate)
            ->whereDate('product_details.end_time','>=',$ldate)
            ->first();

        //ส่วนลด 
        $section_discount = ProductDetailModel::query()
        ->where('product_details.id',$section->id)
        ->where('promotion.is_active','Y')
        ->leftJoin('map_promotion', 'map_promotion.id_product', '=', 'product_details.id')
        ->leftJoin('promotion', 'map_promotion.id_promotion', '=', 'promotion.id')
        ->first();     
        \log::info("ลดจ้าาา");
        \log::info($section_discount);
   
        \log::info($section->price);
     
        $discount='';
        if($section_discount!= "")
        {
            \log::info("้มีค่า");
            \log::info($section_discount->type);
            \log::info($section_discount->discount);
            $discount = Product::discount($section->price,$section_discount->type,$section_discount->discount);
            $datas['type']['type'] = $section_discount->type;

        }
        else{
            \log::info("้ไม่มีค่า");
            $discount = Product::discount($section->price,'','');
            $datas['type']['type'] = '';
        }

        \log::info($discount);
        
     
    
        if($section == null){
            return redirect('/');
        }

        // $discount = Product::discount($section->price,$section_discount->type,$section_discount->discount);
  
        $sectionImages = $section->ProductDescrition;
        $sectionImages_th = $section->ProductDescritionth;
        $sectionColor = $section->Products;
        $sectionTop = $section->Producttopci->where('user_id',$user_id);
        $sectionCate = $section->Cate;
        $sectionreview = $section->productreview;
        $datas['id'] = $section->id;
        $datas['product_name'] = $section->name_en;
        $datas['img_product'] = $section->cover_img;
        $datas['img_product_zoom'] = $section->cover_zoom;
        $datas['product_price'] = $section->price;
        $datas['sales_price'] = $discount;
        $datas['display_type'] = $section->display_type;
        $datas['detail_th'] = $section->detail_th;
        $datas['detail_en'] = $section->detail_en;
        $datas['product_details'] = [];
        $datas['product'] = [];
        $datas['review'] = [];
        $datas['product_details_th'] = [];
        $type = null;
        $datas['category_name'] = $section->name;
        $datas['category_pic'] = $section->img;
        $datas['lang'] = session()->get('locale');
       

        if($sectionColor){
            foreach ($sectionColor as $key => $sectionColors) {
                $datas['product'][$key]['id'] = $sectionColors->id;
                $datas['product'][$key]['sku'] = $sectionColors->sku;
                $datas['product'][$key]['name_en'] = $sectionColors->name_en;
                $datas['product'][$key]['name_th'] = $sectionColors->name_th;
                $datas['product'][$key]['img_color'] = $sectionColors->img_color;
            }
        }

        if($sectionTop){
            foreach ($sectionTop as $key => $sectionTops) {
                $datas['topic'][$key]['id'] = $sectionTops->id;
                $datas['topic'][$key]['title'] = $sectionTops->title;
                $datas['topic'][$key]['created_at'] = $sectionTops->created_at;
                $datas['topic'][$key]['name'] = $name;
                $datas['topic'][$key]['user_id'] = $sectionTops->user_id;
            }
        }

        if($sectionCate)
        {
            $datas['category_name'] = $sectionCate->name;
            $datas['category_pic'] = $sectionCate->img;
        }


        if($sectionImages)
        {
            foreach ($sectionImages as $key => $sectionImage) {
                $datas['product_details'][$key]['seq'] = $sectionImage->seq;
                $datas['product_details'][$key]['img_en'] = $sectionImage->img_en;
                $datas['product_details'][$key]['img_th'] = $sectionImage->img_th;
                $datas['product_details'][$key]['detail_en'] = $sectionImage->detail_en;
                $datas['product_details'][$key]['detail_th'] = $sectionImage->detail_th;
            }
        }

        if($sectionImages_th)
        {
            foreach ($sectionImages_th as $key => $sectionImages_ths) {
                $datas['product_details_th'][$key]['seq'] = $sectionImages_ths->seq;
                $datas['product_details_th'][$key]['img_th'] = $sectionImages_ths->img_th;
            }
        }

        if($sectionreview)
        {
            foreach ($sectionreview as $key => $sectionreviews) {
                $datas['review'][$key]['id'] = $sectionreviews->id;
                $datas['review'][$key]['title'] = $sectionreviews->title;
                $datas['review'][$key]['product_id'] = $sectionreviews->product_id;
                $datas['review'][$key]['score'] = $sectionreviews->score;
                $datas['review'][$key]['created_at'] = $sectionreviews->created_at;
                $datas['review'][$key]['user_id'] = $sectionreviews->user_id;
                $user_name = User::findorfail($sectionreviews->user_id);
                $datas['review'][$key]['username'] = $user_name->name;
            }
        }

        return view('frontend/Product.view',$datas);
    }

    public function productlist()
    {
        $product = ProductModel::all();
        return response()->json([
            'datas' => $product,
        ]);
    }

    public function getdatalist($id)
    {

    //    $product = ProductDetailModel::where('category_id',$id)->take(12)->get();

        $mytime = Carbon::now();
        $ldate = date('Y-m-d');
        $product = ProductDetailModel::where('category_id',$id)
            ->orderBy('created_at', 'desc')
            ->where('is_active','Y')
            ->whereDate('start_time','<=',$ldate)
            ->whereDate('end_time','>=',$ldate)
            ->take(12)->get();
        return response()->json([
            'datas' => $product,
        ]);
    }


    public function choose(Request $request)
    {

        \Log::info("เลือก Sku");

        $id = $request->get('selValue');
        \Log::info($id);


        $datas = [];
        $section = ProductModel::query()
            ->select('product.id as id_sku','product.sku','product.name_en as sku_name','product.product_details_id','product.stock','product_details.name_en as product_name','product_details.price','promotion.*','map_promotion.*')
            ->where('product.id',$id)
            ->leftJoin('product_details', 'product_details.id', '=', 'product.product_details_id')
            ->leftJoin('map_promotion', 'map_promotion.id_product', '=', 'product_details.id')
            ->leftJoin('promotion', 'map_promotion.id_promotion', '=', 'promotion.id')
            ->first();

        $sectionCate = $section->test;
        $discount = Product::discount($section->price,$section->type,$section->discount);

        $datas['id'] = $section->id_sku;
        $datas['price'] = $section->price;
        $datas['sales_price'] = $discount;
        $datas['product_name'] = $section->sku_name;
        $datas['stock'] = $section->stock;
        $datas['sku'] = $section->sku;


      //  $actual_start_at = Carbon::parse($times->updated_at);
       // $current_timestamp = Carbon::now();
       // $mins            = $current_timestamp->diffInHours($actual_start_at, true);


        //   $session = session('product_incart');
      \Log::info($datas);




        return response()->json([
            'data' => $datas,
        ]);

    }
    public function color(Request $request)
    {


        $data = ProductModel::find($request->id);
        return response()->json([
            'data' => $data,
        ]);

    }

    public function checkStock(Request $request)
    {

        setcookie('TestCookie', '1234', time() + (60*30), "/");
        $value = '11111';

        //setcookie("TestCookie", $value,time()+60);

        foreach ($request->sku as $key => $options) {
            $product_sku = ProductModel::where('sku',$options['sku'])->first();
            if(($product_sku->stock) >= ($options['sku_qty'])) {
                $data['status'] = true;
            }else {
                $data['status'] = false;
                break;
            }
        }
        return response()->json(['status' => $data['status']]);
    }

    public function cutstock(Request $request)
    {
        $productId = $request->sku;
        $Id = $request->product_id;

        $product = ProductDetailModel::query()
            ->where('product_details.id',$Id)
            ->select('product_details.name_en','product_details.id as idproduct','product_details.price','product_details.cover_img','map_promotion.*','promotion.*')
            ->leftJoin('map_promotion', 'map_promotion.id_product', '=', 'product_details.id')
            ->leftJoin('promotion', 'map_promotion.id_promotion', '=', 'promotion.id')
            ->first();
   
        $cart = session('product_incart');
        foreach ($productId as $key => $options) {
            $quantity = !empty($cart[$options['sku']]) ? $cart[$options['sku']]['sku_qty'] : 0;
            $cart[$options['sku']]['sku_qty'] = ($quantity + $options['sku_qty']);
            $cart[$options['sku']]['sku_name'] = $options['sku_name'];
            $cart[$options['sku']]['sku_total'] = $product->price;

            $cart[$options['sku']]['img'] = $product->cover_img;
            $cart[$options['sku']]['product_name'] = $product->name_en;
            $cart[$options['sku']]['product_id'] = $product->idproduct;
            $cart[$options['sku']]['sku_id'] = $options['sku'];
            $discount = Product::discount(($product->price *(number_format($cart[$options['sku']]['sku_qty']))),$product->type,$product->discount);
            $price_discount = Product::discount($product->price,$product->type,$product->discount);
            $cart[$options['sku']]['sum_total'] = $discount;
            $cart[$options['sku']]['sales_total'] = $discount;
            $cart[$options['sku']]['price_discount'] = $price_discount;
        }

        session(['product_incart' => $cart]);

        return response()->json([
            'status' => true,
            'carts' => $cart,
            'sum_quantity' => count($cart),
        ]);

    }
    public function receive(Request $request)
    {
        session()->forget('product_incart');
        return response()->json(['status' => true]);
    }


}
