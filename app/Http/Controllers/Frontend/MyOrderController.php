<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\OrderCartModel;
use App\Model\OrderModel;
use DB;


class MyOrderController extends Controller
{
    //
    public function index()
    {
        return view('frontend.Myorder.index');
    }

    public function store(Request $request)
    {
        ///อีเมลว่ามีไหม//
        $user = User::where('email', $request->email)->first();

        if($user == ""){
            $return['code'] = 501;
            $return['content'] = 'ไม่สำเร็จ กรุณาตรวจสอบอีเมลของท่านอีกครั้ง';
        }else{
            $return['code'] = 200;
            $return['listUser'] =  $user->id;
        }
        $return['title'] = 'My Order';
        return $return;
    }

    public function getorder($id)
    {
        //รายการสินค้า
        $listitem = OrderCartModel::where('id_user',$id)->orderby('id','desc')->get();
        $data = [
            'item' => $listitem,
            'num' => 1,
        ];
        return view('frontend.Myorder.order')->with($data);
    }

    public function getproduct($id)
    {
        //Product
         $listitem = DB::table('order') 
         ->where('id_cart', $id)
         ->leftJoin('product_details', 'product_details.id', '=', 'order.id_product')
         ->select('order.*','product_details.id','product_details.name_en','product_details.cover_img')
         ->get();

        //ชื่อ Sku
        $listSku = DB::table('order') 
          ->where('id_cart', $id)
          ->leftJoin('product', 'product.sku', '=', 'order.id_sku')
          ->select('order.id_sku','product.sku','product.name_en')
          ->get();

        $listorder = OrderCartModel::where('id',$id)->first();

        $data = [
            'item' => $listitem,
            'listSku' => $listSku,
            'num' => 1,
            'listorder' => $listorder,
        ];
        return view('frontend.Myorder.product')->with($data);
    }
    
}
