<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\IngredientModel;

class IngredientController extends Controller
{
    public function show($id)
    {

        $data_ingredient = IngredientModel::find($id);
        return view('ingredient')->with('data_ingredient',$data_ingredient);
    }
}
