<?php

namespace App\Http\Controllers\API;

use App\Admin;
use App\Menu;
use App\Model\AdminHeadMenu;
use App\Model\AdminMenu;
use App\Model\AdminModel;
use App\Model\AdminProcessMenu;
use App\Model\AdminSubmenu;
use App\Model\Artist;
use App\Model\ArtistTipModel;
use App\Model\ArtistMag;
use App\Model\Brandjsm;
use App\Model\BrandMagazineModel;
use App\Model\CategoryModel;
use App\Model\Color;
use App\Model\EventModel;
use App\Model\Faq;
use App\Model\Faq_cate;
use App\Model\Langfront;
use App\Model\LangMenu;
use App\Model\LineStoryModel;
use App\Model\LineStoryDesciptionModel;
use App\Model\NoticeModel;
use App\Model\Product;
use App\Model\ProductDesciptionModel;
use App\Model\ProductDetailModel;
use App\Model\ProductModel;
use App\Model\Qa\QaanswersModel;
use App\Model\Qa\QacateModel;
use App\Model\Qa\QarelatModel;
use App\Model\Qa\QatopicModel;
use App\Model\VideoModel;
use App\Model\PopupModel;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Product_Details;
use App\Model\OptionModel;
use App\Model\BannerModel;
use App\CoreFunction\Datatable;
use Yajra\DataTables\DataTables;
use App\Model\Review;
use App\Model\FindStoreModel;
use App\Model\FindStore_cate;
use App\Model\Noticeimages;
use App\Model\ProductDesciption_Th;
use App\OrderCartModel;


class TestdataController extends Controller
{
    public function index(Request $request)
    {

        $query = OrderCartModel::query()
            ->select('session_cart.cart_number','session_cart.created_at','response_payment.created_at as createpayment','users.fname','users.lname','users.phone','users.email','users.address','districts.province','districts.zipcode','session_cart.trackNumber','product.sku','product.name_en','order.price','order.qty','order.sumPrice','session_cart.totalOrder','session_cart.totalShipping','session_cart.deliveryStatus')
            ->leftJoin('users', 'users.id', '=', 'session_cart.id_user')
            ->leftJoin('order', 'order.id_cart', '=', 'session_cart.id')
            ->leftJoin('product', 'product.sku', '=', 'order.id_sku')
            ->leftJoin('response_payment', 'response_payment.order_id', '=', 'session_cart.cart_number')
            ->leftJoin('districts', 'districts.id', '=', 'users.id_address');
        $query = $query->where('session_cart.cart_number', "JSM20200809000001");

        $export = $query->paginate();

        return $export;
    }

    public function store(Request $request)
    {
        /*
                $msg = "";
                $headers = $request->header();
                \Log::info($headers);
                if(array_key_exists('atoken', $headers)){
                    $data = "yellowidea";
                    $encryptSha = hash('sha256', $data);
                    if($headers['atoken'][0] == 'yellowidea'){
                        // $msg = "ข้อมูลการ Authen ถูกต้อง";
                    }else{
                        $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                        return response()->json([
                            'code_return' => 88,
                            'msg' => $msg,
                        ]);
                    }
                }else{
                    $msg = "ไม่พบข้อมูล Authen";
                    return response()->json([
                        'code_return' => 99,
                        'msg' => $msg,
                    ]);
                }
                $productdetail = ProductDetailModel::all();
        */
        $department = Datatable::listdata($request);

        return DataTables::of($department)
            ->setRowClass(function ($department) {
                return $department->is_active ? '' : 'alert-danger';
            })
            ->make(true);
        // return response()->json($productdetail);
    }
}
