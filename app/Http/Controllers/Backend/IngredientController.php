<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\IngredientModel;
use App\CoreFunction\Datatable;
use Yajra\DataTables\DataTables;
use Log;
use DB;
use App\CoreFunction\GenUrl;


class IngredientController extends Controller
{
    //
    public function index(){
        return view('backend.Product.index_ingredient');
    }
    public function getDatatable(Request $request)
    {

        $department = Datatable::listingredient($request);
        return DataTables::of($department)
            ->setRowClass(function ($department) {
                return $department->is_active ? '' : 'alert-danger';
            })
            ->make(true);
    }

    public function show(){
        return view('backend.Product.add_ingredient');
    }

    public function store(Request $request){

        $type = 'ingredient';
        $gendata = GenUrl::generate($type);
        $data = new IngredientModel();
        $data->title_en = $request->title_en;
        $data->title_th = $request->title_th;
        $data->detail_en = $request->detail_en;
        $data->detail_th = $request->detail_th;
        $data->link = $gendata['link'];
        $data->is_active = "Y";
        $data->save();

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function destroy($id)
    {
        $data = IngredientModel::find($id);
        $data->is_active = "N";
        $data->update();
        
        // $data->delete();
        return response()->json([
            'type' => 'success',
            'title' => 'delete'
        ]);
    }

    public function edit($id)
    {
        $listitem = IngredientModel::findOrFail($id);
        $data = [
            'item' => $listitem,
        ];
        return view('backend.Product.edit_ingredient')->with($data);
    }

    public function update(Request $request){
        $data = IngredientModel::findOrFail($request->id);
        $data->title_en = $request->title_en;
        $data->title_th = $request->title_th;
        $data->detail_en = $request->detail_en;
        $data->detail_th = $request->detail_th;
        $data->update();

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }


    function fetch_data()
    {
        $positions = IngredientModel::select('id','title_en', 'link', 'is_active')->where('is_active','Y');
        return DataTables::of($positions)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $btn = '<a href="javascript:void(0)" class="edit btn btn-info btn-sm" onclick="CopyMyLeftTd(this)">Copy Link</a>';
              //  $btn = '<button class="btn btn-sm yellow" onclick="myFunction();"><i class="fa fa-cog"></i></button>';
                return $btn;
            })
            ->make(true);
    }



}
