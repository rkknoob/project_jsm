<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\PromotionModel;
use App\Model\CategoryModel;
use App\Model\ProductDetailModel;
use App\Model\MapPromotionModel;
use App\CoreFunction\Datatable;
use Yajra\DataTables\DataTables;
use Auth;


class PromotionController extends Controller
{
    //
    public function index(){
        return view('backend.Promotion.index');
    }

    public function show(){
        
        $listCategory = CategoryModel::all();
        $data = [
            'itemCategory' => $listCategory,
            'idCategory' => null,
            'id_promotion' => null,
        ];
     
        return view('backend.Promotion.add')->with($data);
    }

    public function getDatatable(Request $request)
    {
        $department = Datatable::listpromotion($request);
        return DataTables::of($department)
            ->setRowClass(function ($department) {
                return $department->is_active ? '' : 'alert-danger';
            })
            ->make(true);
    }


    public function store(Request $request){

        $startdate = date("Y-m-d", strtotime($request->startdate));
        $enddate = date("Y-m-d", strtotime($request->enddate));
        $admin = Auth::guard('admin')->user();
        $data = new PromotionModel();
        $data->title_en = $request->title_en;
        $data->title_th = $request->title_th;
        $data->type = $request->type;
        $data->discount = $request->discount;
        $data->start_date = $startdate;
        $data->end_date =  $enddate;
        $data->created_user_id = $admin->id;
        $data->is_active = "Y";
        $data->save();

        $max_id = PromotionModel::max('id');
        
        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
            'max_id' =>  $max_id,
        ]);
    }

    public function destroy($id)
    {
        $admin = Auth::guard('admin')->user();
        $data = PromotionModel::find($id);
        $data->updated_user_id = $admin->id;
        $data->is_active = "N";
        $data->update();

        $data = MapPromotionModel::where('id_promotion',$id)->get();
        foreach($data as $promotion){
            $data = MapPromotionModel::find($promotion->id);
            $data->delete();
        }

        
        return response()->json([
            'type' => 'success',
            'title' => 'delete'
        ]);
    }

    public function edit($id)
    {
        $listitem = PromotionModel::findOrFail($id);
        $start = date("m/d/Y", strtotime($listitem->start_date));
        $end = date("m/d/Y", strtotime($listitem->end_date));
        $date = $start." - ".$end;
        $data = [
            'item' => $listitem,
            'date' => $date,
        ];
        return view('backend.Promotion.edit')->with($data);
    }

    public function update(Request $request){

        $startdate = date("Y-m-d", strtotime($request->startdate));
        $enddate = date("Y-m-d", strtotime($request->enddate));
        $admin = Auth::guard('admin')->user();

        $data = PromotionModel::findOrFail($request->id);
        $data->title_en = $request->title_en;
        $data->title_th = $request->title_th;
        $data->type = $request->type;
        $data->discount = $request->discount;
        $data->start_date = $startdate;
        $data->end_date =  $enddate;
        $data->updated_user_id = $admin->id;
        $data->update();

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }
}
