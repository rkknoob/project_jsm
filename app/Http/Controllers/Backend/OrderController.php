<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\OrderModel;
use App\OrderCartModel;
use App\User;
use App\District;
use App\Sendemail;
use DB;
use Mail;
use App\CoreFunction\CheckEmail;

class OrderController extends Controller
{
    public function index(Request $request)
    {

        $filter = $request->validate([
            'dayfisrt' => 'nullable|date_format:Y-m-d',
            'order_number' => 'nullable',
            'daylast' => 'nullable|date_format:Y-m-d',
            'statuspayment' => 'nullable',
            'daypayment' => 'nullable|date_format:Y-m-d',

        ]);

        // Customer Name
        $listUser = DB::table('users')
            ->leftJoin('session_cart', 'session_cart.id_user', '=', 'users.id')
            ->leftJoin('response_payment', 'response_payment.order_id', '=', 'session_cart.cart_number')
            ->orderBy('session_cart.id', 'desc')
            ->select('users.*','session_cart.id', 'session_cart.id_user','session_cart.cart_number','session_cart.paymentStatus','session_cart.sumPrice','session_cart.deliveryStatus','session_cart.created_at','response_payment.created_at as createpayment','response_payment.order_id');

        if($filter['order_number'] ?? false){
            $listUser->where('session_cart.cart_number', $filter['order_number']);
        }
        if($filter['statuspayment'] ?? false){
            $listUser->where('paymentStatus', $filter['statuspayment']);
        }
        if($filter['dayfisrt'] ?? false){
         //   $chagneformate = OrderModel::change($request->dayfisrt);
            $listUser->whereDate('session_cart.created_at', '>=',$filter['dayfisrt']);
        }
        if($filter['daylast'] ?? false){
            //   $chagneformate = OrderModel::change($request->dayfisrt);
            $listUser->whereDate('session_cart.created_at', '<=',$filter['daylast']);
        }
        if($filter['daypayment'] ?? false){
            //   $chagneformate = OrderModel::change($request->dayfisrt);
            $listUser->whereDate('response_payment.created_at', '>=',$filter['daypayment']);
           
        }

        $data = $listUser->get();
        $datas = [
            'i' => 1,
        ];

        $order_number = $request->order_number;
        $dayfisrt = $request->dayfisrt;
        $daylast = $request->daylast;
        $statuspayment = $request->statuspayment;
        $daypayment = $request->daypayment;

        return view('backend.Order.index',compact('data','dayfisrt','daylast','order_number','statuspayment','daypayment'))->with($datas);
    }

    public function edit($id)
    {

        $listitem = OrderCartModel::findOrFail($id);

        //รายการสินค้า
        $listCart = OrderModel::where('id_cart',$id)->get();

        //ชื่อสินค้า
        $listProduct = DB::table('order')
            ->where('id_cart', $listitem->id)
            ->leftJoin('product_details', 'product_details.id', '=', 'order.id_product')
            ->get();

        //ชื่อ Sku
        $listSku = DB::table('order')
            ->where('id_cart', $listitem->id)
            ->leftJoin('product', 'product.id', '=', 'order.id_sku')
            ->select('order.id_sku','product.id','product.name_en')
            ->get();

        //ชื่อผู้ใช้งาน
        $listUser = User::where('id', $listitem->id_user)->get();

        //Address
        $idAddress = User::where('id', $listitem->id_user)->pluck('id_address')->first();
        $listAddress = District::where('id', $idAddress)->first();

        $data = [
            'item' => $listitem,
            'itemCart' => $listCart,
            'itemProduct' => $listProduct,
            'itemSku' => $listSku,
            'itemUser' => $listUser,
            'itemAddress' => $listAddress,

        ];
        return view('backend.Order.edit')->with($data);
    }

    public function update(Request $request){

        $data = OrderCartModel::findOrFail($request->id);
        $data->deliveryStatus = 'sent';
        $data->trackNumber = $request->trackNumber;
        $data->axFree = $request->axFree;
        $data->update();

        //sent mail
        $listItem = OrderCartModel::findOrFail($request->id);

            $listUser = User::findOrFail($listItem->id_user);
            //Address
            $listAddress = District::where('id', $listUser->id_address)->first();
            $addressUser = $listUser->address .' แขวง/ตำบล: '.$listAddress->district.
                            ' เขต/อำเภอ: '.$listAddress->amphoe.' จังหวัด: '.$listAddress->province.
                            ' รหัสไปรษณีย์: '.$listAddress->zipcode ;

            //Order
            $listCart = OrderModel::where('id_cart',$listItem->id)->get();

            //ชื่อสินค้า
            $listProduct = DB::table('order')
                ->where('id_cart', $listItem->id)
                ->leftJoin('product_details', 'product_details.id', '=', 'order.id_product')
                ->get();

            //ชื่อ Sku
            $listSku = DB::table('order')
                ->where('id_cart', $listItem->id)
                ->leftJoin('product', 'product.sku', '=', 'order.id_sku')
                ->select('order.id_sku','product.id','product.sku','product.name_en')
                ->get();


        $data = array(
            'email' => $listUser->email,
            'name' => $listUser->fname . ' ' . $listUser->lname,
            'address' =>  $addressUser,
            'phone' => $listUser->phone,
            'trackNumber' => $listItem->trackNumber,
            'totalOrder' => $listItem->totalOrder,
            'totalShipping' => $listItem->totalShipping,
            'sumPrice' => $listItem->sumPrice,
            'itemCart' => $listCart,
            'listProduct' => $listProduct,
            'listSku' => $listSku,
        );

        $checksendEmail = Sendemail::create([
            'email' => $data['email'],
            'type' => 'T'
        ]);

        $checkemail = CheckEmail::TrackSendEmail($data);

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);

    }

}
