<?php
namespace App\Http\Controllers;
use App\OrderCartModel;
use Illuminate\Http\Request;
use App\Exports\BulkExport;
use App\Imports\BulkImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\OrderExport;


class ImportExportController extends Controller
{

    public function importExportView()
    {
        //dd("importExportView");
       return view('backend.importexport');
    }
    public function import() 
    {
        Excel::import(new BulkImport,request()->file('file'));
        return redirect()->back()->with('message','บันทึกข้อมูลสำเร็จแล้ว');
    }

    public function export($ordernumber,$dayfirst,$daylast,$statuspayment,$daypayment)
    {

         return Excel::download(new OrderExport($ordernumber,$dayfirst,$daylast,$statuspayment,$daypayment), 'ReportOrder_'. date("dmY") .'.xlsx');
    }

}