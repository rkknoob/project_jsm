<?php

namespace App\Http\Middleware;

use Closure;
use App;

class MultiLang
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

      //$request->session()->flush();
        $locale = session()->get('locale');
        if(!$locale){
            $thai = session()->put('locale', 'th');
       //     \Log::info($thai);
        //    \Log::info('ผ่าน');
            App::setLocale(session()->get('locale'));
        }else {

            App::setLocale(session()->get('locale'));
        }



        return $next($request);

    }
}
