<?php

namespace App;

use App\Model\OrderModel;
use Illuminate\Database\Eloquent\Model;

class OrderCartModel extends Model
{
    protected $table = 'session_cart';

    protected $fillable = [
        'id',
        'cart_number',
        'id_user',
        'totalOrder',
        'totalShipping',
        'sumPrice',
        'paymentStatus',
        'deliveryStatus',
        'trackNumber',
        'axFree',
        
    ];

    public function saleOrders()
    {
        return $this->hasMany(OrderModel::class, 'id_cart');
    }

    
}
