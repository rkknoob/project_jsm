<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MapPromotionModel extends Model
{
    //
    protected $table = 'map_promotion';
    public $timestamps = true;


    protected $fillable = [
        'id',
        'id_product',
        'id_promotion',
    ];
}
