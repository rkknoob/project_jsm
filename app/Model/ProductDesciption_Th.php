<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductDesciption_Th extends Model
{

    protected $table = 'product_description_th';
    protected $fillable = [
        'seq',
        'product_id',
        'detail_type',
        'img_th',
        'is_active',
        'created_at',
        'updated_at',
    ];

}
