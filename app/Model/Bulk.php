<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;
class Bulk extends Model
{
    protected $table = 'product';
    protected $fillable = [
        'sku', 'stock',
    ];
}