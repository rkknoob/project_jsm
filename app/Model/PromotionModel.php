<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PromotionModel extends Model
{
    //
      //
      protected $table = 'promotion';
      public $timestamps = true;
  
  
      protected $fillable = [
          'id',
          'title_en',
          'title_th',
          'type',
          'discount',
          'is_active',
          'start_date',
          'end_date',
          'created_user_id',
          'updated_user_id',

      ];
}
