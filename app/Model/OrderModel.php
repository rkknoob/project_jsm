<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class OrderModel extends Model
{
    //
    protected $table = 'order';
    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

        'id',
        'id_product',
        'id_sku',
        'id_cart',
        'id_user',
        'price',
        'qty',
        'sumPrice',
        'status',
        'created_at',
        'updated_at',
    ];

    public static function change($date)
    {

        $test = date('Y-m-d',$date);
        \Log::info($test);
        $newDate = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)
            ->format('Y-m-d');

        return $newDate;
    }
    public static function lastdate($date)
    {

        $date = date('Y-m-d H:i:s');
        $newDate = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->addDays(1)->format('Y-m-d');
        return $newDate;
    }
}
