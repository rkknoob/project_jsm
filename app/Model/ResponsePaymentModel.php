<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ResponsePaymentModel extends Model
{
    //
    protected $table = 'response_payment';
    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

        'id',
        'request_timestamp',
        'channel_response_code',
        'channel_response_desc',
        'payment_status',
        'ReturnUrl',
        'version',
        'localizedMessage',
        'payment_channel',
        'merchant_id',
        'order_id',
        'invoice_no',
        'currency',
        'amount',
        'masked_pan',
        'transaction_ref',
        'approval_code',
        'eci',
        'transaction_datetime',
        'browser_info',
        'ippPeriod',
        'ippInterestType',
        'ippInterestRate',
        'ippMerchantAbsorbRate',
        'user_defined_1',
        'user_defined_2',
        'user_defined_3',
        'user_defined_4',
        'user_defined_5',
        'hash_value',
        'backend_invoice',
        'stored_card_unique_id',
        'recurring_unique_id',
        'payment_scheme',
        'process_by',
        'sub_merchant_list',
    ];
}
