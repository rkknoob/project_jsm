<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class IngredientModel extends Model
{
    //
    protected $table = 'product_ingredient';
    public $timestamps = true;


    protected $fillable = [
        'id',
        'title_en',
        'title_th',
        'detail_en',
        'detail_th',
        'link',
        'is_active',
    ];
}
