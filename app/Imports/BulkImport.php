<?php
namespace App\Imports;
use App\Model\Bulk;
use Maatwebsite\Excel\Concerns\ToModel;

class BulkImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $listitem = Bulk::where('sku',$row[0])->first();
        // \Log::info($listitem);

        if($listitem == ""){
            // \Log::info("non-sku");
        }else{
            $data = Bulk::find($listitem->id);
            $data->stock = $listitem->stock+$row[1];
            $data->update();
            // \Log::info("success");
        }
    }
}
