<?php

namespace App\Console\Commands;


use App\Model\OrderModel;
use App\OrderCartModel;
use Illuminate\Console\Command;

use Carbon\Carbon;
use App\Product_Incart;
use App\Model\ProductModel;

class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'word:day';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

      //  $cart = session('product_incart');
//        $cart = session('product_incart');
//
//        $times = SessionCart::where('status','C')->get();
//        $current_timestamp = Carbon::now();
//        foreach ($times as $key => $mid) {
//            $actual_start_at = Carbon::parse($mid->updated_at);
//            $mins            = $current_timestamp->diffInHours($actual_start_at, true);
//            if($mins >= 3){
//                SessionCart::where('cart_number', $mid['cart_number'])
//                    ->update([
//                        'status' => 'R'
//                    ]);
//                $product_incart = Product_Incart::where('cart_number',$mid['cart_number'])->get();
//                foreach ($product_incart as $index => $product_incarts) {
//                    $product_sku = ProductModel::where('sku',$product_incarts['sku'])->first();
//                     $stock_sku = $product_sku->stock;
//                     $recive = $product_incarts['sku_qty'];
//                     $total_stock = $stock_sku + $recive;
//                     \Log::info($total_stock);
//
//                    ProductModel::where('sku', $product_incarts['sku'])
//                        ->update([
//                            'stock' => $total_stock
//                        ]);
//
//
//                }
//
//            }
//        }
        $product_wait = OrderCartModel::where('paymentStatus','wait')->get();
        $current_timestamp = Carbon::now();
        $data = collect();
        foreach ($product_wait as $key => $mid) {
            $actual_start_at = Carbon::parse($mid->updated_at);
            $mins = $current_timestamp->diffInHours($actual_start_at, true);
            $max = 3;

            if($mins >= $max){
               $order = OrderCartModel::where('cart_number', $mid->cart_number)->first();
                    $order->update([
                        'paymentStatus' => 'cancel',
                        'deliveryStatus' => 'cancel'
                    ]);  //update รายการ Cart Session
                $data->push($mid->cart_number);

             $session_id = OrderCartModel::where('cart_number', $mid->cart_number)->first();
             $find_order = OrderModel::where('id_cart', $session_id->id)->get();
                \Log::info($session_id->saleOrders);
                foreach ($find_order as $key => $data) {
                    $product_sku = ProductModel::where('sku',$data->id_sku)->first();
                    $sum = $product_sku->stock + $data->qty;
                    $product_sku->update([
                        'stock' => $sum,
                    ]);
                }

            }
        }



        $Token = 'PfXLjN3sHsErMXmTBIeyyzHYkDfrO5gGvY9paKcxIdI';
        $str = "สินค้าที่เกินช่วงเวลา 3 ชั่วโมง";
        $message =  $data;
        $lineapi = $Token; // ใส่ token key ที่ได้มา
        $mms =  trim($message); // ข้อความที่ต้องการส่ง
        date_default_timezone_set("Asia/Bangkok");
        $chOne = curl_init();
        curl_setopt( $chOne, CURLOPT_URL, "https://notify-api.line.me/api/notify");
        // SSL USE
        curl_setopt( $chOne, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt( $chOne, CURLOPT_SSL_VERIFYPEER, 0);
        //POST
        curl_setopt( $chOne, CURLOPT_POST, 1);

        curl_setopt( $chOne, CURLOPT_POSTFIELDS, "message=$mms");

        curl_setopt( $chOne, CURLOPT_FOLLOWLOCATION, 1);

        $headers = array( 'Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer '.$lineapi.'', );
        curl_setopt($chOne, CURLOPT_HTTPHEADER, $headers);

        curl_setopt( $chOne, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec( $chOne );

    }
}
