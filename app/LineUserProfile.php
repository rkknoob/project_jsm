<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LineUserProfile extends Model
{
    protected $table = 'dim_line_user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'userId',
        'avatar',
    ];

}
