<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_Incart extends Model
{
    //
    protected $table = 'product_incart';

    protected $fillable = [
        'id',
        'cart_number',
        'product_id',
        'name',
        'total',
        'sku',
        'sku_name',
        'sku_total',
        'sku_qty',
        'status',

    ];
}
