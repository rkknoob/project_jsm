<?php

namespace App\CoreFunction;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Model\ProductDesciptionModel;
use App\Model\IngredientModel;
use App\Model\ProductDetailModel;
use Log;



class GenUrl extends Model
{
    public static function generate($type)
    {
        $data = [];
        $url_generate = config('app.url');
    
        $protocol = explode(':',"https://project_jsm.test");
        $cuturl = explode('//',$url_generate);
        $generate = $cuturl['1'];

        if ($type == 'ingredient'){
            $maxingredient = IngredientModel::max('id')+1;
            $data['link'] = $generate .'/ingredient/'.$maxingredient;
        }
        if ($type == 'product'){
            $maxingredient = ProductDetailModel::max('id')+1;
            $data['link'] = $generate .'/product/detail/view/'.$maxingredient;
        }


        return $data;
    }
}
