<?php

namespace App\CoreFunction;

use App\Model\Qa\QatopicModel;
use App\Sendemail;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Auth;
use Log;
use Cookie;
use Carbon\Carbon;




class CheckEmail extends Model
{
    public static function SendEmail($data)
    {
        \Mail::send('frontend.Email.form_reset_password', $data, function ($message) use ($data) {
            $message->from(env('MAIL_USERNAME'), 'Project JSM');
            $message->to($data['email'], '')->subject('คำขอรีเซ็ตรหัสผ่าน');
        });
    }


    public static function BSendEmail($data)
    {
        \Mail::send('backend.Email.form_reset_password', $data, function ($message) use ($data) {
            $message->from(env('MAIL_USERNAME'), 'Project JSM');
            $message->to($data['email'], '')->subject('คำขอรีเซ็ตรหัสผ่าน');
        });
    }

    public static function TrackSendEmail($data)
    {
        \Log::info($data);
        \Mail::send('backend.Email.form_set_track', $data, function ($message) use ($data) {
            $message->from(env('MAIL_USERNAME'), 'Project JSM');
            $message->to($data['email'], '')->subject('TrackSendEmail');
        });
    }



}
