<?php

namespace App\CoreFunction;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Model\ProductDesciptionModel;


class Product extends Model
{
    public function storeCategorie($request)
    {

    }

    public static function checkimplode($request)
    {
        if($request[0] == ','){
            $request =  substr($request, 1);
        }

        $checkdata = (explode(",",$request));

        return $checkdata;
    }

    public static function implode($data)
    {
        $check = $data->implode(',');

        return $check;
    }

    public static function cleardes($request)
    {
        $checkdata = (explode(",",$request->file));
/*
        foreach ($checkdata as $p) {
           ProductDesciptionModel::where('img_en',$p)->delete();
            \File::delete(public_path('public/product/'.$p));
        }
*/
        return 1;

    }

    public static function storeImage($id)
    {
        ////ยังไม่ได้ใช้
        $checkdata = (explode(",",$id));    ////data file from request
        try {
            foreach ($checkdata as $image) {
                $data = new ProductDesciptionModel();
                $data->product_id = $id;
                $data->img_en = $image;
                $data->is_active = 'Y';
                $data->save();
            }
            return response()->json([
                'msg_return' => 'ok',
                'code_return' => 1,
            ]);

        } catch (\Exception $e) {
            return response()->json([
                'msg_return' => 'Contact Admin',
                'code_return' => 99,
            ]);
        }

    }

    public static function generateRandomString()
    {
        $cart = session('product_incart');

        if($cart == ""){

            $length = 10;
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }else{
            $cart = session('product_incart');
        //    \Log::info($cart);
    //        \Log::info($cart['0']['cart_number']);

            return $cart['0']['cart_number'];
        }

    }

    public static function checkcartnumber()
    {
        $cart = session('product_incart');
        if($cart == ""){
            $data = 'insert';
            return $data;
        }else {
            $data = 'update';
            return $data;
        }

    }

    public static function discount($price,$type,$discount)

    {
        // \Log::info("ส่วนลด");
    
        // \Log::info($price);
        // \Log::info($type);
        // \Log::info($discount);

        if($type=='percent'){
            $total =  (($discount / 100) * $price);
            $sumdiscount = ($price - $total);
            //\Log::info($sumdiscount);
            return $sumdiscount;
           
        }
        if($type==''){
            $sumdiscount = $price;
            return $sumdiscount;
        }
        if($type=='bath'){
            $sumdiscount = $price-$discount;
            return $sumdiscount;
        }
    }

//    function discount($total)
//    {
//        \Log::info($total);
//        return 0;
//    }


}
