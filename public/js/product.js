$(document).ready(function () {
  $('.detail_cart').on('click', function (e) {
    var form = $('#cartForm');
    var data = form.serialize(); //   var qty = $('input[name="qty[]"]').val();
    //  var sku = $('input[name="sku[]"]').val();

    var urlForm = form.attr('action');
    var base_url = window.location.origin;
    var urlVerify = base_url + '/carts/verify_quantity';
    var id = $('input[name="id"]').val();
    var price = $('input[name="price"]').val();
    var sales_price = $('input[name="sales_price"]').val();
    var name = $('input[name="name"]').val();
    var total = $('input[name="total"]').val();
    var sku = document.getElementsByName('sku[]');
    var sku_qty = document.getElementsByName('qty[]');
    var sku_name = document.getElementsByName('sku_name[]');
    var sku_total = document.getElementsByName('totalitem[]');
    var result = [];

    for (var i = 0; i < sku.length; i++) {
      var productitem = {
        //  สินค้า Sku
        sku_qty: sku_qty[i].value,
        sku: sku[i].value,
        sku_name: sku_name[i].value,
        sku_total: sku_total[i].value
      };
      result.push(productitem);
    }

    var productitem = {
      product_id: id,
      price: price,
      sales_price: sales_price,
      name: name,
      total: total,
      sku: result
    };

    if (productitem['sku'].length == 0) {
      swal("คุณไม่ได้เลือก Option", "กรุณาเลือก Option", "error");
      return false;
    }

    axios.post(urlVerify, productitem).then(function (response) {
      if (response.data.status) {
        axios.post(urlForm, productitem).then(function (res) {
          $('.badge-cart').text(res.data.sum_quantity);
          swal("เพิ่มสินค้าลงตะกร้าเรียบร้อย", '', "success");
        })["catch"](function (error) {});
      } else {
        swal("สินค้าไม่มีเพียงพอ", '', "error");
        return false;
      }
    })["catch"](function (error) {});
  });
  $('.detail_now').on('click', function (e) {
    var form = $('#cartForm');
    var data = form.serialize(); //   var qty = $('input[name="qty[]"]').val();
    //  var sku = $('input[name="sku[]"]').val();

    var urlForm = form.attr('action');
    var base_url = window.location.origin;
    var urlVerify = base_url + '/carts/verify_quantity';
    var id = $('input[name="id"]').val();
    var price = $('input[name="price"]').val();
    var name = $('input[name="name"]').val();
    var total = $('input[name="total"]').val();
    var sku = document.getElementsByName('sku[]');
    var sku_qty = document.getElementsByName('qty[]');
    var sku_name = document.getElementsByName('sku_name[]');
    var sku_total = document.getElementsByName('totalitem[]');
    var result = [];

    for (var i = 0; i < sku.length; i++) {
      var productitem = {
        //  สินค้า Sku
        sku_qty: sku_qty[i].value,
        sku: sku[i].value,
        sku_name: sku_name[i].value,
        sku_total: sku_total[i].value
      };
      result.push(productitem);
    }

    var productitem = {
      product_id: id,
      price: price,
      name: name,
      total: total,
      sku: result
    };

    if (productitem['sku'].length == 0) {
      swal("คุณไม่ได้เลือก Option", "กรุณาเลือก Option", "error");
      return false;
    }

    axios.post(urlVerify, productitem).then(function (response) {
      if (response.data.status) {
        axios.post(urlForm, productitem).then(function (res) {
          $('#option_product').prop('selectedIndex', 0);
          var base_url = window.location.origin;
          var urlVerify = base_url + '/shop/basket';
          window.location.href = urlVerify;
        })["catch"](function (error) {});
      } else {
        swal("สินค้าไม่มีเพียงพอ", '', "error");
        return false;
      }
    })["catch"](function (error) {});
  });
});
