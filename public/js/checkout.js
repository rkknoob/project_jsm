var sumnumber = 0;
var sumprice = 0;
var sumtotal = 0;



$(document).ready(function () {
    getincart();

    function getincart() {
        n = 0;
        // ดึง session มาแสดง
        $.ajax({
            type: "POST",
            dataType: 'JSON',
            url: '/shop/me',
            data: {
                data: 1
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function success(data) {
                //console.log(data);
                var html = '';
                $("#product-incard").html("");
                $.each(data.products, function (index, itemdata) {
                    sumprice += parseInt(itemdata.sum_total.replace(",", ""));
                    n += parseInt(itemdata.sku_qty);

                    //console.log(itemdata);
                    html += '<tr>';
                    // html +=
                    //     '<td class="displaynone">\n' +
                    //     '<div class="tb-center">\n' +
                    //     '<input type="checkbox" name="basketchks" id="basketchks" checked="checked" class="MS_input_checkbox pr_NORMAL_2191468" onclick="cal_basket_chk(this);">\n' +
                    //     '<input type="hidden" name="basket_item" value="{&quot;uid&quot;:&quot;2191468&quot;,&quot;cart_id&quot;:&quot;1&quot;,&quot;cart_type&quot;:&quot;NORMAL&quot;,&quot;pack_uid&quot;:&quot;&quot;}">\n' +
                    //     '<input type="hidden" name="extra_item" value="{&quot;extra_require_uid&quot;:null,&quot;extra_require&quot;:null,&quot;extra_main_brandname&quot;:&quot;&quot;}">\n' +
                    //     '</div>\n' +
                    //     '</td>';
                    html +=
                        '<td>\n' +
                        '<div class="tb-center">\n' +
                        '<div class="thumb">\n' +
                        '<a href="#">\n' +
                        '<img src="/public/product/' + itemdata.img + '" alt="Product thumbnail" title="Product thumbnail">\n' +
                        '</a>\n' +
                        '</div>\n' +
                        '</div>\n' +
                        '</td>';
                    html +=
                        '<td>\n' +
                        '<div class="tb-left">\n' +
                        '<h5 class="media-heading">\n' +
                        '<a href="#">\n' +
                        '<font class="name-product" style="vertical-align: inherit;">\n' +
                        '<font style="vertical-align: inherit;">' + itemdata.product_name + '</font>\n' +
                        '</font>\n' +
                        '</a>\n' +
                        '</h5>\n' +
                        '<span class="MK-product-icons"></span>\n' +
                        '<div id="2191468_1" class="tb-left tb-opt">\n' +
                        '<strong>\n' +
                        '<font style="vertical-align: inherit;">\n' +
                        '<font style="vertical-align: inherit;"></font>\n' +
                        '</font>\n' +
                        '</strong>\n' +
                        '<font style="vertical-align: inherit;">\n' +
                        '<font style="vertical-align: inherit;"> ' + itemdata.sku_name + '</font>\n' +
                        '</font>\n' +
                        '</div>\n' +
                        '<div class="uni-opt">\n' +
                        '<a href="javascript:modify_option(\'2191468\', \'1\',\'\');" class="modify">\n' +
                        '<font style="vertical-align: inherit;">\n' +
                        '</font>\n' +
                        '</a>\n' +
                        '</div>\n' +
                        '</div>\n' +
                        '</td>';
                    html +=
                        '<td>\n' +
                        '<div class="tb-center">\n' +
                        '<font style="vertical-align: inherit;">\n' +
                        '<font style="vertical-align: inherit;">฿' + itemdata.price_discount + '</font>\n' + '                                            </font>\n' + '                                        </div>\n' + '                                    </td>';
                    html += '<td>\n' +
                        '<div class="tb-center">\n' +
                        '<div class="num_c">\n' +
                        '<font style="vertical-align: inherit;">' + itemdata.sku_qty + '</font>\n' +
                        '</div>\n' +
                        '</td>';
                    html +=
                        '<td>\n' +
                        '<div class="tb-center tb-bold">\n' +
                        '<span>\n' +
                        '<font style="vertical-align: inherit;">\n' +
                        '<font class="{{$a}}" style="vertical-align: inherit;">฿' + itemdata.sum_total + '</font>\n' +
                        '</font>\n' + '                                            </span>\n' + '                                        </div>\n' + '                                    </td>';
                    // html += '   <td>\n' + '                                        <div class="tb-center tb-delivery">\n' + '                                            <div class="MS_tb_delivery">\n' + '                                                <span class="MS_deli_txt" onmouseover="overcase(this, \'1\')" onmouseout="outcase(this, \'1\')">\n' + '                                                    <span class="MS_deli_title MS_deli_block">\n' + '                                                        <font style="vertical-align: inherit;">\n' + '                                                            <font style="vertical-align: inherit;">[Basic Shipping]</font>\n' + '                                                        </font></span><span class="MS_deli_desc MS_deli_block">\n' + '                                                        <font style="vertical-align: inherit;">\n' + '                                                            <font style="vertical-align: inherit;">Condition</font>\n' + '                                                        </font>\n' + '                                                    </span>\n' + '                                                </span>\n' + '                                                <div id="deliverycase1" class="MS_layer_delivery">\n' + '                                                    <dl>\n' + '                                                        <dt>\n' + '                                                            <font style="vertical-align: inherit;">\n' + '                                                                <font style="vertical-align: inherit;">Delivery condition: Basic delivery (condition)</font>\n' + '                                                            </font>\n' + '                                                        </dt>\n' + '                                                        <dd>\n' + '                                                            <font style="vertical-align: inherit;">\n' + '                                                                <font style="vertical-align: inherit;">If the order amount is </font>\n' + '                                                                <font style="vertical-align: inherit;">less than </font>\n' + '                                                            </font>\n' + '                                                            <span class="MS_highlight">\n' + '                                                                <font style="vertical-align: inherit;">\n' + '                                                                    <font style="vertical-align: inherit;">30,000 won</font>\n' + '                                                                </font>\n' + '                                                            </span>\n' + '                                                            <font style="vertical-align: inherit;">\n' + '                                                                <font style="vertical-align: inherit;"> , </font>\n' + '                                                            </font>\n' + '                                                            <br>\n' + '                                                            <br style="line-height:3px">\n' + '                                                            <font style="vertical-align: inherit;">\n' + '                                                                <font style="vertical-align: inherit;">shipping fee of </font>\n' + '                                                            </font>\n' + '                                                            <span class="MS_highlight">\n' + '                                                                <font style="vertical-align: inherit;">\n' + '                                                                    <font style="vertical-align: inherit;">2,500 won</font>\n' + '                                                                </font>\n' + '                                                            </span>\n' + '                                                            <font style="vertical-align: inherit;">\n' + '                                                                <font style="vertical-align: inherit;"> will be charged.</font>\n' + '                                                            </font>\n' + '                                                        </dd>\n' + '                                                    </dl>\n' + '                                                    <span class="bull"></span>\n' + '                                                    <!-- <iframe id="deliverycase_iframe1" class="MS_layer_delivery_iframe" frameborder="no" border="0"></iframe> -->\n' + '                                                </div>\n' + '                                            </div>\n' + '                                        </div>\n' + '                                    </td>';
                    // html += ' <td>\n' + '                                        <div class="tb-center">\n' + '                                            <span class="d-block link_del">\n' + '                                                <a href="#" data-product_id="16828" data-product_sku="" data-toggle="tooltip" data-placement="top" data-original-title="ลบ"  id="' + index + '" class="remove btn btn-primary btn-block">\n' + '                                                    <i class="fa fa-trash-o"></i>\n' + '                                                </a>\n' + '                                            </span>\n' + '                                        </div>\n' + '                                    </td>';
                    html += '</tr>';


                });

                console.log(sumprice);
                console.log(n);

                var htmlsumnumber = '';
                $("#number_sum").html("");
                htmlsumnumber = '<span id="number_sum">' + sumnumber.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.00' + '</span>'
                $("#number_sum").append(htmlsumnumber);

                var htmlsumprice = '';
                $("#sumprice").html("");
                htmlsumprice = '<span id="sumprice">' + sumprice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.00' + '</span>'
                $("#sumprice").append(htmlsumprice);


                if (sumprice < 500) {
                    var htmlems_sum = '';
                    $("#ems_sum").html("");
                    htmlems_sum = '<span id="ems_sum">' + 40 + '</span>'
                    $("#ems_sum").append(htmlems_sum);

                    sumtotal = sumprice + 40;

                    var htmltotal_sum = '';
                    $("#total_sum").html("");
                    htmltotal_sum = '<span id="total_sum">' + sumtotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.00' + '</span>'
                    $("#total_sum").append(htmltotal_sum);

                } else if (sumprice > 500) {

                    var htmlems_sum = '';
                    $("#ems_sum").html("");
                    htmlems_sum = '<span id="ems_sum">' + 0 + '</span>'
                    $("#ems_sum").append(htmlems_sum);

                    sumtotal = sumprice;

                    var htmltotal_sum = '';
                    $("#total_sum").html("");
                    htmltotal_sum = '<span id="total_sum">' + sumtotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.00' + '</span>'
                    $("#total_sum").append(htmltotal_sum);

                }

                $('#product-incard').append(html);
            },
            error: function error(qXHR, textStatus, errorThrown) {}
        });
    }

});
