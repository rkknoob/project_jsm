<?php

use App\Model\ProductDesciptionModel;
use App\Model\ProductDesciption_Th;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ProductdescTableSeeds extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_description')->delete();
        DB::table('product_description_th')->delete();

        $json = File::get("database/productdes.json");
        $json_th = File::get("database/productdes_th.json");
        $data = json_decode($json);
        $data_th = json_decode($json_th);
        foreach ($data as $obj) {
            ProductDesciptionModel::create(array(
                'id' => $obj->id,
                'seq' => $obj->seq,
                'product_id' => $obj->product_id,
                'detail_type' => null,
                'img_en' => $obj->img_en,
                'img_th' => $obj->img_th,
                'is_active' => $obj->is_active,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ));
        }

        foreach ($data_th as $objs) {
            ProductDesciption_Th::create(array(
                'id' => $objs->id,
                'seq' => $objs->seq,
                'product_id' => $objs->product_id,
                'detail_type' => null,
                'img_th' => $objs->img_th,
                'is_active' => $objs->is_active,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ));


        }

    }
}
