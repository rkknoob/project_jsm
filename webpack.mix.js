const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');


mix.babel(['resources/js/pages/front/test.js'], 'public/js/test.js').version();
mix.babel(['resources/js/pages/front/product.js'], 'public/js/product.js').version();
mix.babel(['resources/js/pages/front/cart.js'], 'public/js/cart.js').version();




mix.babel(
    [
        'node_modules/vue/dist/vue.min.js',
        'node_modules/jquery/dist/jquery.min.js',
        'node_modules/axios/dist/axios.min.js',
        'node_modules/popper.js/dist/umd/popper.min.js',
        'node_modules/bootstrap/dist/js/bootstrap.min.js',
        'node_modules/select2/dist/js/select2.full.min.js',
        'node_modules/sweetalert2/dist/sweetalert2.all.min.js',
        'node_modules/jquery-validation/dist/jquery.validate.min.js',
        'node_modules/jquery-validation/dist/additional-methods.min.js',
        'node_modules/jquery-validation/dist/localization/messages_th.min.js',
        'node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
        'node_modules/bootstrap-select/dist/js/bootstrap-select.min.js',
        'node_modules/summernote/dist/summernote-bs4.min.js',
        'node_modules/bootstrap-input-spinner/src/bootstrap-input-spinner.js',
        'node_modules/numeral/min/numeral.min.js',
        'node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js',
        'resources/js/front.js'
    ],
    'public/js/global.js'
).version();
